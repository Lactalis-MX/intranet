<?php
require_once 'Controllers/Controller.php';
require_once 'Models/Crud.php';
session_start();
$solicitud = new MvcController();
$solicitud->ingresoUserController();
if (!isset($_SESSION['user'])  && !isset($_SESSION['user_dasa']) && !isset($_SESSION['user_del'])) {
    header('Location: index.php');
}
?>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Nosotros</title>
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    <script src="assets/js/jquery-ui-1.12.1.custom/jquery-ui.js"></script>
    <script src="assets/js/sliderDash.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="assets/js/envioEmail.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/dynamicDashboard.js"></script>


    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/imgStyle.css">
    <!--<link rel="stylesheet" href="http://csshake.surge.sh/csshake.css">-->
    <link rel="stylesheet" href="assets/css/styleDashBoard.css">
    <link rel="stylesheet" href="assets/css/sliderStyle.css">
    <link rel="stylesheet" href="assets/css/menuStyle.css">
    <link rel="stylesheet" href="assets/css/bannerStile.css">
    <link rel="stylesheet" href="assets/css/footerStyle.css">


    <!-- Custom styles for this template -->
    <link href="css/modern-business.css" rel="stylesheet">
    <link rel="icon" href="img/grupo_lactalis.png" type="image/gif" sizes="16x16">

</head>

<body>

<!-- Navigation -->
<nav class="navbar fixed-top navbar-expand-lg navbar- bg fixed-top" style="background: #77c5e7">
    <div class="container">
        <a class="navbar-brand" href="principal.php" id="imgLogoLactalis"><img src="img/lactalis_mexico.jpg" alt=""
                                                                               class="float-lg-left w-50"
                                                                               id="img_logo_home"></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="acerca.php" style="color: #FFFFFF;font-size: 18px">Acerca de nosotros</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="contactUs.php" style="color: #FFFFFF;font-size: 18px"">Contáctanos</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="historia.php" style="color: #FFFFFF;font-size: 18px"">Historia</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="principal.php#title" style="color: #FFFFFF;font-size: 18px"">Comunicados</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="lactajob.php" style="color: #FFFFFF;font-size: 18px"">LactaJob</a>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false" style="color: #FFFFFF;font-size: 18px"">
                    Formatos
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                        <a class="dropdown-item" href="intranet/Formatos/Formato%20de%20Servicios%20IT.pdf">Formato de servicios IT</a>
                        <a class="dropdown-item" href="intranet/Formatos/FORMATO%20PRESTAMOS.pdf">Formato de préstamos</a>
                        <a class="dropdown-item" href="intranet/Formatos/FORMATO%20VACACIONES.pdf">Formato de vacaciones</a>
                        <a class="dropdown-item" href="intranet/Formatos/Anexo%201%20-Requisición%20de%20Personal.pdf">Requisición de Personal</a>
                        <a class="dropdown-item" href="intranet/Formatos/Formato Entrevista de Salida Nuevo V04062019.pdf">Formato  Entevista de Salida</a>
                        <a class="dropdown-item" href="intranet/Formatos/FORMATO%20DE%20MOVIM.%20DE%20PERSONAL.PDF">Formato Movimiento de Personal</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false" style="color: #FFFFFF;font-size: 18px"">
                    Politicas
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                        <a class="dropdown-item" href="intranet/POLITICAS/Anexo%201%20-Requisición%20de%20Personal.pdf">Política Requisición de Personal</a>
                        <a class="dropdown-item" href="intranet/POLITICAS/DRH-P012-POLITICA%20UBER%20FOR%20BUSINESS.pdf">Política Uber para Negocios</a>
                        <a class="dropdown-item" href="intranet/POLITICAS/Política%20de%20viajes%20nacionales%20e%20internacionales.pdf">Política de Viajes</a>
                        <a class="dropdown-item" href="intranet/POLITICAS/CODIGO%20DE%20CONDUCTA.PDF">Código de Conducta</a>
                        <a class="dropdown-item" href="intranet/POLITICAS/Comunicado Interno - Movimientos de Personal.pdf">Movimiento de Personal</a>
                    </div>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false" style="color: #FFFFFF;font-size: 18px"">
                    Configuración
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                        <a class="dropdown-item" href="profile.php">Perfil</a>
                        <a class="dropdown-item" href="Controllers/cerrarSesion.php">Cerrar sesión</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>


<br><br><br>

<!-- Page Content -->
<div class="container">

    <!-- Page Heading/Breadcrumbs -->
    <h1 class="mt-4 mb-3" style="">
        <b>Nuestra Historia</b>
    </h1>

    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="principal.php">Inicio</a>
        </li>
        <li class="breadcrumb-item active">Historia</li>

        <li class="breadcrumb-item"><a href="#h1933" id="li1933">1933</a></li>
        <li class="breadcrumb-item"><a href="#h1940" id="li1933">1940</a></li>
        <li class="breadcrumb-item"><a href="#h1950" id="li1933">1950</a></li>
        <li class="breadcrumb-item"><a href="#h1968" id="li1933">1968</a></li>
        <li class="breadcrumb-item"><a href="#h2000" id="li1933">2000</a></li>
        <li class="breadcrumb-item"><a href="#h2006" id="li1933">2006</a></li>
        <li class="breadcrumb-item"><a href="#h2010" id="li1933">2010</a></li>
        <li class="breadcrumb-item"><a href="#h2015" id="li1933">2015</a></li>


    </ol>

    <!-- Intro Content -->
    <div class="row" style="background-image: url('img/img_historia/fondo_milk.jpg')">

        <div class="col-lg-3  mb-4 py-5">
            <img class="img-fluid rounded mb-lg-5" src="img/img_historia/besnier.jfif" alt="">
        </div>

        <div class="col-lg-7  mb-4  card-body" id="h1933">
            <h3 class="col-lg-3 card-title"><a href="#li1933" class="text-lg" style="font-size: 40px">1933</a></h3>
            <div class="card-text text-lg-center py-5">

                <p>
                    <strong>
                        André Besnier - 1ra generación
                        El 19 de octubre de 1933 marca el inicio de la aventura Lactalis.
                    </strong>
                </p>
                <p>
                    <strong>
                        André Besnier produce sus primeros 17 camemberts usando 35 litros de leche recolectados
                        alrededor de
                        Laval.
                    </strong>
                </p>
                <p>
                    <strong>
                        La primera página de la historia de la compañía está escrita: así es como nace la compañía
                        individual André Besnier.
                    </strong>

                </p>

                <p>
                    <strong>
                        Muy rápidamente, André Besnier se rodea de colaboradores que lo acompañan en esta nueva aventura
                        lechera. La primera marca de Camembert "Le Petit Lavallois", con su etiqueta emblemática de
                        Laval
                        medieval, pronto se convirtió en sinónimo de calidad.
                    </strong>
                </p>

            </div>
        </div>

    </div>
    <br>
    <div class="row" style="background-image: url('img/img_historia/1940.jpg')">

        <div class="col-lg-3  mb-4 py-4">
            <img class="img-fluid rounded mb-lg-5" src="img/img_historia/h40.jpg" alt="">
        </div>

        <div class="col-lg-7  mb-4  card-body" id="h1940">
            <h3 class="col-lg-3 card-title"><a href="#li1933" class="text-lg" style="font-size: 40px">1940</a></h3>
            <div class="card-text text-lg-center py-5">
                <p>
                    <strong>
                        Los años 40 confirman el crecimiento de la compañía y un número creciente de agricultores se
                        convierten en su proveedor de leche. En 1948, la compañía se transformó en "SARL Société
                        Laitière de
                        Laval A. Besnier & Cie", que alcanzó la meta de 10,000 litros de leche recolectada; un evento
                        importante, hecho posible gracias a una política comercial dinámica e innovadora implementada
                        por
                        André Besnier, con la ayuda de 25 empleados.

                    </strong>

                </p>
                <p>
                    <strong>
                        Desde el principio, Besnier define las fortalezas que serán la clave del éxito de los 80 años
                        siguientes: la pasión por la leche, la necesidad de calidad, el espíritu empresarial y la
                        capacidad
                        de estar siempre rodeado de las mejores personas.
                    </strong>
                </p>

            </div>
        </div>

    </div>
    <br>
    <div class="row" style="background-image: url('img/img_historia/1950.jpg')">

        <div class="col-lg-3  mb-4   py-4">
            <img class="img-fluid rounded mb-lg-5" src="img/img_historia/h40.jpg" alt="">
        </div>

        <div class="col-lg-9  mb-4  card-body" id="h1950">
            <h3 class="col-lg-3 card-title"><a href="#li1933" class="text-lg" style="font-size: 40px">1950</a></h3>
            <div class="card-text text-lg-center py-5">
                <p>
                    <strong>

                        Al ser un empresario calificado, André Besnier desarrolló su negocio considerablemente en la
                        primera
                        mitad de la década de 1950 y, dado que el sector lácteo es cada vez más competitivo, decidió
                        diversificar su oferta produciendo también mantequilla y nata.

                    </strong>
                </p>
                <p>
                    <strong>
                        Al igual que con camembert, que define la reputación de la compañía y se relanza con la marca
                        "Le
                        Voyageur", todos los demás productos vendidos también cuentan con las mismas características de
                        calidad en un intento por lograr un objetivo simple y exigente: la perfección. El ambicioso y
                        fino
                        estratega, André Besnier también vende leche de consumo, y es uno de los primeros en lanzar
                        leche en
                        una botella de vidrio de 1 litro, bajo la marca "SSL Le Bon Lait".
                    </strong>
                </p>
                <p>
                    <strong>
                        1955 fue el año en que falleció André Besnier, pero también el año en que su hijo Michel ocupó
                        su
                        lugar en la empresa. Apasionado tanto como su padre, Michel Besnier desarrolla completamente el
                        sentido empresarial familiar y duplica su facturación anual en solo dos años.
                    </strong>
                </p>

            </div>
        </div>

    </div>
    <br>
    <div class="row" style="background-image: url('img/img_historia/1960.jpg')">

        <div class="col-lg-3  mb-4   py-4">
            <img class="img-fluid rounded mb-lg-5" src="img/img_historia/content_60.jpg" alt="">
        </div>

        <div class="col-lg-9  mb-4  card-body" id="h1968">
            <h3 class="col-lg-3 card-title"><a href="#li1933" class="text-lg" style="font-size: 40px">1968</a></h3>
            <div class="card-text text-lg-center py-5">
                <p>
                    <strong>
                        Bajo la guía de Michel Besnier, se definen los fundamentos de lo que serán su éxito y su modelo
                        de
                        negocio: competencia industrial, calidad e innovación, compromiso con el procesamiento de
                        productos
                        lácteos y crecimiento externo dinámico. Varias lecherías del Gran Oeste de Francia se unen a la
                        compañía Besnier, extendiendo así el área y el volumen de recolección de leche.

                    </strong>
                </p>
                <p>
                    <strong>
                        Varias lecherías del Gran Oeste de Francia se unen a la compañía Besnier, extendiendo así el
                        área y
                        el volumen de recolección de leche.
                    </strong>
                </p>
                <p>
                    <strong>

                        Esto también le permite ampliar la gama y satisfacer las necesidades de los supermercados que
                        están
                        empezando a crecer. El visionario Michel Besnier sabe que el comercio está cambiando y que debe
                        ser
                        uno de los primeros en contribuir a esta revolución.
                </p>

                <p>
                    Es entonces cuando decide lanzar la marca Président, que se convertirá en el emblema del Grupo.
                    Además, innova al comercializar la primera leche envasada en Tetra Pak, la "Lait 2000", que influirá
                    profundamente en los hábitos de consumo de los franceses.
                    </strong>
                </p>

            </div>
        </div>

    </div>
    <br>
    <div class="row" style="background-image: url('img/img_historia/1980.jpg')">

        <div class="col-lg-3  mb-4   py-4">
            <img class="img-fluid rounded mb-lg-5" src="img/img_historia/content_90.jpg" alt="">
        </div>

        <div class="col-lg-9  mb-4  card-body" id="h2000">
            <h3 class="col-lg-3 card-title"><a href="#li1933" class="text-lg" style="font-size: 40px">2000</a></h3>
            <div class="card-text text-lg-center py-5">
                <p>
                    <strong>
                        En 1999, la compañía Besnier toma el nombre de Groupe Lactalis para responder a su vocación
                        internacional, con un nombre que es más fácil de entender y pronunciar en los cinco continentes.
                        Finalmente, en el mismo año, Michel Besnier inauguró el museo "Lactopôle André Besnier" en el
                        sitio
                        de la lechería histórica de Laval, para rendir homenaje a los intercambios de leche y volver
                        sobre
                        la historia de la industria láctea.
                    </strong>
                </p>
                <p>
                    <strong>
                        La nueva década comienza, lamentablemente, con la muerte de Michel Besnier, un evento inesperado
                        e
                        impactante para todos sus colaboradores. Sin embargo, la compañía continúa manteniendo su
                        administración enteramente familiar y las riendas de la compañía son tomadas por su hijo,
                        Emmanuel
                        Besnier.
                    </strong>

                </p>
                <p>

                </p>


            </div>
        </div>

    </div>
    <br>
    <div class="row" style="background-image: url('img/img_historia/00.jpg')">

        <div class="col-lg-3  mb-4   py-4">
            <img class="img-fluid rounded mb-lg-5" src="img/img_historia/Galbani_logo.png" alt="">
        </div>

        <div class="col-lg-9  mb-4  card-body" id="h2006">
            <h3 class="col-lg-3 card-title"><a href="#li1933" class="text-lg" style="font-size: 40px">2006</a></h3>
            <div class="card-text text-lg-center py-5">
                <p><strong>A pesar del difícil contexto de la crisis económica de 2008, Groupe Lactalis ha mantenido una
                        tasa
                        de crecimiento constante desde 2000.
                    </strong>
                </p>
                <p>
                    <strong>
                        Retomada de Galbani,en 2006 respectivamente,
                        en Italia, con la adquisición del buque insignia Galbani, y en Europa, con la empresa conjunta
                        con
                        Nestlé para productos frescos, que permitirá a las marcas (en particular, a La Laitière) para
                        renovarse y producir un crecimiento rentable. Al año siguiente, el líder croata de productos
                        lácteos
                        Dukat se unirá al Grupo.
                    </strong>
                </p>

            </div>
        </div>

    </div>
    <br>
    <div class="row" style="background-image: url('img/img_historia/10.jpg')">

        <div class="col-lg-3  mb-4   py-4">
            <img class="img-fluid rounded mb-lg-5" src="img/img_historia/content_2010.jpg" alt="">
        </div>

        <div class="col-lg-9  mb-4  card-body" id="h2010">
            <h3 class="col-lg-3 card-title"><a href="#li1933" class="text-lg" style="font-size: 40px">2010</a></h3>
            <div class="card-text text-lg-center py-5">
                <br>
                <p>
                    <strong>
                        Es en 2011, el año en que Laval Group se convierte en el líder mundial en el sector lácteo
                        gracias a
                        la adquisición de las acciones mayoritarias del líder italiano en leche para consumo, Parmalat,
                        que
                        en ese momento tenía 14,000 empleados y logró una mayor rotación. de 4 mil millones de euros.
                    </strong>
                </p>
                <p>
                    <strong>
                        La empresa italiana es perfectamente complementaria, gracias a su presencia en territorios donde
                        el
                        Grupo no operaba o no estaba muy presente. Con esta nueva dimensión, el Grupo puede continuar su
                        crecimiento dinámico y rentable, con mercados históricos y maduros, así como con países
                        emergentes.
                    </strong>
                </p>
                <p>
                    <strong>
                        La leche de consumo se convierte en la segunda categoría más importante para el Grupo, que sin
                        embargo sigue siendo el líder en la producción de queso.
                    </strong>

                </p>

            </div>
        </div>

    </div>
    <br>
    <div class="row" style="background-image: url('img/img_historia/historiaEsmeralda.jpg')">

        <div class="col-lg-3  mb-4   py-4">
            <img class="img-fluid rounded mb-lg-5" src="" alt="">
        </div>

        <div class="col-lg-9  mb-4  card-body" id="h2015">
            <h3 class="col-lg-3 card-title"><a href="#h2015" class="text-lg" style="font-size: 40px"></a></h3>
            <div class="card-text text-lg-center py-5">
                <br>
                <br>
                <br>
                <p class="col-sm-4"  id="contentEsmeralda">
                    <strong>
                        En 1956 nace Derivados de Leche Esmeralda, en San Miguel de Allende Guanajuato.
                        Iniciando operaciones a través de la venta de dulces y posteriormente elaborando quesos, cremas
                        y mantequillas.
                    </strong>
                </p>

                <p style="font-size: 17px">
                    <strong>

                    </strong>
                </p>
                <p class="col-sm-4"  id="contentParmalat">
                    <strong>
                        Es Mayo de 2015, Lactalis adquiere Esmeralda a través de Parmalat, con 4 mil colaboradores.
                    </strong>
                </p>
                <p class="col-sm-3" id="contentActual">
                    <strong>
                        En 2018, Lactalis ya cuenta con 80,000 empleados y 19 billones de litros de leche recogidos / año,
                        contando con 240 plantas de producción en 47 paíeses
                    </strong>
                </p>

            </div>
        </div>

    </div>


    <!-- Our Customers -->
    <hr>
    <hr>
    <!-- /.row -->

</div>
<!-- /.container -->

<!-- Footer -->
<footer class="py-5 bg" style="background: #77c5e7">
    <div class="container" style="color: #FFFFFF">
        © 2018 Copyright:
        <a href="http://www.esmeralda.com.mx/" style="color: #FFFFFF"> Esmeralda.com.mx</a>
        <p>todos los derechos reservados</p>
    </div>
    <!-- /.container -->
</footer>

</body>

</html>
