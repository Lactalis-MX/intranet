<?php
session_start();
require_once "Controllers/Controller.php";
require_once "Models/Crud.php";
$login = new MvcController();
$login->ingresoUserController();
?>
<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    <script src="assets/js/validarRegistro.js"></script>
    <!--<script src="assets/js/jquery-ui.min.js"></script>-->
    <script src="assets/js/moment.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>



    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style_Login.css">
    <link rel="icon" href="img/grupo_lactalis.png" type="image/gif" sizes="16x16">


    <style media="screen">
        @media (min-width: 576px){
            .Login{
                position: absolute;
                background: #4e94ab;
                width: 70%;
                margin-left: -70%;
                transition: all 0.5s;
            }
        }

    </style>
</head>
<body>
<?php
require 'partials/formLoginUser.php';
?>
</body>
</html>