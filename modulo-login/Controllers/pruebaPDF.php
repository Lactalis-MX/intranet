<?php
require __DIR__.'/../vendor/autoload.php';

use Spipu\Html2Pdf\Exception\ExceptionFormatter;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Html2Pdf;

try{
    ob_start();
    require_once '../partials/reporte.php';
    require '../Controllers/fechaActual.php';

    $html   =   ob_get_clean();
    $html2pdf = new Html2Pdf('P','A4','es');
    $html2pdf->writeHTML($html);
    $html2pdf->output('Constacia_de_Vacaciones.pdf');
}catch (Html2PdfException $e){
    $html2pdf->clean();
    $formatter  =   new ExceptionFormatter($e);
    echo $formatter->getHtmlMessage();
}

?>
