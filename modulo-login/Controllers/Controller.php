<?php
/*
 * Librería PHPmailer, es la ruta absoluta donde se encutra dicha libreria, para subir a producción se deben mantener estas lineas descomentadas.
 * Para entrar en desarrollo se debe descomentar a partir desde la linea 12
 */
include('C:\inetpub\wwwroot\modulos\modulo-login\vendor\PHPMailer-master\src\OAuth.php');
include('C:\inetpub\wwwroot\modulos\modulo-login\vendor\PHPMailer-master\src\PHPMailer.php');
include('C:\inetpub\wwwroot\modulos\modulo-login\vendor\PHPMailer-master\src\Exception.php');
include('C:\inetpub\wwwroot\modulos\modulo-login\vendor\PHPMailer-master\src\SMTP.php');
include('C:\inetpub\wwwroot\modulos\modulo-login\vendor\PHPMailer-master\src\POP3.php');

/*
include('C:\xampp\htdocs\PhpstormProjects\modulos\modulo-login\vendor\PHPMailer-master\src\OAuth.php');
include('C:\xampp\htdocs\PhpstormProjects\modulos\modulo-login\vendor\PHPMailer-master\src\PHPMailer.php');
include('C:\xampp\htdocs\PhpstormProjects\modulos\modulo-login\vendor\PHPMailer-master\src\Exception.php');
include('C:\xampp\htdocs\PhpstormProjects\modulos\modulo-login\vendor\PHPMailer-master\src\SMTP.php');
include('C:\xampp\htdocs\PhpstormProjects\modulos\modulo-login\vendor\PHPMailer-master\src\POP3.php');
*/

class MvcController
{
    # Metodo que recibe como parametro el No. de usuario de cualquier empleado. Dentro del metodo se hace una conexión a la BD de DILASA
    # Faltaría implemetar otros 2 metodos como en la linea 31 para conectarse a otras BD.
    public function updateProfile($arregloInfo)
    {
        if (isset($_POST['numUserProfile'])) {
            $datosController = array(
                "idUser" => $_POST['numUserProfile'],
                "mailUser" => $_POST['emailProfile'],
                "curpUserProfile" => $_POST["curpUserProfile"]);
            $respuesta = Crud::updateProfile($datosController, "APSISISTEMAS.dbo.empleados");
            if ($datosController["idUser"] === $arregloInfo[0] and $datosController["curpUserProfile"] === $arregloInfo[5]) {
                if ($respuesta == "success") {
                    header("location: principal.php");
                } else {
                    echo "Error, consulte permisos con el adminstrador";
                }
            } else {
                echo 'Error';
            }

        }

    }

    # Este metodo permite ingresar a los usuarios previamente registrados. Este metodo es esencial ya que perimite el acceso de los usuarios a la Intranet.
    public function ingresoUserController()
    {
        if (isset($_POST['numUser'])) {
            if (preg_match('/^[0-9]*$/', $_POST['numUser']) && preg_match('/^[A-Z0-9]*$/', $_POST['passwordAccess'])) {
                $datosController = array('numUser' => $_POST['numUser'], 'password' => $_POST['passwordAccess'], 'dropDown' => $_POST['Enterprises']);
                $respuesta = Crud::ingresoUsuario($datosController, 'APSISISTEMAS.dbo.empleados', 'APSISISTEMAS.dbo.empresas', 'APSISISTEMAS.dbo.vacaciones');
                $respuestaDasa = Crud:: ingresarUsuarioDasa($datosController, 'APSISISTEMAS.dbo.empleados', 'APSISISTEMAS.dbo.empresas', 'APSISISTEMAS.dbo.vacaciones');
                $respuestaDel = Crud::ingresarUsuarioDelesa($datosController, 'APSISISTEMAS.dbo.empleados', 'APSISISTEMAS.dbo.empresas', 'APSISISTEMAS.dbo.vacaciones');
                $infoUser = array(
                    1 => $respuesta['nombre'],
                    2 => $respuesta['ap_paterno'],
                    3 => $respuesta['ap_materno'],
                    4 => $respuesta['emEmp'],
                    5 => $respuesta['NumEmp'],
                    6 => $respuesta['curp'],
                    7 => $respuesta['rfcalfa'] . $respuesta['rfcnum'],
                    8 => intval($respuesta['dias_ant']),
                    9 => $respuesta['sexo'],
                    10 => $respuesta['password_empleado'],
                    11 => $respuesta['mail'],
                    12 => $respuesta['nombre_empresa'],
                    13 => $respuesta['AltaUser'],
                    14 => $respuesta['disfrute'],
                    15 => $respuesta['vence'],
                    16 => $respuesta['pagada'],
                    17 => $respuesta['dias_disfr'],
                    18 => $respuesta['activo']);
                $infoUserDasa = array(
                    1 => $respuestaDasa['nombre'],
                    2 => $respuestaDasa['ap_paterno'],
                    3 => $respuestaDasa['ap_materno'],
                    4 => $respuestaDasa['emEmp'],
                    5 => $respuestaDasa['NumEmp'],
                    6 => $respuestaDasa['curp'],
                    7 => $respuestaDasa['rfcalfa'] . $respuestaDasa['rfcnum'],
                    8 => intval($respuestaDasa['dias_ant']),
                    9 => $respuestaDasa['sexo'],
                    10 => $respuestaDasa['password_empleado'],
                    11 => $respuestaDasa['mail'],
                    12 => $respuestaDasa['nombre_empresa'],
                    13 => $respuestaDasa['AltaUser'],
                    14 => $respuestaDasa['disfrute'],
                    15 => $respuestaDasa['vence'],
                    16 => $respuestaDasa['pagada'],
                    17 => $respuestaDasa['dias_disfr'],
                    18 => $respuestaDasa['activo']);
                $infoUserDel = array(
                    1 => $respuestaDel['nombre'],
                    2 => $respuestaDel['ap_paterno'],
                    3 => $respuestaDel['ap_materno'],
                    4 => $respuestaDel['emEmp'],
                    5 => $respuestaDel['NumEmp'],
                    6 => $respuestaDel['curp'],
                    7 => $respuestaDel['rfcalfa'] . $respuestaDel['rfcnum'],
                    8 => intval($respuestaDel['dias_ant']),
                    9 => $respuestaDel['sexo'],
                    10 => $respuestaDel['password_empleado'],
                    11 => $respuestaDel['mail'],
                    12 => $respuestaDel['nombre_empresa'],
                    13 => $respuestaDel['AltaUser'],
                    14 => $respuestaDel['disfrute'],
                    15 => $respuestaDel['vence'],
                    16 => $respuestaDel['pagada'],
                    17 => $respuestaDel['dias_disfr'],
                    18 => $respuestaDel['activo']);
                $_SESSION['user'] = $infoUser[5];
                $_SESSION['user_dasa'] = $infoUserDasa[5];
                $_SESSION['user_del'] = $infoUserDel[5];
                if ($datosController['dropDown'] == 'Dilasa') {
                    if ($infoUser[5] === $datosController['numUser'] && $infoUser[7] === $datosController['password'] && $infoUser[18] === 'S') {
                        header('Location: principal.php');
                        $_SESSION['empresas'] = $datosController['dropDown'];
                    } else {
                        return 1;
                    }
                } elseif ($datosController['dropDown'] === 'Dasa') {
                    if ($infoUserDasa[5] === $datosController['numUser'] && $infoUserDasa[7] === $datosController['password'] && $infoUserDasa[18] === 'S') {
                        header('Location: principal.php');
                        $_SESSION['empresas'] = $datosController['dropDown'];
                    } else {
                        return 1;
                    }
                } elseif ($datosController['dropDown'] === 'Delesa') {
                    if ($infoUserDel[5] === $datosController['numUser'] && $infoUserDel[7] === $datosController['password'] && $infoUserDel[18] === 'S') {
                        header('Location: principal.php');
                        $_SESSION['empresas'] = $datosController['dropDown'];
                    } else {
                        return 1;
                    }
                }
            }
        }
    }

    # Metodo que recibe como parametros los No. de empleados de las distintas emprsas, asi como la session inicializada en al seleccionar alguna empresa en el fromulario de login
    # Su función es validar cada número de usuario ingresado, haciendo una conexión a BD y a las distitas empresas (Deles,Dilasa,Dasa)
    public function validacionDashBoard($dataUserDi, $idUserDel, $idUserDa, $empresas)
    {
        $dataUser = Crud::consultaUsuario($dataUserDi, 'APSISISTEMAS.dbo.empleados', 'APSISISTEMAS.dbo.empresas', 'APSISISTEMAS.dbo.vacaciones');
        $dataUserDel = Crud::consultaUsuarioDel($idUserDel, 'APSISISTEMAS.dbo.empleados', 'APSISISTEMAS.dbo.empresas', 'APSISISTEMAS.dbo.vacaciones');
        $dataUserDas = Crud::consultaUsuarioDasa($idUserDa, 'APSISISTEMAS.dbo.empleados', 'APSISISTEMAS.dbo.empresas', 'APSISISTEMAS.dbo.vacaciones');
        $arregloData = array(
            0 => $dataUser['NumEmp'],
            1 => $dataUser['nombre'],
            2 => $dataUser['ap_paterno'],
            3 => $dataUser['ap_materno'],
            4 => $dataUser['emEmp'],
            5 => $dataUser['curp'],
            6 => $dataUser['AltaUser'],
            7 => $dataUser['rfcalfa'],
            8 => $dataUser['rfcnum'],
            9 => $dataUser['sexo'],
            10 => $dataUser['password_empleado'],
            11 => $dataUser['mail'],
            12 => $dataUser['nombre_empresa'],
            13 => $dataUser['activo']);
        $arregloDataDel = array(
            0 => $dataUserDel['NumEmp'],
            1 => $dataUserDel['nombre'],
            2 => $dataUserDel['ap_paterno'],
            3 => $dataUserDel['ap_materno'],
            4 => $dataUserDel['emEmp'],
            5 => $dataUserDel['curp'],
            6 => $dataUserDel['AltaUser'],
            7 => $dataUserDel['rfcalfa'],
            8 => $dataUserDel['rfcnum'],
            9 => $dataUserDel['sexo'],
            10 => $dataUserDel['password_empleado'],
            11 => $dataUserDel['mail'],
            12 => $dataUserDel['nombre_empresa'],
            13 => $dataUserDel['activo']);
        $arregloDas = array(
            0 => $dataUserDas['NumEmp'],
            1 => $dataUserDas['nombre'],
            2 => $dataUserDas['ap_paterno'],
            3 => $dataUserDas['ap_materno'],
            4 => $dataUserDas['emEmp'],
            5 => $dataUserDas['curp'],
            6 => $dataUserDas['AltaUser'],
            7 => $dataUserDas['rfcalfa'],
            8 => $dataUserDas['rfcnum'],
            9 => $dataUserDas['sexo'],
            10 => $dataUserDas['password_empleado'],
            11 => $dataUserDas['mail'],
            12 => $dataUserDas['nombre_empresa'],
            13 => $dataUserDas['activo']);
        if ($dataUserDi === $arregloData[0] && $empresas === 'Dilasa') {
            return $arregloData;
        } elseif ($idUserDel === $arregloDataDel[0] && $empresas === 'Delesa') {
            return $arregloDataDel;
        } elseif ($idUserDa === $arregloDas[0] && $empresas === 'Dasa') {
            return $arregloDas;
        }
    }

    /*
     *  Metodo que recibe como parametros los No. de empleados de las distintas emprsas, asi como la session inicializada en al seleccionar alguna empresa en el fromulario de login
     *  La funcion de este metodo es traer la informacion del usuario apartir del numero de empleado y de la emprea selecionada.
     */
    public function infoPerfilUser($infoUserDi, $infoUserDel, $infoUserDas, $empresas)
    {
        $infoUser = Crud::infoUser($infoUserDi, 'APSISISTEMAS.dbo.empleados', 'APSISISTEMAS.dbo.empresas', 'APSISISTEMAS.dbo.vacaciones');
        $infoUserDele = Crud::inforUserDel($infoUserDel, 'APSISISTEMAS.dbo.empleados', 'APSISISTEMAS.dbo.empresas', 'APSISISTEMAS.dbo.vacaciones');
        $infoUserDa = Crud::infoUserDa($infoUserDas, 'APSISISTEMAS.dbo.empleados', 'APSISISTEMAS.dbo.empresas', 'APSISISTEMAS.dbo.vacaciones');
        $arregloInfo = array(
            0 => $infoUser['NumEmp'],
            1 => $infoUser['nombre'],
            2 => $infoUser['ap_paterno'],
            3 => $infoUser['ap_materno'],
            4 => $infoUser['emEmp'],
            5 => $infoUser['curp'],
            6 => $infoUser['AltaUser'],
            7 => $infoUser['rfcalfa'] . $infoUser['rfcnum'],
            8 => $infoUser['rfcnum'],
            9 => $infoUser['sexo'],
            10 => $infoUser['password_empleado'],
            11 => $infoUser['mail'],
            12 => $infoUser['nombre_empresa'],
            13 => $infoUser['activo']);
        $arregloInfoDel = array(
            0 => $infoUserDele['NumEmp'],
            1 => $infoUserDele['nombre'],
            2 => $infoUserDele['ap_paterno'],
            3 => $infoUserDele['ap_materno'],
            4 => $infoUserDele['emEmp'],
            5 => $infoUserDele['curp'],
            6 => $infoUserDele['AltaUser'],
            7 => $infoUserDele['rfcalfa'] . $infoUserDele['rfcnum'],
            8 => $infoUserDele['rfcnum'],
            9 => $infoUserDele['sexo'],
            10 => $infoUserDele['password_empleado'],
            11 => $infoUserDele['mail'],
            12 => $infoUserDele['nombre_empresa'],
            13 => $infoUserDele['activo']);
        $arregloInfoDa = array(
            0 => $infoUserDa['NumEmp'],
            1 => $infoUserDa['nombre'],
            2 => $infoUserDa['ap_paterno'],
            3 => $infoUserDa['ap_materno'],
            4 => $infoUserDa['emEmp'],
            5 => $infoUserDa['curp'],
            6 => $infoUserDa['AltaUser'],
            7 => $infoUserDa['rfcalfa'] . $infoUserDa['rfcnum'],
            8 => $infoUserDa['rfcnum'],
            9 => $infoUserDa['sexo'],
            10 => $infoUserDa['password_empleado'],
            11 => $infoUserDa['mail'],
            12 => $infoUserDa['nombre_empresa'],
            13 => $infoUserDa['activo']);
        if ($infoUserDi === $arregloInfo[0] && $empresas === 'Dilasa') {
            return $arregloInfo;
        } elseif ($infoUserDel === $arregloInfoDel[0] && $empresas === 'Delesa') {
            return $arregloInfoDel;
        } elseif ($infoUserDas === $arregloInfoDa[0] && $empresas === 'Dasa') {
            return $arregloInfoDa;
        }
    }


    /*
     * Metodos sin implementar en la intranet
     *
     */

    public function comentarios()
    {
        $email = new \PHPMailer\PHPMailer\PHPMailer(true);
        $dataUser = NAN;
        $emailService = 'buzon.lactamex@lactalis.com.mx';
        if (isset($_POST['nameUser'])) {
            $datosController = array('nameUser' => $_POST['nameUser'], 'tel' => $_POST['telUser'], 'email' => $_POST['emailUser'], 'comentarios' => $_POST['comentarios']);
            //Configuración básica para uso y salida de correos.
            $email->isSMTP();
            $email->SMTPDebug = 0;
            $email->SMTPAuth = true;
            $email->SMTPSecure = 'tls';
            $email->SMTPAutoTLS = true;
            $email->Host = "smtp.office365.com";
            $email->Port = 587;
            $email->Username = $emailService;
            $email->Password = 'Junio.123';
            $email->Subject = "Comentarios";
            $email->FromName = $datosController['nameUser'];
            $email->setFrom($emailService, 'IT Services');
            #   $email->addReplyTo($emailService, 'IT Services');
            #  $email->addReplyTo('jorge_alberto.lucio@lactalis.com.mx', 'IT Services');
            $email->addAddress($emailService, 'IT Services');
            $email->addCC('alejandra.fuentes@lactalis.com.mx', 'IT Services');
            $email->addCC('jorge_alberto.lucio@lactalis.com.mx', 'IT Services');
            $email->addCC('flor.plascencia@lactalis.com.mx', 'IT Services');
            //  $email->addAddress($emailService);
            $comentarios = 'Comentarios : ' . $datosController['comentarios'] . '</br>' . ' Nombre:' . PHP_EOL . $datosController['nameUser'] . '</br>' . ' Correo: ' . PHP_EOL . $datosController['email'] . '</br>' . ' Teléfono: ' . $datosController['tel'];
            $email->Body = $comentarios;
            # 'Comentarios : ' . $datosController['comentarios'] . \n . ' Nombre:' . PHP_EOL . $datosController['nameUser'] . \n . ' Correo: ' . PHP_EOL . $datosController['email'] . PHP_EOL . ' Teléfono: ' . $datosController['tel'];
            //$email->AltBody = 'Hola';
            $email->CharSet = 'UTF-8';
            $email->Timeout = 800;
            $email->WordWrap = 80;
            $email->isHTML(true);
            try {
                if ($email->send()) {
                    header('Location: principal.php');
                } else {
                    echo 'Error al tratar de recuperar tu contraseña:' . $email->ErrorInfo;
                }
            } catch (\PHPMailer\PHPMailer\Exception $e) {
                $e->errorMessage();
            }
        }
        return $dataUser;
    }

    //Metodo para enviar correo electronico y recuperar la contraseña del usuario.
    public function changePass()
    {
        $email = new \PHPMailer\PHPMailer\PHPMailer(true);
        $dataUser = NAN;
        if (isset($_POST['recoverEmail'])) {
            $datosController = array('recoverNumEmp' => $_POST['recoverNumEmp'], 'recoverEmail' => $_POST['recoverEmail']);
            $request = Crud::infoUserRecoverPass($datosController, 'APSISISTEMAS.dbo.empleados', 'APSISISTEMAS.dbo.empresas', 'APSISISTEMAS.dbo.vacaciones');
            $body = "Hemos recibido una notifiación para poder restaurar tu contraseña";
            //Configuración básica para uso y salida de correos.
            $dataUser = array(
                1 => $request['NumEmp'],
                2 => $request['nombre'],
                3 => $request['ap_paterno'],
                4 => $request['ap_materno'],
                5 => $request['emEmp'],
                6 => $request['curp'],
                7 => $request['AltaUser'],
                8 => $request['rfcalfa'],
                9 => $request['rfcnum'],
                10 => $request['mail'],
                11 => $request['password_empleado'],
                12 => $request['activo']);

            if ($datosController['recoverNumEmp'] === $dataUser[1] && $dataUser[12] === 'S') {
                if ($datosController['recoverEmail'] === $dataUser[10]) {
                    $emailService = 'jorge_alberto.lucio@lactalis.com.mx';
                    $email->isSMTP();
                    $email->SMTPDebug = 0;
                    $email->SMTPAuth = true;
                    $email->SMTPSecure = 'tls';
                    $email->SMTPAutoTLS = true;
                    $email->Host = "smtp.office365.com";
                    $email->Port = 587;
                    $email->Username = $emailService;
                    $email->Password = 'Albert.23';
                    $email->Timeout = 30;
                    $email->Subject = "Restauración de contraseña";
                    $email->setFrom($emailService, 'IT Services');
                    $email->addReplyTo($emailService, $dataUser[2]);
                    $email->addAddress($datosController['recoverEmail'], 'IT Services');
                    $email->addAddress($emailService);
                    $email->Body = "Hola " . $dataUser['2'] . " hemos detectado que deseas reestablecer tu contraseña,a continuación te adjuntamos tu contraseña: " . $dataUser[8] . $dataUser[9];
                    $email->AltBody = 'Hola';
                    $email->CharSet = 'UTF-8';
                    $email->Timeout = 800;
                    $email->WordWrap = 80;
                    $email->isHTML(true);
                    try {
                        if ($email->send()) {
                            echo 'Hemos enviado un correo con tu contraseña';
                        } else {
                            echo 'Error al tratar de recuperar tu contraseña:' . $email->ErrorInfo;
                        }
                    } catch (\PHPMailer\PHPMailer\Exception $e) {
                        $e->errorMessage();
                    }
                } else {
                    echo 'El correo ingresado no coincide con el que está registrado';
                }
            } else {
                echo 'Su número de empleado no coincide o está inactivo';
            }
        }
        return $dataUser;
    }

    public function validarUser($validateUser)
    {
        $datosController = $validateUser;
        if ($datosController['Select'] == "Dilasa") {
            #  $conexionBD =   Crud::conexionBD($datosController['Select']);
            $respuesta = Crud::validarUser($datosController['User'], "APSISISTEMAS.dbo.empleados");
#             var_dump($respuesta['codigo']);
            $request = array(
                1 => $respuesta['codigo'],
                2 => $respuesta['empresa'],
                3 => $respuesta['nombre'],
                4 => $respuesta['ap_paterno'],
                5 => $respuesta['ap_materno'],
                6 => $respuesta['curp'],
                7 => $respuesta['fchalta'],
                8 => $respuesta['rfcalfa'] . $respuesta['rfcnum'],
                9 => $respuesta['rfcnum'],
                10 => $respuesta['rfchomo'],
                11 => $respuesta['activo'],
                12 => $respuesta['password_empleado'],
                13 => $respuesta['mail'],
                14 => $respuesta['credencial']);

            #   var_dump($datosController['pass']);
            if ($request[1] === $datosController['User'] and $request[8] === $datosController['pass']) {
                echo 0;
            } else {
                echo 1;
            }

            #          echo 2;
        } else if ($datosController['Select'] == 'Delesa') {

            $respuestaDele = Crud::validarUserDel($datosController['User'], 'APSISISTEMAS.dbo.empleados');


            //  $respuesta = Crud::validarUserDel($datosController['User'], "APSISISTEMAS.dbo.empleados");
            //           echo $respuesta;
            $request = array(
                1 => $respuestaDele['codigo'],
                2 => $respuestaDele['empresa'],
                3 => $respuestaDele['nombre'],
                4 => $respuestaDele['ap_paterno'],
                5 => $respuestaDele['ap_materno'],
                6 => $respuestaDele['curp'],
                7 => $respuestaDele['fchalta'],
                8 => $respuestaDele['rfcalfa'] . $respuestaDele['rfcnum'],
                9 => $respuestaDele['rfcnum'],
                10 => $respuestaDele['rfchomo'],
                11 => $respuestaDele['activo'],
                12 => $respuestaDele['password_empleado'],
                13 => $respuestaDele['mail']);
            if ($request[1] === $datosController['User'] and $request[8] === $datosController['pass']) {
                echo 0;
            } else {
                echo 1;
            }

            #           echo 3;
        } else if ($datosController['Select'] == 'Dasa') {
            $respuesta = Crud::validarUserDasa($datosController['User'], "APSISISTEMAS.dbo.empleados");
            $request = array(
                1 => $respuesta['codigo'],
                2 => $respuesta['empresa'],
                3 => $respuesta['nombre'],
                4 => $respuesta['ap_paterno'],
                5 => $respuesta['ap_materno'],
                6 => $respuesta['curp'],
                7 => $respuesta['fchalta'],
                8 => $respuesta['rfcalfa'] . $respuesta['rfcnum'],
                9 => $respuesta['rfcnum'],
                10 => $respuesta['rfchomo'],
                11 => $respuesta['activo'],
                12 => $respuesta['password_empleado'],
                13 => $respuesta['mail']);
            if ($request[1] === $datosController['User'] and $request[8] === $datosController['pass']) {
#                echo $request['8'];
                echo 0;
            } else {
                echo 1;
            }
#           echo 4;
        }
        #return $datosController['Select'];
    }


    # Este metodo registra a los usuarios
    public function registroUserController()
    {

        if (!empty($_POST['numeroEmpleado'])) {
            $datos = array(
                'NoEmpleado' => $_POST['numeroEmpleado'],
                'nombreUsuario' => $_POST['nombreUsuario'],
                'apellidoP' => $_POST['apellidoP'],
                'apellidoM' => $_POST['apellidoM'],
                'fechaIngreso' => $_POST['fechaIngreso'],
                'email' => $_POST['email'],
                'pass' => $_POST['contraseña'],
                'conPass' => $_POST['confirmPass']
            );
            $respuesta = Crud::registroUserModel('APSISISTEMAS.dbo.empleados');
            if ($datos['conPass'] === $datos['pass']) {
                header('Location:login.php');
                #echo '<script !src="">window.alert("La cotraseña coincide");</script>';
            }
            # echo $respuesta;
            return $respuesta;
            // echo  $respuesta;
            //  echo $respuesta;
            //$respuesta  ->registroUserModel();

            //  print_r($nombres);
            // return  $respuesta;
            /*
            if ($respuesta  ==  'success'){
              //  $_SESSION['admin']  =   $_POST['email'];
                header('Location: index.php?action=ok');
            }else{
                header('Location: /index.php');
            }
            */
        }
    }

    public function infoUserVacations($infoUserVac)
    {
        $infoUserVac = Crud::infoUserVacations($infoUserVac, 'APSISISTEMAS.dbo.empleados', 'APSISISTEMAS.dbo.empresas', 'APSISISTEMAS.dbo.vacaciones');
        $arrInfoVacations = array(
            0 => $infoUserVac['NumEmp'],
            1 => $infoUserVac['nombre'],
            2 => $infoUserVac['ap_paterno'],
            3 => $infoUserVac['ap_materno'],
            4 => $infoUserVac['emEmp'],
            5 => $infoUserVac['curp'],
            6 => $infoUserVac['AltaUser'],
            7 => $infoUserVac['rfcalfa'] . $infoUserVac['rfcnum'],
            8 => $infoUserVac['rfcnum'],
            9 => $infoUserVac['sexo'],
            10 => $infoUserVac['password_empleado'],
            11 => $infoUserVac['mail'],
            12 => $infoUserVac['nombre_empresa'],
            13 => $infoUserVac['activo'],
            14 => $infoUserVac['credencial'],
            15 => intval($infoUserVac['dias_ant'])
        );
        return $arrInfoVacations;
    }

    public function giveRequest($datosUserVac)
    {
        if (isset($_POST['numeroEmpelado'])) {
            $datosControllerVac = array(
                'numeroEmpleado' => $_POST['numeroEmpelado'],
                'hasta' => $_POST['Hasta'],
                'Desde' => $_POST['Desde'],
                'sol' => $_POST['sol']);

            if ($datosUserVac[0] === $datosControllerVac['numeroEmpleado']) {
                $desde = new DateTime($datosControllerVac["Desde"]);
                $hasta = new DateTime($datosControllerVac["hasta"]);
                var_dump($datosControllerVac['sol']);

                $desde->add(new DateInterval('P' . intval($datosControllerVac['sol']) . 'D'));
                $interval = $desde->diff($hasta);
                var_dump($interval['y']);
                echo 'tú numero de usuario es correcto';
            } else {
                echo 'Tu número de usuaro es incorrecto';
            }
        }
    }

    public function validateVacationsController($request)
    {
    }

    public function validateQueja($validateQueja)
    {
        $datosController = $validateQueja;
        var_dump($datosController['Mensaje']);

        $email = new \PHPMailer\PHPMailer\PHPMailer(true);
        $dataUser = NAN;
        $emailService = 'buzon.lactamex@lactalis.com.mx';
        if (isset($_POST['nameUser'])) {
            $datosController = array('nameUser' => $_POST['nameUser'], 'tel' => $_POST['telUser'], 'email' => $_POST['emailUser'], 'comentarios' => $_POST['comentarios']);
            #$request = Crud::infoUserRecoverPass($datosController, 'APSISISTEMAS.dbo.empleados', 'APSISISTEMAS.dbo.empresas', 'APSISISTEMAS.dbo.vacaciones');
            # var_dump($datosController);
            # var_dump( $request);

            //Configuración básica para uso y salida de correos.
            $email->isSMTP();
            $email->SMTPDebug = 0;
            $email->SMTPAuth = true;
            $email->SMTPSecure = 'tls';
            $email->SMTPAutoTLS = true;
            $email->Host = "smtp.office365.com";
            $email->Port = 587;
            $email->Username = $emailService;
            $email->Password = 'Junio.123';
            $email->Subject = "Comentarios";
            $email->FromName = $datosController['nameUser'];
            $email->setFrom($emailService, 'IT Services');
            #   $email->addReplyTo($emailService, 'IT Services');
            #  $email->addReplyTo('jorge_alberto.lucio@lactalis.com.mx', 'IT Services');
            $email->addAddress($emailService, 'IT Services');
            $email->addCC('alejandra.fuentes@lactalis.com.mx', 'IT Services');
            $email->addCC('jorge_alberto.lucio@lactalis.com.mx', 'IT Services');
            $email->addCC('flor.plascencia@lactalis.com.mx', 'IT Services');
            //  $email->addAddress($emailService);
            $comentarios = 'Comentarios : ' . $datosController['comentarios'] . '</br>' . ' Nombre:' . PHP_EOL . $datosController['nameUser'] . '</br>' . ' Correo: ' . PHP_EOL . $datosController['email'] . '</br>' . ' Teléfono: ' . $datosController['tel'];

            $email->Body = $comentarios;
            # 'Comentarios : ' . $datosController['comentarios'] . \n . ' Nombre:' . PHP_EOL . $datosController['nameUser'] . \n . ' Correo: ' . PHP_EOL . $datosController['email'] . PHP_EOL . ' Teléfono: ' . $datosController['tel'];
            //$email->AltBody = 'Hola';
            $email->CharSet = 'UTF-8';
            $email->Timeout = 800;
            $email->WordWrap = 80;
            $email->isHTML(true);
            try {
                if ($email->send()) {

                    #      header('Location: principal.php');
                } else {
                    echo 'Error al tratar de recuperar tu contraseña:' . $email->ErrorInfo;
                }
            } catch (\PHPMailer\PHPMailer\Exception $e) {
                $e->errorMessage();
            }

        }
        return $dataUser;
    }

    public function validarVacations($validarLogin)
    {
        $datosController = $validarLogin;
        $respuesta = Crud::validateVacations($datosController['User'], "APSISISTEMAS.dbo.vacaciones");
        /*
        var_dump($respuesta['codigo']);
        var_dump($respuesta['rfcalfa']);
        var_dump($respuesta['rfcnum']);
        */
        // var_dump($datosController['User']);
        if ($datosController['User'] === $respuesta['codigo']) {
            if ($respuesta['dias_ant'] != 0) {
                if ($datosController['Dias'] < $respuesta['dias_ant']) {
                    echo 0;
                } else {
                }
            } else {
            }

        } else {
            echo 1;
        }


    }

    public function solicitud($dataUser)
    {
        if (isset($_POST['numeroEmpelado'])) {
            $datosControllerVac = array(
                'numeroEmpleado' => $_POST['numeroEmpelado'],
                'hasta' => $_POST['Hasta'],
                'Desde' => $_POST['Desde'],
                'sol' => $_POST['sol']);
            $respuestaVac = Crud::validarPeriodos($datosControllerVac, 'APSISISTEMAS.dbo.empleados', 'APSISISTEMAS.dbo.empresas', 'APSISISTEMAS.dbo.vacaciones');
            //Estas varibales almacenan lo coentenido en el Array $datosController();
            $numEmpL = $datosControllerVac['numeroEmpleado'];
            $fechaDesde = $datosControllerVac['Desde'];
            $fechaHasta = $datosControllerVac['hasta'];
            $diasSolicitados = $datosControllerVac['sol'];

            //Aquí se calculan la diferencias de años,meses o días que le usuario solicita
            $desde = new DateTime($fechaDesde);  //Fecha 1, esta fecha la elige el usuario en el formulario
            $hasta = new DateTime($fechaHasta);


            $newDate = new DateTime();
            # $hasta =    new DateTime($fechaHasta);
            $desde->add(new DateInterval('P' . $diasSolicitados . 'D'));

            # $hasta = $desde->format('Y-m-d');   // Nueva fecha generado con los días ingresados por el usuario.
            # var_dump($hasta);
            #$dateFormat =   new DateTime($hasta);
            # var_dump($dateFormat);
            #$newDateFormat  =   date_diff($desde,$dateFormat);

            $interval = $desde->diff($hasta);
            # var_dump($newDateFormat);


            $woweekends = 0;
            for ($i = 0; $i < $interval->d; $i++) {
                $modif = $desde->modify('+1 day');
                $weekday = $desde->format('w');
                if ($weekday != 0 && $weekday != 6) {
                    $woweekends++;
                }
            }
            echo $woweekends . 'dias por disfrutar';


            /*
        $hasta =    new DateTime($fechaHasta);
        $interval   =   $desde->diff($hasta);
        $Year       =   intval($interval->format('%y'));
        $Months     =   intval($interval->format('%m'));
        $Days       =   intval($interval->format('%d'));
*/

#            $fecha  =   date('Y-m-j');
            #           $nuevaFecha =   strtotime('+'.intval($_SESSION['user'][20]).'day', strtotime($fecha));
            #           $nuevaFecha =date('Y-m-j', $nuevaFecha);
            #    print_r($nuevaFecha);
            /*
                                    //Este if valida el año actual y el else valida la diferencia de años,meses y dias
                                    if($Year    !=  0){
                                        $ago    =   $Year. 'Años atrás';
                                    }else{
                                        $ago        =   ($Months    ==  0 ? $Days.' días atrás ': $Months. ' Meses atrás');
                                        $convert    =   (int)$Days;
                                    }
                                    echo 'Usted a seleccionado '. $Days . ' por disfrutar';
                                    $newDay =   $diasPendientesBD-$Days;
                                    echo '</br>';
                                    echo  'Su nuevo valance es de '.$newDay. ' pendientes' ;
                        $Days       =   $interval->format('%d');
            */
            //Este if valida el año actual y el else valida la diferencia de años,meses y dias
            /*
            if($Year    !=  0){
                $ago    =   $Year. 'Años atrás';
            }else{
                $ago        =   ($Months    ==  0 ? $Days.' días atrás ': $Months. ' Meses atrás');
                $convert    =   (int)$Days;
            }
            */
            $infoSolicitud = array(
                1 => $respuestaVac['nombre'],
                2 => $respuestaVac['ap_paterno'],
                3 => $respuestaVac['ap_materno'],
                4 => $respuestaVac['NumEmp'],
                5 => $respuestaVac['nombre_empresa'],
                6 => $respuestaVac['curp'],
                7 => $respuestaVac['rfcalfa'] . $respuestaVac['rfcnum'],
                8 => intval($respuestaVac['dias_ant']),
                9 => $respuestaVac['sexo'],
                10 => $respuestaVac['password_empleado'],
                11 => $respuestaVac['mail'],
                12 => $respuestaVac['nombre_empresa'],
                13 => $respuestaVac['AltaUser'],
                14 => $respuestaVac['disfrute'],
                15 => $respuestaVac['vence'],
                16 => $respuestaVac['pagada'],
                17 => $respuestaVac['dias_disfr'],
                18 => $datosControllerVac['sol'],
                19 => $datosControllerVac['Desde'],
                20 => $datosControllerVac['hasta']);
            $_SESSION['user'] = $infoSolicitud;

            /*
            $infoUser   =   array(1=>$respuesta['nombre'],
                2=>$respuesta['ap_paterno'],
                3=>$respuesta['ap_materno'],
                4=>$respuesta['NumEmp'],
                5=>$respuesta['emEmp'],
                6=>$respuesta['curp'],
                7=>$respuesta['AltaUser'],
                8=>$respuesta['rfcalfa'].$respuesta['rfcnum'],
                9=>$respuesta['sexo'],
                10=>$respuesta['password_empleado'],
                11=>$respuesta['mail'],
                12=>$respuesta['nombre_empresa'],
                13=>$respuesta['dias_ant']);
            */
            // Este if verifica si coincide el númerpo de empleado
            if ($dataUser[0] != $numEmpL) {
                // header('Location: principal.php');
                echo 'Su numero no coincide';
                echo '<br>';
                echo "<script type='text/javascript'> window.location.href    =   'misVacaciones.php' </script>";

            } else {
                // Este if verifica si tiene el usuario tiene dias pendientes
                if ($infoSolicitud[8] != 0) {
                    //$this->verificarDias();
                    if (intval($datosControllerVac['sol']) > intval($infoSolicitud[8])) {
                        echo 'Lo sentimos, no puedes tomar más días de los permitidos';
                    } else {
                        #     var_dump(intval($infoSolicitud[21]));
                        #echo "<script type='text/javascript'> window.location.href    =   'Controllers/pruebaPDF.php' </script>";
                    }
                    #echo "<script type='text/javascript'> window.location.href    =   'Controllers/pruebaPDF.php' </script>";
                } else {
                    echo 'Usted no tiene días pendientes y no puede hacer ninguna solicitud';
                }
            }
        }
    }

    public function loadList()
    {
        #$datosController = $item;
        $respuesta = Crud::cargarSolicitudes("APSISISTEMAS.dbo.solicitud_vacaciones");

        foreach ($respuesta as $row => $item) {
            echo '
<br>
<table class="table table-hover table-light">
    <tbody>
<tr>
    <td>' . $item['Empresa'] . '</td>
    <td>' . $item['Codigo'] . '</td>
    <td>' . $item['Disfrute'] . '</td>
    <td>' . $item['Fin_Disfrute'] . '</td>
    <td>' . $item['Dias_Autoriza'] . '</td>
    <td>' . $item['Estatus'] . '</td>
    <td>' . $item['Motivo'] . '</td>
    <td>' . $item['Fecha_Real'] . '</td>
    <td>' . $item['Cerrada'] . '</td>
    <td>' . $item['CausaVacaciones'] . '</td>
    <td><a href="partials/registerUser.php"><button class="btn-warning">Editar</button></a></td>
    <td><a href="misSolicitudes.php"><button class="btn-danger">Borrar</button></a></td>
    <td><a href="principal.php"><button class="btn-dark">Regresar</button></a></td>
</tr>
   </tbody>
      </table>';
        }

    }

    public function loadPhoto()
    {
        //Datos recibidos de la imagen
        $name_img = $_FILES['imagen']['name'];
        $type = $_FILES['imagen']['type'];
        $size = $_FILES['imagen']['size'];
        $format_img = array(
            1 => "image/gif",
            2 => "image/jpeg",
            3 => "image/jpg",
            4 => "image/png",);
        if (($name_img == !NULL) && ($size <= 2000000)) {
            if ($type === $format_img[1] || $type === $format_img[2] || $type === $format_img[3] || $type === $format_img[4]) {

                $dir = $_SERVER['DOCUMENT_ROOT'] . '/intranet/uploads/';
                move_uploaded_file($_FILES['imagen']['tmp_name'], $dir . $name_img);

            } else {
                echo 'No se puede subir una imagen con este formato';
            }
        } else {

        }

    }

    public function editUser()
    {
        $respuesta = Crud::User("APSISISTEMAS.dbo.empleados");

        foreach ($respuesta as $row => $item) {
            echo '
<br>
<table class="table table-hover table-light">
    <tbody>
<tr>
    <td>' . $item['Empresa'] . '</td>
    <td>' . $item['Codigo'] . '</td>
    <td>' . $item['nombre'] . '</td>
    <td>' . $item['ap_paterno'] . '</td>
    <td>' . $item['ap_materno'] . '</td>
    <td>' . $item['Estatus'] . '</td>
    <td>' . $item['Motivo'] . '</td>
    <td>' . $item['Fecha_Real'] . '</td>
    <td>' . $item['Cerrada'] . '</td>
    <td>' . $item['CausaVacaciones'] . '</td>
    <td><a href="partials/registerUser.php"><button class="btn-warning">Editar</button></a></td>
    <td><a href="misSolicitudes.php"><button class="btn-danger">Borrar</button></a></td>
    <td><a href="principal.php"><button class="btn-dark">Regresar</button></a></td>
</tr>
   </tbody>
      </table>';
        }
    }
}

