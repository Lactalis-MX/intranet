<?php
require_once 'Controllers/Controller.php';
require_once 'Models/Crud.php';
session_start();
$solicitud = new MvcController();
$solicitud->ingresoUserController();
if (!isset($_SESSION['user'])  && !isset($_SESSION['user_dasa']) && !isset($_SESSION['user_del'])) {
    header('Location: index.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Nosotros</title>
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    <script src="assets/js/jquery-ui-1.12.1.custom/jquery-ui.js"></script>
    <script src="assets/js/sliderDash.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="assets/js/envioEmail.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/dynamicDashboard.js"></script>


    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/imgStyle.css">
    <!--<link rel="stylesheet" href="http://csshake.surge.sh/csshake.css">-->
    <link rel="stylesheet" href="assets/css/styleDashBoard.css">
    <link rel="stylesheet" href="assets/css/sliderStyle.css">
    <link rel="stylesheet" href="assets/css/menuStyle.css">
    <link rel="stylesheet" href="assets/css/bannerStile.css">
    <link rel="stylesheet" href="assets/css/footerStyle.css">


    <!-- Custom styles for this template -->
    <link href="css/modern-business.css" rel="stylesheet">
    <link rel="icon" href="img/grupo_lactalis.png" type="image/gif" sizes="16x16">

</head>

<body>

<!-- Navigation -->
<nav class="navbar fixed-top navbar-expand-lg navbar- bg fixed-top" style="background: #77c5e7">
    <div class="container">
        <a class="navbar-brand" href="principal.php" id="imgLogoLactalis"><img src="img/lactalis_mexico.jpg" alt=""
                                                                               class="float-lg-left w-50"
                                                                               id="img_logo_home"></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="acerca.php" style="color: #FFFFFF;font-size: 18px">Acerca de nosotros</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="contactUs.php" style="color: #FFFFFF;font-size: 18px"">Contáctanos</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="historia.php" style="color: #FFFFFF;font-size: 18px"">Historia</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="principal.php#title" style="color: #FFFFFF;font-size: 18px"">Comunicados</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="lactajob.php" style="color: #FFFFFF;font-size: 18px"">LactaJob</a>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false" style="color: #FFFFFF;font-size: 18px"">
                    Formatos
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                        <a class="dropdown-item" href="intranet/Formatos/Formato%20de%20Servicios%20IT.pdf">Formato de servicios IT</a>
                        <a class="dropdown-item" href="intranet/Formatos/FORMATO%20PRESTAMOS.pdf">Formato de préstamos</a>
                        <a class="dropdown-item" href="intranet/Formatos/FORMATO%20VACACIONES.pdf">Formato de vacaciones</a>
                        <a class="dropdown-item" href="intranet/Formatos/Anexo%201%20-Requisición%20de%20Personal.pdf">Requisición de Personal</a>
                        <a class="dropdown-item" href="intranet/Formatos/Formato Entrevista de Salida Nuevo V04062019.pdf">Formato  Entevista de Salida</a>
                        <a class="dropdown-item" href="intranet/Formatos/FORMATO%20DE%20MOVIM.%20DE%20PERSONAL.PDF">Formato Movimiento de Personal</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false" style="color: #FFFFFF;font-size: 18px"">
                    Politicas
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                        <a class="dropdown-item" href="intranet/POLITICAS/Anexo%201%20-Requisición%20de%20Personal.pdf">Política Requisición de Personal</a>
                        <a class="dropdown-item" href="intranet/POLITICAS/DRH-P012-POLITICA%20UBER%20FOR%20BUSINESS.pdf">Política Uber para Negocios</a>
                        <a class="dropdown-item" href="intranet/POLITICAS/Política%20de%20viajes%20nacionales%20e%20internacionales.pdf">Política de Viajes</a>
                        <a class="dropdown-item" href="intranet/POLITICAS/CODIGO%20DE%20CONDUCTA.PDF">Código de Conducta</a>
                        <a class="dropdown-item" href="intranet/POLITICAS/Comunicado Interno - Movimientos de Personal.pdf">Movimiento de Personal</a>
                    </div>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false" style="color: #FFFFFF;font-size: 18px"">
                    Configuración
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                        <a class="dropdown-item" href="profile.php">Perfil</a>
                        <a class="dropdown-item" href="Controllers/cerrarSesion.php">Cerrar sesión</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
<br><br><br>


<!-- Page Content -->
<div class="container">

    <!-- Page Heading/Breadcrumbs -->
    <h1 class="mt-4 mb-3" style="">
        <b>Acerca de nosotros</b>
    </h1>

    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="principal.php">Inicio</a>
        </li>
        <li class="breadcrumb-item active">Acerca de</li>
    </ol>

    <!-- Intro Content -->
    <div class="row">
        <div class="col-lg-6">
            <img class="img-fluid rounded mb-4" src="img/img_acerca.jpg" alt="">
        </div>
        <div class="col-lg-6">
            <h2 style="">¿Quiénes somos?</h2>
            <p>Fundado en 1933 por André Besnier en Laval (Francia), el Grupo Lactalis ha sido fiel, durante tres
                generaciones, a su compromiso inicial: la transformación de los productos lácteos en todas sus formas.
            </p>
            <p>
                Empresa familiar, crecemos por todo el mundo, manteniéndonos fieles a nuestra historia y a nuestro
                patrimonio.
            </p>
            <h3 style="">Misión</h3>
            <p>Ser una de las compañías líderes en lácteos México, con un crecimiento rentable, ofreciendo productos con
                la mejor relación precio – calidad. </p>

            <h3 style="">Visión</h3>
            <p>Alimentar el corazón de nuestros consumidores con productos de la mejor calidad, siendo siempre fieles a
                nuestros valores y al cuidado de nuestra gente.</p>
        </div>
    </div>
    <!-- /.row -->

    <!-- Team Members -->

    <div class="row">
        <h2 style="">
            <img src="img/img_historia/valores.png" alt=""
                 class="img-responsive col-md-4 col-md-4" id="imgValores">
        </h2>
        <div id="forma_valores" class="col-md-4 col-md-4">
            <div class="card-body" id="cont_ambicion" style="color: #FFFFFF">
                <h3 style="font-family: 'Comic Sans MS'">Ambición</h3>

                <p>
                    <strong>Alto desempeño</strong>
                    Defino objetivos ambiciosos para entregar los resultados necesarios para el
                    negocio.

                </p>

                <p>
                    <strong>Mejora continua</strong>
                    Busco siempre todas las oportunidades de mejora.
                </p>

                <p>
                    <strong>Autodesarrollo y
                        de equipo</strong>
                    Identifico áreas y situaciones para mi propio desarrollo y mis colegas.

                </p>

                <p>

                </p>


            </div>

        </div>

        <div class="col-lg-4 col-lg-4" id="forma_valores_compromiso">
            <div class="card-body" id="cont_compromiso" style="color:  #FFFFFF">
                <h3 class="" style="">Compromiso</h3>

                <p>
                    <strong>Espíritu empresarial</strong>
                    Gerencio el negocio como si fuera mío.

                </p>

                <p>
                    <strong>Resiliencia / Determinación</strong>
                    Estoy decidido para superar los obstáculos. 

                </p>

                <p>
                    <strong>Rendición de cuentas</strong>
                    Asumo la responsabilidad de mis actos y presto cuentas de forma eficiente.

                </p>

                <p>
                    <strong>Lealtad</strong>
                    Apoyo y protejo a la empresa con integridad y respeto.

                </p>


            </div>

        </div>

        <div class="col-lg-4 col-lg-4" id="forma_valores_sencillez">
            <div class="card-body" id="cont_sencilles" style="color: #FFFFFF">
                <h3 style="font-family: 'Comic Sans MS'">con Sencillez</h3>

                <p>
                    <strong>Accesibilidad</strong>
                    Creo buenas relaciones basadas en la accesibilidad, la modestia y la afinidad. 

                </p>

                <p>
                    <strong>Transparencia</strong>
                    Actúo y comunico de modo
                    claro y transparente. 
                </p>
                <p>
                    <strong>Práctica con resultados</strong>
                    Trabajo de modo concreto, simple, eficaz y con sentido común. 

                </p>

            </div>

        </div>
    </div>
    <br><br>

</div>


<!-- Footer -->
<footer class="py-5 bg" style="background: #77c5e7">
    <div class="container" style="color: #FFFFFF">
        © 2018 Copyright:
        <a href="http://www.esmeralda.com.mx/" style="color: #FFFFFF"> Esmeralda.com.mx</a>
        <p>todos los derechos reservados</p>
        <div class="col-lg-12 py-lg-5">
            <div class="mb-3 flex-center">
                <a class="fb-ic"
                   href="https://www.facebook.com/pages/category/Industrial-Company/Vacantes-Lactalis-M%C3%A9xico-281980632337467/">
                    <img src="img/facebook.png" alt="">
                </a>
                <a class="tw-ic" href="https://twitter.com/groupe_lactalis?lang=en">
                    <img src="img/twitter.png">
                </a>

                <a class="ins-ic" href="https://www.instagram.com/galbanicheese/?hl=en">
                    <img src="img/instagram.png">
                </a>

            </div>
        </div>
    </div>
    <!-- /.container -->
</footer>

</body>

</html>
