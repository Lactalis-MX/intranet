<?php
include_once "Models/Crud.php";
include_once "Controllers/Controller.php";
$validarLogin = new MvcController();
$login = new MvcController();
$login->ingresoUserController();
if (!isset($_SESSION['user'])){
    header("Location: login.php");
}
?>
<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bienvenidos</title>
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    <script src="assets/js/validarRegistro.js"></script>
    <script src="assets/js/jquery-ui.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="assets/js/moment.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/style_Login.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <style type="text/css">
        body {
            background: #FFFFFF;
        }

        .send {
            color: #0000CC;
        }
    </style>
</head>
<body>
<!-- Modal de Inicio de sesión-->
<br>
<div class="text-center">
    <a href="" class="btn btn-primary btn-rounded mb-4" data-toggle="modal" data-target="#modalLoginForm">Inicio de
        Sesión</a>
</div>

<div class="text-center"><a href="" class="" data-toggle="modal" data-target="#exampleModalLong">Instrucciones</a></div>


<!--Modal: modalSocial-->
<div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog cascading-modal" role="document">

        <!--Content-->
        <div class="modal-content">

            <!--Header-->
            <div class="modal-header light-blue darken-3 white-text">
                <h4 class="title"><i class="fas fa-users"></i> Inicio de Sesión</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
            </div>

            <!--Body-->
            <div class="modal-body mb-0 text-center">

                <form name="Login" id="Login" method="POST" onsubmit="return validarRegistro()" class="form-row">
                    <div class="form-group">
                        <label for="NoEmp" class="col-xs-2 control-label">Número de empleado <span></span></label>
                        <br>
                        <input type="text" name="numUser" placeholder="Ingrese su número de empleado" id="NoEmp"
                               class="form-control input-sm col-xs-1" maxlength="6" for="idUser">
                    </div>

                    <div class="form-group">
                        <label for="EmpPass" class="col-xs-9 control-label">RFC</label>
                        <br>
                        <input type="text" class="form-control input-sm col-xs-1" name="passwordAccess"
                               placeholder="Ingrese su rfc"
                               id="EmpPass" maxlength="10">
                    </div>

                    <div class="md-form ml-xl-5">
                        <p>
                            <input type="checkbox" id="terminos">
                            <a href="avisoPrivacidad.php">
                                Acepta términos y condiciones
                            </a>
                        </p>
                    </div>
                    <br>

                    <div class="form-group" id="wait">
                        <img src="img/spinner.gif" id="spinner" width="100" class="align-content-md-center"
                             style="display: none">
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn-lg btn-primary" value="Enviar" id="btnLogin">
                            Iniciar
                        </button>
                    </div>
                    <div class="mxl-form col-md-12">
                        <?php
                        //   require 'partials/conditions.php';
                        require 'partials/forgetPass.php';
                        ?>
                    </div>

                </form>

            </div>

        </div>
        <!--/.Content-->

    </div>
</div>
<!--Modal: modalSocial-->


<div class="modal fade modal-open" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">

    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Inicio de Sesión</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>


            <div class="modal-body mx-sm-5" id="divLogin">
                <form name="Login" id="Login" method="POST" onsubmit="return validarRegistro()" class="form-row">
                    <div class="form-group">
                        <label for="NoEmp" class="control-label">Número de empleado <span></span></label>
                        <br>
                        <input type="text" name="numUser" placeholder="Ingrese su número de empleado" id="NoEmp"
                               class="form-control input-sm" maxlength="6" for="idUser">
                    </div>

                    <div class="form-group">
                        <label for="EmpPass" class="control-label">RFC</label>

                        <input type="text" class="form-control input-sm" name="passwordAccess"
                               placeholder="Ingrese su rfc"
                               id="EmpPass" maxlength="10">
                    </div>

                    <div class="md-form ml-xl-5">
                        <p>
                            <input type="checkbox" id="terminos">
                            <a href="avisoPrivacidad.php">
                                Acepta términos y condiciones
                            </a>
                        </p>
                    </div>
                    <br>

                    <div class="form-group" id="wait">
                        <img src="img/spinner.gif" id="spinner" width="100" class="align-content-md-center"
                             style="display: none">
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn-lg btn-primary" value="Enviar" id="btnLogin">
                            Iniciar
                        </button>
                    </div>
                    <div class="mxl-form col-md-12">
                        <?php
                        //   require 'partials/conditions.php';
                        require 'partials/forgetPass.php';
                        ?>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>
<?php
require 'partials/imagen_Index.php';
?>
<label for="NoEmp" class="col-form-label-lg"> <span></span></label>
</body>
</html>