<?php
/*
 * Clase de conexion a BD. Esta clase es promordial para la multiconexion a 3 BD diferentes y remotas.
 * Se divide en 3 metodos, done el metodo conexion() hace referencia a la BD de DILASA
 * El segundo metodo conexionDel() hace referencia a la BD de DELESA
 * El tercer metodo conexionDasa() hace referencia a la BD de DASA
 *  Nota: Todo codigo que este en PHP no se ve reflejado en el navegador.
 */
ob_start();
class Connection
{
    public function conexion()
    {
        try {

            $connect = new PDO("sqlsrv:server=172.22.2.203\\APSI;Database=APSISISTEMAS", "Lacamex-sa", "7ntelmex.2");
        } catch (PDOException   $err) {
            die("Conexion Fallida... :(" . $err->getMessage());

        }
        return $connect;
    }

    public function conexionDel()
    {
        try {
            $connect = new PDO("sqlsrv:server=172.22.20.185\\APSI;Database=APSISISTEMAS", "Lactamex-sma", "7Intelsma.3");
        } catch (PDOException   $err) {
            die("Conexion Fallida... :(" . $err->getMessage());
        }
        return $connect;
    }

    public function conexionDasa()
    {
        try {
            $connect = new PDO("sqlsrv:server=187.188.149.217\\APSI;Database=APSISISTEMAS", "Lactamex-gdl", "7Intelgdl.3");
        } catch (PDOException   $err) {
            die("Conexion Fallida... :(" . $err->getMessage());
        }
        return $connect;
    }
}
ob_end_flush();