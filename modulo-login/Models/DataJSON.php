<?php
/**
 * Created by PhpStorm.
 * User: jorge-alberto.lucio
 * Date: 11/02/2019
 * Time: 09:20 AM
 */
require_once "Controllers/controller.php";
require_once 'Models/Crud.php';

class ReqJSON{

    public function JSON (){
        if (!empty($_POST['numeroEmpelado'])){
            $datosControllerVac = array('numeroEmpleado' => $_POST['numeroEmpelado'],'Desde'   =>  $_POST['Desde'],'Hasta'=>$_POST['Hasta'],'sol'=>$_POST['sol']);
            $respuestaCrud = Crud::validarPeriodos($datosControllerVac, 'APSISISTEMAS.dbo.empleados','APSISISTEMAS.dbo.empresas','APSISISTEMAS.dbo.vacaciones');

            $reqJSON    =   '{"codigo: ", "emEmp: ", "nombre:", "ap_paterno:","ap_materno:","AltaUser: "}';
            $infoJSON    =   json_decode($reqJSON);
            echo $infoJSON->codigo."<br>";
        }
    }
}

