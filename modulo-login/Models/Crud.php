<?php
require_once "db.php";

class Crud extends Connection
{
    #Metodo para actualizar la informacion del usuario, en este caso sólo el correo de los usuarios.
    #Recibe como parametros los datos enviados por formulario en la seccion de miperfil -> del input mi correo.
    # Recibe como segundo parametro la tabla a donde se desa conectar

    public function updateProfile($datos, $tabla)
    {
        $stmt = Connection::conexion()->prepare("UPDATE $tabla SET $tabla.mail=:email where $tabla.codigo=:numEmp ");
        $stmt->bindParam(":email", $datos["mailUser"], PDO::PARAM_STR);
        #   $stmt->bindParam(":passUser", $datos[7], PDO::PARAM_STR);
        $stmt->bindParam(":numEmp", $datos["idUser"], PDO::PARAM_STR);

        if ($stmt->execute()) {
            #  return  $stmt->fetchAll(PDO::FETCH_ASSOC);
            # var_dump($stmt->fetchAl(PDO::PARAM_STR));
            return "success";
        } else {
            return "error";
        }

        $stmt->close();
    }


/*
 * Metodo que permiten conectar a las 3 diferentes BD de cada empresa, tambien verifica que los datos enviados por metodo POST existan en alguna BD.
 */
    # Meotodo funcional que se conecta a la BD de DILASA y regresa un arreglo con los datos del usuario y permite el acceso de dichos usuarios
    public function ingresoUsuario($datos, $tabla, $tabla2, $tabla3)
    {
        $stmt = Connection::conexion()->prepare("SELECT $tabla.empresa as emEmp,nombre,ap_paterno,ap_materno,$tabla.codigo as NumEmp,curp,cast(empleados.fchalta as DATE)as AltaUser,
                                                  rfcalfa,rfcnum,sexo,password_empleado,mail,nombre_empresa,
                                                  dias_ant,disfrute,vence,pagada,dias_disfr,activo
                                                  FROM $tabla inner join $tabla2 on $tabla2.empresa  = $tabla.empresa 
                                                        inner join $tabla3 on $tabla3.codigo  = $tabla.codigo
                                                    WHERE $tabla.codigo  =:numEmp");

        $stmt->bindParam(':numEmp', $datos['numUser'], PDO::PARAM_STR);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
        $stmt->close();
    }

    # Meotodo funcional que se conecta a la BD de DASA y regresa un arreglo con los datos del usuario y permite el acceso de dichos usuarios
    public function ingresarUsuarioDasa($datos, $table, $table2, $table3)
    {
        $stmt = Connection::conexionDasa()->prepare("Select $table.empresa as emEmp, nombre,ap_paterno,ap_materno, $table.codigo as NumEmp,curp,cast(empleados.fchalta as DATE) as AltaUser,
       rfcalfa,rfcnum,sexo,password_empleado,mail,nombre_empresa,dias_ant,disfrute,vence,pagada,dias_disfr,activo
        From $table inner join $table2 on $table2.empresa = $table.empresa
        inner join $table3 on $table3.codigo = $table.codigo where $table.codigo =:numEmp");

        $stmt->bindParam(':numEmp', $datos['numUser'], PDO::PARAM_STR);
        # $stmt -> bindParam(":empresa",$datos['Enterprises'],PDO::PARAM_STR);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
        $stmt->close();
    }

    # Meotodo funcional que se conecta a la BD de DELESA y regresa un arreglo con los datos del usuario y permite el acceso de dichos usuarios
    public function ingresarUsuarioDelesa($datos, $table, $table2, $table3)
    {
        $stmt = Connection::conexionDel()->prepare("Select $table.empresa as emEmp, nombre,ap_paterno,ap_materno, $table.codigo as NumEmp,curp,cast(empleados.fchalta as DATE) as AltaUser,
       rfcalfa,rfcnum,sexo,password_empleado,mail,nombre_empresa,dias_ant,disfrute,vence,pagada,dias_disfr,activo
        From $table inner join $table2 on $table2.empresa = $table.empresa
        inner join $table3 on $table3.codigo = $table.codigo where $table.codigo =:numEmp");

        $stmt->bindParam(':numEmp', $datos['numUser'], PDO::PARAM_STR);
        # $stmt -> bindParam(":empresa",$datos['Enterprises'],PDO::PARAM_STR);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
        $stmt->close();
    }



    /*
      * Estos metodos van verificar que el No. de usuario ingresado se encuentre la BD y si existe regresa un arreglo con sus datos.
     *  de lo contrario enviara null.
      */

    # Meotodo funcional que se conecta a la BD de DILASA y regresa un arreglo con los datos del usuario
    public function consultaUsuario($datos, $tabla, $tabla2, $tabla3)
    {
        $stmt = Connection::conexion()->prepare("SELECT $tabla.empresa as emEmp,nombre,ap_paterno,ap_materno,$tabla.codigo as NumEmp,curp,cast(empleados.fchalta as DATE)as AltaUser,
                                                  rfcalfa,rfcnum,sexo,password_empleado,mail,nombre_empresa,
                                                  dias_ant,disfrute,vence,pagada,dias_disfr,activo
                                                  FROM $tabla inner join $tabla2 on $tabla2.empresa  = $tabla.empresa 
                                                        inner join $tabla3 on $tabla3.codigo  = $tabla.codigo
                                                    WHERE $tabla.codigo  =:numEmp");
        $stmt->bindParam(':numEmp', $datos, PDO::PARAM_STR);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
        $stmt->close();
    }


    # Meotodo funcional que se conecta a la BD de DASA y regresa un arreglo con los datos del usuario
    public function consultaUsuarioDasa($datos, $tabla, $tabla2, $tabla3)
    {
        $stmt = Connection::conexionDasa()->prepare("SELECT $tabla.empresa as emEmp,nombre,ap_paterno,ap_materno,$tabla.codigo as NumEmp,curp,cast(empleados.fchalta as DATE)as AltaUser,
                                                  rfcalfa,rfcnum,sexo,password_empleado,mail,nombre_empresa,
                                                  dias_ant,disfrute,vence,pagada,dias_disfr,activo
                                                  FROM $tabla inner join $tabla2 on $tabla2.empresa  = $tabla.empresa 
                                                        inner join $tabla3 on $tabla3.codigo  = $tabla.codigo
                                                    WHERE $tabla.codigo  =:numEmp");
        $stmt->bindParam(':numEmp', $datos, PDO::PARAM_STR);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
        $stmt->close();
    }

    # Meotodo funcional que se conecta a la BD de DELESA y regresa un arreglo con los datos del usuario
    public function consultaUsuarioDel($datos, $tabla, $tabla2, $tabla3)
    {
        $stmt = Connection::conexionDel()->prepare("SELECT $tabla.empresa as emEmp,nombre,ap_paterno,ap_materno,$tabla.codigo as NumEmp,curp,cast(empleados.fchalta as DATE)as AltaUser,
                                                  rfcalfa,rfcnum,sexo,password_empleado,mail,nombre_empresa,
                                                  dias_ant,disfrute,vence,pagada,dias_disfr,activo
                                                  FROM $tabla inner join $tabla2 on $tabla2.empresa  = $tabla.empresa 
                                                        inner join $tabla3 on $tabla3.codigo  = $tabla.codigo
                                                    WHERE $tabla.codigo  =:numEmp");
        $stmt->bindParam(':numEmp', $datos, PDO::PARAM_STR);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
        $stmt->close();
    }


/*
 * Metodo para consultar información baísca del usuario, Dichos metodos estan separados segun el nombre de cada emopresa.
 */

    # Meetodo funcional donde se pasa por parametro el No. de empleado y hace su posterior consulta a la BD de DILASA
    public function infoUser($datos, $tabla, $tabla2, $tabla3)
    {
        $stmt = Connection::conexion()->prepare("SELECT $tabla.empresa as emEmp,nombre,ap_paterno,ap_materno,$tabla.codigo as NumEmp,curp,cast(empleados.fchalta as DATE)as AltaUser,
                                                  rfcalfa,rfcnum,sexo,password_empleado,mail,nombre_empresa,
                                                  dias_ant,disfrute,vence,pagada,dias_disfr,activo
                                                  FROM $tabla inner join $tabla2 on $tabla2.empresa  = $tabla.empresa 
                                                        inner join $tabla3 on $tabla3.codigo  = $tabla.codigo
                                                    WHERE $tabla.codigo  =:numEmp");
        $stmt->bindParam(':numEmp', $datos, PDO::PARAM_STR);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
        $stmt->close();
    }

    # Meetodo funcional donde se pasa por parametro el No. de empleado y hace su posterior consulta a la BD de DELESA
    public function inforUserDel($datos, $tabla, $tabla2, $tabla3)
    {
        $stmt = Connection::conexionDel()->prepare("SELECT $tabla.empresa as emEmp,nombre,ap_paterno,ap_materno,$tabla.codigo as NumEmp,curp,cast($tabla.fchalta as DATE) as AltaUser,
         rfcalfa,rfcnum,sexo,password_empleado,mail,nombre_empresa,dias_ant,disfrute,vence,pagada,dias_disfr,activo From $tabla inner join $tabla2 on $tabla2.empresa = $tabla.empresa
            inner join $tabla3 on $tabla3.codigo = $tabla.codigo 
            WHERE $tabla.codigo  =:numEmp");
        $stmt->bindParam(':numEmp', $datos, PDO::PARAM_STR);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
        $stmt->close();
    }

    # Meetodo funcional donde se pasa por parametro el No. de empleado y hace su posterior consulta a la BD de DASA
    public function infoUserDa($datos, $tabla, $tabla2, $tabla3)
    {
        $stmt = Connection::conexionDasa()->prepare("SELECT $tabla.empresa as emEmp,nombre,ap_paterno,ap_materno,$tabla.codigo as NumEmp,curp,cast(empleados.fchalta as DATE)as AltaUser,
                                                  rfcalfa,rfcnum,sexo,password_empleado,mail,nombre_empresa,
                                                  dias_ant,disfrute,vence,pagada,dias_disfr,activo
                                                  FROM $tabla inner join $tabla2 on $tabla2.empresa  = $tabla.empresa 
                                                        inner join $tabla3 on $tabla3.codigo  = $tabla.codigo
                                                    WHERE $tabla.codigo  =:numEmp");
        $stmt->bindParam(':numEmp', $datos, PDO::PARAM_STR);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
        $stmt->close();
    }



    /*
     * Metodos para consultar vacaciones de los empleados
     */
    public function infoUserVacations($datos, $tabla, $tabla2, $tabla3)
    {
        $stmt = Connection::conexion()->prepare("SELECT $tabla.empresa as emEmp,nombre,ap_paterno,ap_materno,$tabla.codigo as NumEmp,curp,cast(empleados.fchalta as DATE)as AltaUser,
                                                  rfcalfa,rfcnum,sexo,password_empleado,mail,nombre_empresa,
                                                  dias_ant,disfrute,vence,pagada,dias_disfr,activo,credencial
                                                  FROM $tabla inner join $tabla2 on $tabla2.empresa  = $tabla.empresa 
                                                        inner join $tabla3 on $tabla3.codigo  = $tabla.codigo
                                                    WHERE $tabla.codigo  =:numEmp");
        $stmt->bindParam(':numEmp', $datos, PDO::PARAM_STR);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
        $stmt->close();
    }

    public function infoUserRecoverPass($datos, $tabla, $tabla2, $tabla3)
    {
        $stmt = Connection::conexion()->prepare("SELECT $tabla.empresa as emEmp,nombre,ap_paterno,ap_materno,$tabla.codigo as NumEmp,curp,cast(empleados.fchalta as DATE)as AltaUser,
                                                  rfcalfa,rfcnum,sexo,password_empleado,mail,nombre_empresa,
                                                  dias_ant,disfrute,vence,pagada,dias_disfr,activo,credencial
                                                  FROM $tabla inner join $tabla2 on $tabla2.empresa  = $tabla.empresa 
                                                        inner join $tabla3 on $tabla3.codigo  = $tabla.codigo
                                                    WHERE $tabla.codigo  =:numEmp");
        $stmt->bindParam(':numEmp', $datos["recoverNumEmp"], PDO::PARAM_STR);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
        $stmt->close();
    }

    // Este metodo permite consultar el numero de empleado y su fecha de ingreso
    public function validarPeriodos($datos, $tabla, $tabla2, $tabla3)
    {
        $stmt = Connection::conexion()->prepare("SELECT $tabla.empresa as EMPRESA,nombre,ap_paterno,ap_materno,$tabla.codigo as NumEmp,curp,cast(empleados.fchalta as DATE)as AltaUser,
                                                             rfcalfa,rfcnum,sexo,password_empleado,mail,nombre_empresa,dias_ant,disfrute,vence,pagada,dias_disfr
                                                          FROM $tabla inner join $tabla2 on $tabla2.empresa  =$tabla.empresa
                                                            inner join $tabla3 on $tabla3.codigo  = $tabla.codigo
                                                              where $tabla.codigo=:numberEmp");
        $stmt->bindParam(':numberEmp', $datos['numeroEmpleado'], PDO::PARAM_STR);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
        $stmt->close();
    }

    public function cargarSolicitudes($tabla)
    {
        $stmt = Connection::conexion()->prepare("SELECT Empresa,Codigo,Disfrute,Fin_Disfrute,Dias_Disfrute,
                                                        Dias_Autoriza,Estatus,Motivo,Fecha_Real,Cerrada,CausaVacaciones
        FROM $tabla ");

        #$stmt->bindParam(':NumEmp',$datos['NumEmp'],PDO::PARAM_STR);
        $stmt->execute();
        return $stmt->fetchAll();
        $stmt->close();
    }


/*
 * Metodos AJAX que sirven para verificar que los datos introducidos por el usuario sean correctos, devuelve una respuesta ansincrona
 *
 */

    # Metodo donde recibe como parametros los datos enviados por el formulario de login y la tabla a donde se desea conectar, en este caso para la BD de DILASA
    public function validarUser($datosModel, $tabla)
    {
        $stmt = Connection::conexion()->prepare("SELECT codigo,empresa,nombre,ap_paterno,ap_materno,curp,fchalta,
        rfcalfa,rfcnum,rfchomo,fchnac,activo,password_empleado,mail,credencial FROM $tabla WHERE codigo  =:codigoUser");
        $stmt->bindParam(":codigoUser", $datosModel, PDO::PARAM_STR);
        $stmt->execute();
        return $stmt->fetch();
        $stmt->close();
    }

    # Metodo donde recibe como parametros los datos enviados por el formulario de login y la tabla a donde se desea conectar, en este caso para la BD de DELESA
    public function validarUserDel($datosModel, $table)
    {
        $stmt = Connection::conexionDel()->prepare("Select codigo,empresa,nombre,ap_paterno,ap_materno,curp,fchalta,
        rfcalfa,rfcnum,rfchomo,fchnac, activo,password_empleado,mail From $table Where codigo   =:codigoUser");
        $stmt->bindParam(":codigoUser", $datosModel, PDO::PARAM_STR);
        $stmt->execute();
        return $stmt->fetch();
        $stmt->close();

    }

    # Metodo donde recibe como parametros los datos enviados por el formulario de login y la tabla a donde se desea conectar, en este caso para la BD de DASA
    public function validarUserDasa($datosModel, $table)
    {
        $stmt = Connection::conexionDasa()->prepare("SELECT codigo,empresa,nombre,ap_paterno,ap_materno,curp,fchalta,
        rfcalfa,rfcnum,rfchomo,fchnac,activo,password_empleado,mail FROM $table WHERE codigo  =:codigoUser");
        $stmt->bindParam(":codigoUser", $datosModel, PDO::PARAM_STR);
        $stmt->execute();
        return $stmt->fetch();
        $stmt->close();

    }

    /*
     * Metodo AJAX que acutalmente no estan en uso
     */
    public function validateVacations($datosController, $tabla)
    {
        $stmt = Connection::conexion()->prepare("SELECT * FROM $tabla WHERE codigo  =:codigoUser");
        $stmt->bindParam(":codigoUser", $datosController, PDO::PARAM_STR);
        $stmt->execute();
        return $stmt->fetch();
        $stmt->close();
    }

    public function validatePass($datosModel, $dataPass1, $dataPass2, $tabla)
    {
        $stmt = Connection::conexion()->prepare("SELECT codigo,empresa,nombre,ap_paterno,ap_materno,curp,fchalta,
        rfcalfa,rfcnum,rfchomo,fchnac,activo,password_empleado,mail,credencial FROM $tabla WHERE codigo  =:codigoUser");
        $stmt->bindParam(":codigoUser", $dataPass1, PDO::PARAM_STR);
        $stmt->bindParam(":codigoUser", $dataPass2, PDO::PARAM_STR);
        $stmt->bindParam(":codigoUser", $datosModel, PDO::PARAM_STR);

        $stmt->execute();
        return $stmt->fetch();
        $stmt->close();
    }

}
