<?php

/**
 * Created by PhpStorm.
 * User: jorge-alberto.lucio
 * Date: 17/12/2018
 * Time: 10:52 AM
 */
class Paginas
{
    public function enlacesPaginasModel($enlaces)
    {
        if ($enlaces == 'index' || $enlaces == 'principal' || $enlaces == 'misVacaciones' || $enlaces == 'recoverPass'
            || $enlaces == 'solicitud' || $enlaces == 'perfil' || $enlaces  ==  "registro") {
            $modulos = "partials/" . $enlaces . ".php";
        } elseif ($enlaces == 'index') {

            $modulos = '../index';
        } elseif ($enlaces == 'index') {
            $modulos = 'registro.php';

        } elseif ($enlaces == 'ok') {
            $modulos = 'registro.php';
        } else {
            $modulos = 'index.php';
        }


        return $modulos;
    }
}

