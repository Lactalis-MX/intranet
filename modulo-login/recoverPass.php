<?php
include_once "Models/Crud.php";
include_once "Controllers/Controller.php";
$sendEmal   =   new MvcController();
$sendEmal   ->  changePass();
?>
<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Recuperar Contraseña</title>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/validarRegistro.js"></script>
    <script src = "https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="assets/js/jquery-ui.min.js"></script>
    <script src="assets/js/moment.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/jquery-ui-1.12.1.custom/jquery-ui.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>

    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <!--<link rel="stylesheet" href="assets/css/style_Login.css">-->
    <link rel="icon" href="img/grupo_lactalis.png" type="image/gif" sizes="16x16">

</head>
<body>
<?php
include 'partials/recoverPass.php';
?>
</body>
</html>
