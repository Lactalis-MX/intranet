<?php
session_start();
require_once "Controllers/Controller.php";
require_once "Models/Crud.php";
if (!isset($_SESSION['user'])  && !isset($_SESSION['user_dasa']) && !isset($_SESSION['user_del'])) {
    header('Location: index.php');
}
?>
<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>¿Sabías qué?</title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <title>Bienvenidos a Lactamex</title>
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    <script src="assets/js/jquery-ui-1.12.1.custom/jquery-ui.js"></script>
    <script src="assets/js/zoomy.js"></script>
    <script src="assets/js/sliderDash.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="assets/js/envioEmail.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/dynamicDashboard.js"></script>
    <script src="assets/js/zoomImg.js"></script>
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/imagehover.min.css">
    <link rel="stylesheet" href="assets/css/imgEfectsStyles.css">
    <link rel="stylesheet" href="assets/css/imgStyle.css">
    <link rel="stylesheet" href="assets/css/styleDashBoard.css">
    <link rel="stylesheet" href="assets/css/sliderStyle.css">
    <link rel="stylesheet" href="assets/css/menuStyle.css">
    <link rel="stylesheet" href="assets/css/bannerStile.css">
    <link rel="stylesheet" href="assets/css/footerStyle.css">
    <link rel="icon" href="img/grupo_lactalis.png" type="image/gif" sizes="16x16">
</head>
<body>
<nav class="navbar fixed-top navbar-expand-lg navbar- bg fixed-top" style="background: #77c5e7">
    <div class="container">
        <a class="navbar-brand" href="principal.php" id="imgLogoLactalis"><img src="img/lactalis_mexico.jpg" alt=""
                                                                               class="float-lg-left w-50"
                                                                               id="img_logo_home"></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="acerca.php" style="color: #FFFFFF;font-size: 18px">Acerca de nosotros</a>
                </li>
                <!--<li class="nav-item">
                    <a class="nav-link" href="misVacaciones.php" style="color: #FFFFFF;font-size: 18px">Vacaciones</a>
                </li>-->
                <li class="nav-item">
                    <a class="nav-link" href="contactUs.php" style="color: #FFFFFF;font-size: 18px"">Contáctanos</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="historia.php" style="color: #FFFFFF;font-size: 18px"">Historia</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="principal.php#title"
                       style="color: #FFFFFF;font-size: 18px"">Comunicados</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="lactajob.php" style="color: #FFFFFF;font-size: 18px"">LactaJob</a>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false" style="color: #FFFFFF;font-size: 18px"">
                    Formatos
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                        <a class="dropdown-item" href="intranet/Formatos/Formato%20de%20Servicios%20IT.pdf">Formato de
                            servicios IT</a>
                        <a class="dropdown-item" href="intranet/Formatos/FORMATO%20PRESTAMOS.pdf">Formato de
                            prestamos</a>
                        <a class="dropdown-item" href="intranet/Formatos/FORMATO%20VACACIONES.pdf">Formato de
                            vacaciones</a>
                        <a class="dropdown-item" href="intranet/Formatos/Anexo%201%20-Requisición%20de%20Personal.pdf">Requisisión
                            de Personal</a>
                        <a class="dropdown-item"
                           href="intranet/Formatos/Formato Entrevista de Salida Nuevo V04062019.pdf">Formato Entevista
                            de Salida</a>
                        <a class="dropdown-item" href="intranet/Formatos/FORMATO%20DE%20MOVIM.%20DE%20PERSONAL.PDF">Formato
                            Movimiento de Personal</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false" style="color: #FFFFFF;font-size: 18px"">
                    Politicas
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                        <a class="dropdown-item" href="intranet/POLITICAS/Anexo%201%20-Requisición%20de%20Personal.pdf">Política
                            Requisición de Personal</a>
                        <a class="dropdown-item"
                           href="intranet/POLITICAS/DRH-P012-POLITICA%20UBER%20FOR%20BUSINESS.pdf">Política Uber para
                            Negocios</a>
                        <a class="dropdown-item"
                           href="intranet/POLITICAS/Política%20de%20viajes%20nacionales%20e%20internacionales.pdf">Política
                            de Viajes</a>
                        <a class="dropdown-item" href="intranet/POLITICAS/CODIGO%20DE%20CONDUCTA.PDF">Código de
                            Conducta</a>
                        <a class="dropdown-item"
                           href="intranet/POLITICAS/Comunicado Interno - Movimientos de Personal.pdf">Movimiento de
                            Personal</a>
                    </div>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false" style="color: #FFFFFF;font-size: 18px"">
                    Configuración
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                        <a class="dropdown-item" href="profile.php">Perfil</a>
                        <a class="dropdown-item" href="Controllers/cerrarSesion.php">Cerrar sesión</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>

<br><br><br><br>
<div class="container">

    <!-- Page Heading/Breadcrumbs -->
    <h1 class="mt-4 mb-3" style="">
        <b>¿Sabías Qué?</b>
    </h1>

    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="principal.php">Inicio</a>
        </li>
        <li class="breadcrumb-item active">¿Sabías qué?</li>

    </ol>

    <!-- Intro Content -->
    <div class="container">
        <hr>
        <div class="row">
            <div class="col-xs-4 col-lg-4 col-lg-2">

                <div class="col-lg-12  mb-4 py-5">
                    <figure class="imghvr-slide-down">
                        <img src="img/img_logos/logo_nuevo_esmeralda.png" alt="example-image" id="img_sabias1">
                        <figcaption>
                            <h3 class="ih-fade-down ih-delay-sm ">¿Sabías Qué?</h3>
                            <strong><p class="ih-zoom-in ih-delay-md">Cuando compras una chapata que lleve queso en
                                    Cinemex y Cinépolis, estás consumiendo Manchego
                                    Molletero de la marca Esmeralda</p>
                            </strong>
                        </figcaption>
                        <a href="#"></a>
                    </figure>
                </div>
            </div>
            <div class="col-xs-4 col-lg-4 col-lg-2">

                <div class="col-lg-12  mb-4 py-5">
                    <figure class="imghvr-slide-down">
                        <img src="img/sabias/pizza_hut.jpg" alt="example-image" id="img_sabias2">
                        <figcaption>
                            <h3 class="ih-fade-down ih-delay-sm ">¿Sabías Qué?</h3>
                            <strong><p class="ih-zoom-in ih-delay-md">Cuando compras una deliciosa pizza en Pizza Hut,
                                    estás
                                    consumiendo un queso producido por LACTALIS</p>
                            </strong>
                        </figcaption>
                        <a href="#"></a>
                    </figure>
                </div>
            </div>
            <div class="col-xs-4 col-lg-4 col-lg-2">

                <div class="col-lg-12  mb-4 py-5">
                    <figure class="imghvr-slide-down">
                        <img src="img/sabias/starbucks.png" alt="example-image" id="img_sabias1">
                        <figcaption>
                            <h3 class="ih-fade-down ih-delay-sm ">¿Sabías Qué?</h3>
                            <strong><p class="ih-zoom-in ih-delay-md">Cuando compras una chapata que lleve queso en
                                    Starbucks, estás consumiendo Manchego
                                    Molletero de la marca Esmeralda</p>
                            </strong>
                        </figcaption>
                        <a href="#"></a>
                    </figure>
                </div>
            </div>
            <div class="col-xs-4 col-lg-4 col-lg-2">

                <div class="col-lg-12  mb-4 py-5">
                    <figure class="imghvr-slide-down">
                        <img src="img/sabias/becarios.png" alt="example-image" id="img_sabias">
                        <figcaption>
                            <h3 class="ih-fade-down ih-delay-sm ">¿Sabías Qué?</h3>
                            <strong><p class="ih-zoom-in ih-delay-md">Lactalis forma parte del programa Jóvenes
                                    Contruyendo el Futuro</p>
                            </strong>

                            <strong>
                                <p class="ih-zoom-in ih-delay-md">
                                    Programa social implementado por el gobierno federal, cuyo objetivo es que la
                                    empresa, en colaboración con un tutor desarrollen habilidades, aprovechen su talento
                                    y se les de la oportunidad de comenzar experiencia laboral.
                                </p>
                            </strong>
                        </figcaption>
                        <a href="#"></a>
                    </figure>
                </div>
            </div>
            <div class="col-xs-4 col-lg-4 col-lg-2">

                <div class="col-lg-12  mb-4 py-5">
                    <figure class="imghvr-slide-down">
                        <img src="img/sabias/IKALI_SABIAS_QUE.jpg" alt="example-image" id="img_sabias">
                        <figcaption>
                            <h3 class="ih-fade-down ih-delay-sm ">¿Sabías Qué?</h3>
                            <strong><p class="ih-zoom-in ih-delay-md">¡Aprendamos Juntos!</p>
                            </strong>

                            <strong>
                                <p class="ih-zoom-in ih-delay-md">
                                    Una actividad de gran relevancia para el departamento de compras es una correcta
                                    planeación
                                    y determinación de los puntos en los que es necesario hacer un pedido, esto debe ser
                                    en el momento justo y en la cantidad
                                    correcta para evitar compras innecesarias y procesos de invetarios costosos
                                </p>
                            </strong>
                        </figcaption>
                        <a href="#"></a>
                    </figure>
                </div>
            </div>


        </div>
    </div>


    <!-- /.row -->

</div>

<footer class="py-5 bg" style="background: #77c5e7">
    <div class="container" style="color: #FFFFFF">
        © 2018 Copyright:
        <a href="http://www.esmeralda.com.mx/" style="color: #FFFFFF"> Esmeralda.com.mx</a>
        <p>todos los derechos reservados</p>
        <div class="col-lg-12 py-lg-5">
            <div class="mb-3 flex-center">
                <a class="fb-ic"
                   href="https://www.facebook.com/pages/category/Industrial-Company/Vacantes-Lactalis-M%C3%A9xico-281980632337467/">
                    <img src="img/facebook.png" alt="">
                </a>
                <a class="tw-ic" href="https://twitter.com/groupe_lactalis?lang=en">
                    <img src="img/twitter.png">
                </a>

                <a class="ins-ic" href="https://www.instagram.com/galbanicheese/?hl=en">
                    <img src="img/instagram.png">
                </a>

            </div>
        </div>
    </div>
    <!-- /.container -->
</footer>


</body>
</html>