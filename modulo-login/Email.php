<?php
session_start();
require_once 'Controllers/Controller.php';
require_once 'Models/Crud.php';
#require_once 'Controllers/email.php';
$sendMail   =   new MvcController();
$sendMail   ->sendMail();
?>
<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Send Email</title>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/sliderIndex.js"></script>
    <script src="assets/js/modal.js"></script>
    <script src="assets/js/jquery-ui-1.12.1.custom/external/jquery/jquery.js"></script>
    <script src="assets/js/jquery-ui-1.12.1.custom/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/js/bootstrap.bundle.min.js">
</head>
<body>
<!--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Enviar Correo</button>-->
<a href="#exampleModal" data-toggle="modal" data-target="">Enviar correo</a>

<?php
require 'partials/formEmail.php';
?>

</body>
</html>