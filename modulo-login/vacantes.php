<?php
session_start();
require_once 'Controllers/Controller.php';
require_once 'Models/Crud.php';
if (!isset($_SESSION['user'])) {
    header('Location: index.php');
}

?>
<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Vacantes</title>

    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    <script src="assets/js/jquery-ui-1.12.1.custom/jquery-ui.js"></script>
    <script src="assets/js/sliderDash.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="assets/js/envioEmail.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/dynamicDashboard.js"></script>
    <script src="assets/js/lateralMenu.js"></script>

    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/imgEfectsStyles.css">
    <link rel="stylesheet" href="assets/css/imgStyle.css">
    <!--<link rel="stylesheet" href="http://csshake.surge.sh/csshake.css">-->
    <link rel="stylesheet" href="assets/css/styleDashBoard.css">
    <link rel="stylesheet" href="assets/css/imgEfectsStyles.css">
    <link rel="stylesheet" href="assets/css/sliderStyle.css">
    <link rel="stylesheet" href="assets/css/menuStyle.css">
    <link rel="stylesheet" href="assets/css/bannerStile.css">
    <link rel="stylesheet" href="assets/css/footerStyle.css">
    <link rel="icon" href="img/grupo_lactalis.png" type="image/gif" sizes="16x16">
</head>
<body>
<nav class="navbar fixed-top navbar-expand-lg navbar- bg fixed-top" style="background: #77c5e7">
    <div class="container">
        <a class="navbar-brand" href="principal.php" id="imgLogoLactalis"><img src="img/lactalis_mexico.jpg" alt=""
                                                                               class="float-lg-left w-50"
                                                                               id="img_logo_home"></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="acerca.php" style="color: #FFFFFF;font-size: 18px">Acerca de nosotros</a>
                </li>
                <!--<li class="nav-item">
                    <a class="nav-link" href="misVacaciones.php" style="color: #FFFFFF;font-size: 18px">Vacaciones</a>
                </li>-->
                <li class="nav-item">
                    <a class="nav-link" href="contactUs.php" style="color: #FFFFFF;font-size: 18px"">Contáctanos</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="historia.php" style="color: #FFFFFF;font-size: 18px"">Historia</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="principal.php#title"
                       style="color: #FFFFFF;font-size: 18px"">Comunicados</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="lactajob.php" style="color: #FFFFFF;font-size: 18px"">LactaJob</a>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false" style="color: #FFFFFF;font-size: 18px"">
                    Formatos
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                        <a class="dropdown-item" href="">Formato de vacaciones</a>
                        <a class="dropdown-item" href="">Formato de otra cosa</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false" style="color: #FFFFFF;font-size: 18px"">
                    Politicas
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                        <a class="dropdown-item" href="">Politica 1</a>
                        <a class="dropdown-item" href="">Politica 2</a>
                    </div>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false" style="color: #FFFFFF;font-size: 18px"">
                    Configuración
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                        <a class="dropdown-item" href="profile.php">Perfil</a>
                        <a class="dropdown-item" href="Controllers/cerrarSesion.php">Cerrar sesión</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
<br><br><br>
<header>

    <div id="demo" class="carousel slide" data-ride="carousel">

        <!-- Indicators -->
        <ul class="carousel-indicators">
            <li data-target="#demo" data-slide-to="0" class="active"></li>
            <li data-target="#demo" data-slide-to="1"></li>
            <li data-target="#demo" data-slide-to="2"></li>
        </ul>

        <!-- The slideshow -->
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="img/header_quesos.jpg" alt="">
            </div>
            <div class="carousel-item">
                <img src="img/Mutimarcas.jpg" alt="">
            </div>
            <div class="carousel-item">
                <img src="img/header_lactalis_galbani_new.png" alt="">
            </div>
        </div>

        <!-- Left and right controls -->
        <a class="carousel-control-prev" href="#demo" data-slide="prev">
            <span class="carousel-control-prev-icon"></span>
        </a>
        <a class="carousel-control-next" href="#demo" data-slide="next">
            <span class="carousel-control-next-icon"></span>
        </a>

    </div>
</header>

<h2> A continuación te presentamos nuestras vacantes disponibles</h2>

<div class="container">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="lactajob.php">LactaJob</a>
        </li>
        <li class="breadcrumb-item active">Vacantes</li>
    </ol>

    <div class="row" id="vacante1">
        <img src="img/img_postulaciones/gerenteCalidad.jpg" style="max-width: 750px; max-height: 750px;float:left">
        <h4>
            Vacante 1
        </h4>
    </div>
    <br><br>

    <div class="row" id="vacante2">
        <img src="img/img_postulaciones/cpfr.jpg" style="max-width: 750px; max-height: 750px;float:right">
        <h4>
            Vacante 1
        </h4>
    </div>
    <br><br>


    <div class="row" id="vacante3">
        <img src="img/img_postulaciones/cross_border.jpg" style="max-width: 750px; max-height: 750px;float: left">
        <h4>
            Vacante 1
        </h4>
    </div>
    <br><br>


    <div class="row" id="vacante4">
        <img src="img/img_postulaciones/kam.png" style="max-width: 750px; max-height: 750px;float: right">
        <h4>
            Vacante 1
        </h4>
    </div>
    <br><br>


    <div class="row" id="vacante5">
        <img src="img/img_postulaciones/gerenteCuentasClave.jpg" style="max-width: 750px; max-height: 750px;float:  left">
        <h4>
            Vacante 1
        </h4>
    </div>
    <br><br>


</div>

</body>
</html>