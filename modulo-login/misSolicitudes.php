<?php
session_start();
require_once "Controllers/Controller.php";
require_once  "Models/Crud.php";
$solicitud =   new MvcController();
$solicitud  ->loadList();
?>
<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mis Solicitudes</title>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style_Login.css">
    <link rel="stylesheet" href="assets/js/bootstrap.bundle.min.js">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
</head>
<body>
<?php
#require 'partials/headerPeticionVacaciones.php';
require 'partials/resultados.php';
?>
</body>
</html>