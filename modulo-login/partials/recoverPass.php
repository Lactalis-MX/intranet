<br><br>
<div class="container-fluid">
    <h2 class="">Recuperar Contraseña</h2>
    <br>
    <h4 class="p-1">¿Olvidó su contraseña?</h4>
    <p class="p-3">
        Si deseas recuperar tu contraseña, ingresa tu correo empresarial y tu número de empleado
    </p>
    <br>
    <div class="form-row col-lg-12">
        <div class="col-lg-12">
            <form name="recover" method="POST" onsubmit="return validateRecoverPass()"
                  class="form-group">
                <div class="form-group">
                    <label for="exampleInputEmail1">Correo electronico </label>
                    <input type="email" class="form-control" id="emailRecoverUser" name="recoverEmail"
                           aria-describedby="emailHelp" placeholder="Ingrese su correo">
                    <small id="emailHelp" class="form-text text-muted">Nunca compartimos tus datos con ajenos.
                    </small>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Número de empleado</label>
                    <input type="text" class="form-control" name="recoverNumEmp" id="numberRecoverUser"
                           placeholder="Numero de empleado" maxlength="6">
                </div>
                <!--<div class="form-check">
                    <input type="checkbox" class="form-check-input" id="checkRecover">
                    <label class="form-check-label" for="exampleCheck1">Check me out</label>
                </div>-->
                <button type="button" class="btn btn-info" onclick="location.href='index.php'">Regresar</button>
                <button type="reset" class="btn btn-danger">Borrar</button>
                <button type="submit" class="btn btn-primary">Enviar</button>
            </form>
        </div>
    </div>
</div>




