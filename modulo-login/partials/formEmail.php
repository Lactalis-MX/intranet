<!--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Open modal for @mdo</button>-->
<!--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@fat">Open modal for @fat</button>-->
<!--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@getbootstrap">Open modal for @getbootstrap</button>-->

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title col-12" id="exampleModalLabel">Nuevo Correo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="Controllers/email.php">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">:</label>
                        <input type="email" class="form-control" id="recipient-name" name="correo" required>
                    </div>

                    <div class="form-group">
                        <label for="name_user" class="col-form-label">C.C:</label>
                        <input type="email" class="form-control col-6" id="name_user" name="copyMail" >
                    </div>

                    <div class="form-group">
                        <label for="name_user" class="col-form-label">Nombre:</label>
                        <input type="text" class="form-control col-6" id="name_user" name="name" >
                    </div>



                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Message:</label>
                        <textarea class="form-control" id="message-text" name="comentario"></textarea>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Send message</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>