<div class="progress">
    <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
</div>
<form id="regiration_form" novalidate action="action.php"  method="post">
    <fieldset>
        <h2>Step 1: Ingrese sus datos</h2>
        <div class="form-group">
            <label for="email">Número de empleado</label>
            <input type="text" class="form-control" id="email" name="numberUser" placeholder="Número de empleado">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Ingrese la fecha actual</label>
            <input type="date" class="form-control" id="password" placeholder="" name="fechaIngreso">
        </div>
        <input type="button" name="password" class="next btn btn-info" value="Next" onsubmit="return validacionPet()"/>
    </fieldset>
    <fieldset>
        <h2> Step 2: Add Personnel Details</h2>
        <div class="form-group">
            <label for="fName">First Name</label>
            <input type="text" class="form-control" name="fName" id="fName" placeholder="First Name">
        </div>
        <div class="form-group">
            <label for="lName">Last Name</label>
            <input type="text" class="form-control" name="lName" id="lName" placeholder="Last Name">
        </div>
        <input type="button" name="previous" class="previous btn btn-default" value="Previous" />
        <input type="button" name="next" class="next btn btn-info" value="Next" />
    </fieldset>
    <fieldset>
        <h2>Step 3: Contact Information</h2>
        <div class="form-group">
            <label for="mob">Mobile Number</label>
            <input type="text" class="form-control" id="mob" placeholder="Mobile Number">
        </div>
        <div class="form-group">
            <label for="address">Address</label>
            <textarea  class="form-control" name="address" placeholder="Communication Address"></textarea>
        </div>
        <input type="button" name="previous" class="previous btn btn-default" value="Previous" />
        <input type="submit" name="submit" class="submit btn btn-success" value="Submit" />
    </fieldset>
</form>


i have created 3 steps using HTML field-set control for each step, so whenever user click next and previous button we will slide fieldset based on current step.

<!--<strong>Step 3:</strong> Add css class in <code><head></code> section of <code>index.php</code> file to hide <code>fieldset</code> except first <code>fieldset</code>-->


<style type="text/css">
    #regiration_form fieldset:not(:first-of-type) {
        display: none;
    }
</style>

<script type="text/javascript">
    $(document).ready(function(){
        var current = 1,current_step,next_step,steps;
        steps = $("fieldset").length;
        $(".next").click(function(){
            current_step = $(this).parent();
            next_step = $(this).parent().next();
            next_step.show();
            current_step.hide();
            setProgressBar(++current);
        });
        $(".previous").click(function(){
            current_step = $(this).parent();
            next_step = $(this).parent().prev();
            next_step.show();
            current_step.hide();
            setProgressBar(--current);
        });
        setProgressBar(current);
        // Change progress bar action
        function setProgressBar(curStep){
            var percent = parseFloat(100 / steps) * curStep;
            percent = percent.toFixed();
            $(".progress-bar")
                .css("width",percent+"%")
                .html(percent+"%");
        }


    });

    function  validacionPet() {
        var x   =   document.forms["regiration_form"]["numberUser"].value;
        if (x==""){
            //  swal("Oops, Algo salió mal!!", "error");
            //alert("Debe ingresar su número de empleado");
            swal({
                title:"error",
                text:"Debe ingresar su número de usuario",
                icon:"error",
                timer:5000,
            });
            return false;
        }
    }


</script>