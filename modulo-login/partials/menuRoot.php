<nav class="navbar navbar-expand-sm  navbar-dark" id="first_nav">
    <a class="navbar-brand text-dark" href="principal.php">LactaMex</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link text-dark" href="misVacaciones.php">Vacaciones</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-dark" href="misSolicitudes.php">Mis solicitudes</a>
            </li>


            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle text-dark" data-toggle="dropdown" href="#">Configuración</a>
                <div class="dropdown-menu">
                    <a href="profile.php" class="dropdown-item ">Mi perfil</a>
                    <a href='Controllers/cerrarSesion.php' class="dropdown-item">Cerrar Sesión</a>
                </div>
            </li>

        </ul>
    </div>
</nav>


