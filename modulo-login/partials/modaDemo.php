<script src="assets/js/index.js"></script>
<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong">Consultar</button>

<!-- Modal -->
<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Consulta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" role="form" onsubmit="return consltarRegitros()">
                    <label class="col-lg-7 col-form-label form-control-label" for="numUser"> Número de empleado</label>
                    <div class="col-lg-auto">
                        <input type="text" name="numeroEmpleado" id="numUser" placeholder="No Empleado"  maxlength="5" class="form-control-sm" >
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" >Close</button>
                        <button type="reset" class="btn btn-danger" data->Borrar</button>
                        <button type="submit" class="btn btn-primary" value="Cancelar">Enviar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
