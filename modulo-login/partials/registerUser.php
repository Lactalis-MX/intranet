<!--<a href="index.php" class="form-group">Login <br> </a>-->
<div class="mt-lg-5">
    <div class="form-group">
        <div class="mx-auto col-lg-11">
            <div class="card">
                <div class="card-header">
                    <h4 class="mb-0">Alta de Usuarios</h4>
                </div>

                <div class="card-body">
                    <form action="registro.php" method="POST" class="form" role="form">
                        <div class="row">
                            <label class="col-3"> Número de empleado</label>
                            <div class="col-lg-9">
                                <input type="text" name="numeroEmpleado" placeholder="No Empleado" class="form-control"
                                       required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">Nombre</label>
                            <div class="col-lg-9">
                                <input type="text" name="nombreUsuario" placeholder="Nombre" class="form-control">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">Paterno</label>
                            <div class="col-lg-9">
                                <input type="text" name="apellidoP" placeholder="Apellido Paterno" class="form-control">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">Materno</label>
                            <div class="col-lg-9">
                                <input type="text" name="apellidoM" placeholder="Apellido Materno" class="form-control">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">Fecha de Ingreso</label>
                            <div class="col-lg-9">
                                <input type="date" name="fechaIngreso" class="form-control">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">Correo</label>
                            <div class="col-lg-9">
                                <input type="email" name="email" placeholder="tucorreo@.com" class="form-control">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">Contraseña</label>
                            <div class="col-lg-9">
                                <input type="password" name="contraseña" placeholder="Contraseña" class="form-control">
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">Confrimar Contraseña</label>
                            <div class="col-lg-9">
                                <input type="password" name="confirmPass" placeholder="ConfirmarContraseña"
                                       class="form-control">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label"></label>
                            <div class="col-lg-9">
                                <input type="submit" class="btn btn-primary col-2" value="Send">
                                <input type="reset" class="btn btn-secondary col-2" value="Cancelar">
                                <input type="reset" class="btn btn-danger col-2" value="Regresar"
                                       onclick="location.href='principal.php'">
                            </div>
                        </div>
                </div>
            </div>

        </div>
    </div>

</div>

</form>


