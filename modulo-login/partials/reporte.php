<?php
session_start();
require '../Controllers/fechaActual.php';

foreach ($_SESSION as $dato) {
    $nameUser       = $dato[1];
    $lastUserName2  = $dato[2];
    $lastUserName   = $dato[3];
    $numUser        = $dato[4];
    $curp           = $dato[6];
    $rfcUser        = $dato[7];
    $DíasPend       = $dato[8];
    $altaUser       = $dato[13];
    $diaSoli        = $dato[20];
    $Desde          = $dato[22];
    $Hasta          = $dato[23];
    $valance    =   $DíasPend-$diaSoli;

}
$fecha  =   date('Y-m-d');
?>
<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Reporte De Usuario</title>
    <style type="text/css">
        body{
            margin: auto;
        }

        h1{
            text-align: center;
            font-family: Arial;
            font-size: 20px;
            background: #93a3b5;
        }

        .cont-form{
            border: #1d2124;
            border-radius: 20px;
            position: center;
            line-height: 3px;
            height: 1050px;
        }
        .cont-data-user{

        }
        .cont-title{
            text-align: center;
            font-family: Arial;
            font-size: 20px;
            background-size: contain;
        }
        .cont_img{
            width: 55px;
            padding-top: -45px;
            padding-right: 450px;
        }
        .imgEnter{
            position: center;
            height: 150px;
            padding: 130px 185px 15px 115px;
            border-radius: 20px;
        }


        .line-cont{
            text-align: center;
            font-size: 20px;
            background: #93a3b5;

        }
        .cont-name{
            border: #1d2124;
            border-radius: 10px;
            width: 755px;
            height:150px;
        }
        .cont-name-2{

        }
        .label-name-user{
            text-align: left;
        }

        .cont-num-user{
            float: right;
            padding-left: 250px;
            padding-top: -30px;
        }
        .cont-name-ID{
            float: right;
            padding-left: 370px;
            padding-top: -35px;
        }

        .cont-name-DATE{
            float: right;
            padding-left: 590px;
            padding-top: -37px;
        }
        .cont-data-DAYS{
            float: right;
            padding-left: 5px;
            padding-top: -32px;
        }
        .cont-name-RFC{
            float: right;
            padding-left: 250px;
            padding-top: 10px;
        }

        .cont-data-PEN{
            text-align: center;
            float: right;
            padding-left: 370px;
            padding-top: -35px;
        }
        .cont-DATE{
            float: right;
            padding-left: -150px;
        }
        .cont-DESDE{
            float: right;
        }

        .cont-HASTA{
            float: right;
            padding-left: 250px;
            padding-top: -25px;
        }

    </style>
</head>
<body>
<div class="cont-form"">
    <div>
        <form>
            <div class="cont-title">
                <h1>CONSTACIA DE VACACIONES</h1>
                    <div class="cont-DATE">
                        <label class="label-num-user"><strong> Fecha </strong><p><?php echo $fecha?></p></label>
                    </div>
                <div class="cont_img">
                    <img src="../assets/razon.jpg" class="imgEnter" alt="">

                </div>
            </div>

            <div class="cont-data-user">
                <label><strong><p class="line-cont">Datos del Usuario</p></strong></label>
            </div>

            <div class="cont-name">
                <label class="label-name-user"><strong> Nombre: </strong><p><?php echo $nameUser.' '.$lastUserName.' '.$lastUserName2?></p></label>
                <div class="cont-num-user">
                    <label class="label-num-user"><strong> Número: </strong><p><?php echo $numUser ?></p></label>
                </div>

                <div class="cont-name-ID">
                    <label class="label-num-user"><strong> CURP: </strong><p><?php echo $curp ?></p></label>
                </div>

                <div class="cont-name-DATE">
                    <label class="label-num-user"><strong> Ingreso: </strong><p><?php echo $altaUser ?></p></label>
                </div>

                <div class="cont-name-RFC">
                    <label class="label-num"><strong> RFC: </strong><p><?php echo $rfcUser ?></p></label>
                </div>

                <div class="cont-data-DAYS">
                    <label class="label-num-user"><strong> Días Pendientes: </strong><p><?php echo $DíasPend ?></p></label>
                </div>

                    <div class="cont-data-PEN">
                        <label class="label-num-user"><strong> Días Solicitados: </strong><p><?php echo $diaSoli?></p></label>
                    </div>

                <div class="cont-DESDE">
                    <label class="label-num-user"><strong> Desde: </strong><p><?php echo $Desde ?></p></label>
                </div>

                <div class="cont-HASTA">
                    <label class=label-num-user"><strong> Hasta: </strong><p><?php echo $Hasta?></p></label>
                </div>


                <!--<div class="cont-data-PEN">
                    <label class="label-num-user"><strong> Nuevo Valance: </strong><p><?php ?></p></label>
                </div>-->
            </div>

            <div class="cont-data-user">
                <label><strong><p class="line-cont">Departamento de personal</p></strong></label>
            </div>
            <br><br>




        </form>
    </div>
</div>

</body>
</html>



