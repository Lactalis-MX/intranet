<!--<div id="demo" class="carousel slide" data-ride="carousel">


    <ul class="carousel-indicators">
        <li data-target="#demo" data-slide-to="0" class="active"></li>
        <li data-target="#demo" data-slide-to="1"></li>
        <li data-target="#demo" data-slide-to="2"></li>
    </ul>


    <div class="carousel-inner">
        <div class="carousel-item active">
            <img src="img/Comunicado_Skype_Empresarial.png" alt="Los Angeles" width="1100" height="500">
        </div>
        <div class="carousel-item">
            <img src="img/Tu%20empresa_tu%20historia.png" alt="Chicago" width="1100" height="500">
        </div>

        <div class="carousel-item">
            <img src="img/Volcan.png" alt="New York" width="1100" height="500">
        </div>
    </div>


    <a class="carousel-control-prev" href="#demo" data-slide="prev">
        <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#demo" data-slide="next">
        <span class="carousel-control-next-icon"></span>
    </a>
</div>-->
<h2 class="w3-center" id="title_valores" style="font-family: 'Comic Sans MS'">Nuestros Valores</h2>

<div id="contenido_valores">

    <div class="col-4" id="tarjeta_2">
        <h3>Misión</h3>
        <p>
            Ser una de las compañías líderes en lácteos México, con un crecimiento rentable, ofreciendo productos
            con la mejor relación precio – calidad.
        </p>

    </div>


    <div class="col-4" id="tarjeta_2">
        <h3>Visión</h3>
        <p>
            Alimentar el corazón de nuestros consumidores con productos de la mejor calidad, siendo siempre fieles a
            nuestros valores y al cuidado de nuestra gente.
        </p>
    </div>



</div>
