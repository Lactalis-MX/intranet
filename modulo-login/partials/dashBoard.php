<div class="container">

    <h1 class="my-4" style="">Bienvenidos a Lactamex</h1>

    <!-- Marketing Icons Section -->

    <div class="row">


        <li class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

            <div class="col-lg-9" id="">

                <figure class="imghvr-fade-in-up">
                    <img class="img-fluid rounded mb-lg-5" src="img_recetas/fondoRecetario.jpg" id="img_recetario">

                    <figcaption>
                        <h3 class="ih-fade-down ih-delay-sm">Recetario</a>
                        </h3>
                        <p class="ih-zoom-in ih-delay-md">
                            <i>


                                Nuestros recetario, elaborado con nuestros productos, obteniendo platillos
                                deliciosos.
                            </i>
                        </p>
                        <a href="sabias.php" class="text-lg"
                           style="font-size: 40px;color: #FFFFFF;">
                    </figcaption>
                    <a href="recetario.php"></a>
                </figure
            </div>

            <hr>

        </li>

        <li class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

            <div class="col-lg-9" id="">

                <figure class="imghvr-fade-in-up">
                    <img class="img-fluid rounded mb-lg-5" src="img/fondoFelizCumple.jpg" id="img_cumple">

                    <figcaption>
                        <h3 class="ih-fade-down ih-delay-sm">Cumpleañeros
                            del Mes
                        </h3>
                        <p class="ih-zoom-in ih-delay-md">
                            <i>


                                Lactalis México se complace en desearles un feliz cumpleaños a todos los
                                colaboradores que
                                cumplen años en este mes. Gracias por brindar siempre su apoyo y esfuerzo a la
                                compañía.
                                Deseamos que este año esté lleno de alegría, metas cumplidas y mucho éxito.
                            </i>
                        </p>
                        <a href="sabias.php" class="text-lg"
                           style="font-size: 40px;color: #FFFFFF;">
                    </figcaption>
                    <a href="recetario.php" class="text-lg" style="font-size: 40px"
                       data-toggle="modal" data-target="#cumple"></a>

                </figure
            </div>
            <div class="modal" id="cumple">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">


                        <div class="modal-header">
                            <h1 class="modal-title text-center">Feliz Cumpleaños</h1>
                            <button type="button" class="close" data-dismiss="modal">×</button>
                        </div>


                        <div class="modal-body" style="font-family: 'Apple Color Emoji'">
                            <h4 style="font-family: 'Arial Narrow'">Lactalis les desea un feliz cumpleaños a
                                nuestros compañeros..!</h4>

                            <img src="img/img_cumple/Agosto_1.png" class="img-responsive" style="max-width: 750px;max-height: 950px">
                            <br>
                            <img src="img/img_cumple/Agosto_1.png" class="img-responsive" style="max-width: 750px;max-height: 950px">
                            <br>
                            <img src="img/img_cumple/Agosto_1.png" class="img-responsive" style="max-width: 750px;max-height: 950px">
                            <br>
                            <img src="img/img_cumple/Agosto_1.png" class="img-responsive" style="max-width: 750px;max-height: 950px">


                        </div>


                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>

                    </div>
                </div>
            </div>

            <hr>

        </li>

        <li class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

            <div class="col-lg-9" id="">

                <figure class="imghvr-fade-in-up">
                    <img class="img-fluid rounded mb-lg-5" src="img/lactaJob.jpg" id="img_lactajob">

                    <figcaption>
                        <h3 class="ih-fade-down ih-delay-sm">LactaJob</a>
                        </h3>
                        <p class="ih-zoom-in ih-delay-md">
                            <i>

                                Lactalis cree en el crecimiento interno de los colaboradores. Por eso, a través de
                                LACTAJOB
                                nuestro programa de reclutamiento interno, puedes postularte a las vacantes
                                disponibles.
                            </i>
                        </p>
                        <a href="sabias.php" class="text-lg"
                           style="font-size: 40px;color: #FFFFFF;">
                    </figcaption>
                    <a href="lactajob.php"></a>
                </figure
            </div>

            <hr>

        </li>

        <li class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

            <div class="col-lg-9" id="">

                <figure class="imghvr-fade-in-up">
                    <img class="img-fluid rounded mb-lg-5" src="img/sabias_que.jpg" id="img_sabias">

                    <figcaption>
                        <h3 class="ih-fade-down ih-delay-sm">¿Sabías
                            qué?</a>
                        </h3>
                        <p class="ih-zoom-in ih-delay-md">
                            <i>
                                Datos y curiosidades que no sabías acerca de nuestros productos. En este
                                apartado
                                podrás
                                encotrar eso y un poco más
                            </i>
                        </p>
                        <a href="sabias.php" class="text-lg"
                           style="font-size: 40px;color: #FFFFFF;">
                    </figcaption>
                    <a href="sabias.php"></a>
                </figure


            </div>

            <hr>

        </li>

        <li class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

            <div class="col-lg-9" id="">

                <figure class="imghvr-fade-in-up">
                    <img class="img-fluid rounded mb-lg-5" src="img/sabias/BSC.PNG" id="img_sabias">

                    <figcaption>
                        <h3 class="ih-fade-down ih-delay-sm">BSC</a>
                        </h3>
                        <p class="ih-zoom-in ih-delay-md">
                            <i>

                            </i>
                        </p>
                        <a href="" class="text-lg"
                           style="font-size: 40px;color: #FFFFFF;">
                    </figcaption>
                    <a href="" class="text-lg" style="font-size: 40px"
                       data-toggle="modal" data-target="#bsc"></a>
                </figure>
                <div class="modal" id="bsc">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">


                            <div class="modal-header">
                                <h1 class="modal-title text-center" style="color:#77c5e7 ">BSC</h1>
                                <button type="button" class="close" data-dismiss="modal">×</button>
                            </div>


                            <div class="modal-body" style="font-family: 'Apple Color Emoji'">
                                <h4 style="font-family: 'Arial Narrow'">Nuestro BSC</h4>

                                <img src="img/sabias/BSC.PNG" class="img-responsive"
                                     style="max-width: 750px;max-height: 950px">
                                <br>

                            </div>


                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                            </div>

                        </div>
                    </div>
                </div>

            </div>

            <hr>

        </li>



        <!--<div class="row">

            <div class="col-lg-3  mb-4 py-5">
                <img class="img-fluid rounded mb-lg-5" src="img_recetas/fondoRecetario.jpg" id="img_receipe"
                     alt="">
            </div>

            <div class="col-lg-7  mb-4 card-body py-5" id="">
                <h3 class="col-lg-12 card-title"><a href="recetario.php" class="text-lg" style="font-size: 40px">Nuestras
                        Recetas</a>
                </h3>
                <div class="card-text text-lg-center">
                    <p class="card-text text-lg-center">
                        Nuestros recetario, elaborado con nuestros productos, obteninedo platillos
                        deliciosos.
                    </p>
                    <p class="card-text text-lg-center">
                        Si quieres conocer nuestro recestario da click en el apartado
                    </p>

                </div>
            </div>

        </div>-->

        <!--<div class="row">

            <div class="col-lg-3  mb-4 py-5">

            </div>

            <div class="col-lg-7  mb-4 card-body py-5" id="">

                <figure class="imghvr-fade-in-up">
                    <img class="img-fluid rounded mb-lg-5" src="img/fondoFelizCumple.jpg" id="img_receipe"
                         alt="">

                    <figcaption>
                        <h3 class="cih-fade-down ih-delay-sm">Cumpleañeros
                            del Mes
                        </h3>
                        <p>
                            <i class="ih-zoom-in ih-delay-md">
                                Lactalis México se complace en desearles un feliz cumpleaños a todos los colaboradores que
                                cumplen años en este mes. Gracias por brindar siempre su apoyo y esfuerzo a la compañía.
                                Deseamos que este año esté lleno de alegría, metas cumplidas y mucho éxito.
                            </i>
                        </p>
                        <a href="recetario.php" class="text-lg" style="font-size: 40px"
                           data-toggle="modal" data-target="#cumple"</a>
                    </figcaption>
                </figure>

                <div class="card-text text-lg-center">
                    <p class="card-text">
                        Lactalis México se complace en desearles un feliz cumpleaños a todos los colaboradores que
                        cumplen años en este mes. Gracias por brindar siempre su apoyo y esfuerzo a la compañía.
                        Deseamos que este año esté lleno de alegría, metas cumplidas y mucho éxito.
                    </p>
                    <p class="card-text">
                </div>
            </div>

            <div class="modal" id="cumple">
                <div class="modal-dialog modal-dialog-scrollable">
                    <div class="modal-content">


                        <div class="modal-header">
                            <h1 class="modal-title text-center">Feliz Cumpleaños</h1>
                            <button type="button" class="close" data-dismiss="modal">×</button>
                        </div>


                        <div class="modal-body" style="font-family: 'Apple Color Emoji'; background: #77c5e7">
                            <h4 style="font-family: 'Arial Narrow'">Lactalis les desea un feliz cumpleaños a
                                nuestros colaboradores..!</h4>
                            <p>PAOLA SOLIS MENDOZA</p>
                            <p>OMAR RODRIGUEZ OCHOA</p>
                            <p>DIEGO JERSAIN LOZADA CHAVEZ</p>
                            <p>LEONARDO ANDRES MATA ESCALANTE</p>
                            <p>GLORIA LARA FLORES</p>
                            <p>MIRIAM ANTONIO ALEJANDRES</p>
                            <p>JUANA MARIA,MATA MARTINEZ</p>
                            <p>TERESA BALLEZA REYNOSA</p>
                            <p>MARIA ENGRACIA ORTIZ CRUZ</p>
                            <p>MARIA ELVIA,RIOS,CEPEDA</p>
                            <p>LUIS ANTONIO LOPEZ CASTILLO</p>
                            <p>SONIA LORENA CARVAJAL MORENO</p>
                            <p>JESUS JOSEFINA SAENZ HERNANDEZ</p>
                            <p>SANDRA CORNEJO HERNANDEZ</p>
                            <p>OMAR SEGOVIA BELTRAN</p>
                            <p>MAGALY ESTRADA MENDEZ</p>
                            <p>ANTONIA ENCINAS CASTANEDA</p>
                            <p>ALEJANDRA ALCARAZ GRANADOS</p>
                            <p>SILVERIO PEREZ VARGAS</p>

                        </div>


                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>

                    </div>
                </div>
            </div>


        </div>-->


    </div>
    <br>

</div>

