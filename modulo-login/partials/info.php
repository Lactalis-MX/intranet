<div class="card text-center">
    <div class="card-header " id="acerca">
        <h3 class="text-white"><strong>Acerca de nosotros</strong></h3>
    </div>

    <div class="card-body">
        <h3 class="card-title  text-white" id="Origen">Nuestro origen</h3>
        <p class="card-text text-dark text-justify">
            En 1956 nace Derivados de Leche Esmeralda en San Miguel de Allende Guanajuato, ciudad declarada por la UNESCO como Patrimonio de la Humanidad. Iniciamos nuestras operaciones a través de la venta de dulces y posteriormente elaborando quesos, cremas y mantequillas. A lo largo de este tiempo hemos rescatado recetas y procedimientos de elaboración de quesos de diversas regiones de México, por lo cual somos considerados el auténtico queso mexicano ya que la pasión y herencia de nuestras tradiciones se nota en cada producto que llevas a tu hogar. Creemos fielmente que el sabor y calidad que nos distinguen dependen de su origen, por ello nuestros quesos están elaborados con ingredientes 100% frescos.
        </p>

        <h3 class="card-title text-white" id="Somos">¿Quiénes somos?</h3>
        <p class="card-text text-dark text-justify">
            Somos una empresa comprometida con el valor humano de sus colaboradores, con la calidad de sus prouctos y sobre todo con el comprimiso de trabajar en conjunto para poder
            alcanzar nuestros objetivos.
        </p>
        <p class="card-text">

        </p>
        <p class="card-text text-dark text-justify">
           Una empresa con estandares de calidad muy altos, siendo la #3 compañia recolectora de leche más grande, la #1 en produccion de quesos a nivel mundial, la #3 en productos
            refrigerados a nivel mundial y la #4 en el mercado global de leche.
        </p>
        <!--<a href="#" class="btn btn-primary">Algo más</a>-->
    </div>

    <div class="card-body">
        <br>

        <h3 class="card-title  text-white" id="Marcas">Nuestras Maracas</h3>
        <br>
        <div class="card-deck">
            <div class="card" id="contenido1">
                <img class="card-img-top figure-img img-fluid" src="img/galbani.png" alt="Card image cap">
                <div class="card-body text-center">
                    <h5 class="card-title card-subtitle text-lg-center"><strong>Galbani</strong></h5>
                    <p class="card-text">Nacida en un pequeño pueblo italiano en 1882, Galbani inspira a los italianos desde hace maás de 130 años,
                    gracias a sus altos estándares de calida y a una gama innovadora</p>
                    <p class="card-text">Galbani se comercializa en más de 120 países para aportar la auntentica tradición de los quesos italianos en las cuatro esquinas del mundo.</p>
                </div>
            </div>
            <div class="card">
                <img class="card-img-top" src="img/Parmalat.jpg" alt="Card image cap">
                <div class="card-body text-center">
                    <h5 class="card-title"><strong>Parmalat</strong></h5>
                    <p class="card-text">Nacida en 1962, Parmalat se convirtió rápidamente en un actor imprescindible del sector lácteo.</p>
                    <p class="card-text">GRacias a su compromiso histórico con la innovación y el desarrollo de productos con alto valor añadido,
                     Parmalat está presente en los cinco continentes.</p>
                </div>
            </div>
            <div class="card">
                <img class="card-img-top" src="img/queso_presient.png" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title card-subtitle text-lg-center"><strong>President</strong></h5>
                    <p class="card-text">Nacida en Francia en 1968, la marca Président está hoy presente en más de 150 países. La única marca global que ofrece
                    a los incondicionales del queso su producto por todo el mundo.</p>
                    <p class="card-text">Président posee una amplia gama de quesos (75%), pero también mantequillas (20%) y natas (5%)</p>
                </div>
            </div>
        </div>
    </div>

<br><br><br><br>
    <div class="container">
        <h2 class="card-title  text-white" id="Valores">Nuestros Valores</h2>
        <p class="text-justify">Nuestros valores nos caracterizan para poder alcanzar las metas y los objetivos, que como empresa estamos dispuestos por conseguir.</p>
        <br>
        <div class="card-deck">
            <div class="card bg-transparent">
                <div class="card-title bg-secondary">
                    <h2 class="text-white">Sencillez</h2>
                </div>
                <div class="card-body text-center">

                    <p class="card-text text-justify">Actuar y comunicarse de forma fraca, simple y directa</p>
                    <p class="card-text text-justify">Trabajar de forma práctica,efectiva y con sentido común</p>
                    <p class="card-text text-justify">Crear y mantener relaciones basadas en la proximidad y accesibilidad, sabiendo escuhar
                        y respetando las diferencias de criterio.
                    </p>
                    <p class="card-footer bg-light"> </p>
                </div>
            </div>

            <div class="card bg-transparent">
                <div class="card-title bg-info">
                    <h2 class="text-white">Compromiso</h2>
                </div>
                <div class="card-body text-center">
                    <p class="card-text text-justify">Trabajar con entusiasmo, eficiencia y motivación</p>
                    <p class="card-text text-justify">Estar involucrados y ser responsables por nuestros actos</p>
                    <p class="card-text text-justify">Tener espíritu emprendedor, tanto colectivo como individual</p>
                    <br><br>
                    <p class="card-footer bg-light"> </p>

                </div>
            </div>

            <div class="card bg-transparent">
                <div class="card-title bg-warning">
                    <h2 class="text-white">Ambición</h2>
                </div>
                <div class="card-body text-center">
                    <p class="card-text text-justify">Disfrutar los retos y tener siempre una visión futura.</p>
                    <p class="card-text text-justify">Perseguir resultados sobresalientes permanentemente</p>
                    <p class="card-text text-justify">Buscar mejorar continuamente</p>
                    <p></p>
                    <br><br><br>
                    <p class="card-footer bg-light"> </p>
                </div>
            </div>

        </div>
    </div>
<br><br>
    <div class="card-footer text-muted">
        <?php require 'Controllers/fechaActual.php'?>
    </div>
</div>