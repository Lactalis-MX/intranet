<div class="card border-info mb-3 ">
    <div class="card-header alert-primary ">
        <h3><strong>Politica Corporativa</strong></h3></div>
    <div class="card-body text-info">
        <h5 class="card-title ">ÍNDICE PÁGINA</h5>
        <p class="card-text">
            <a href="#obj">1. OBJETIVO</a>
            <a href="#alc">2. ALCANCE</a>
            <a href="#def">3. DEFINICIONES</a>
            <a href="#resp">4. RESPONSABILIDADES</a>
            <a href="#pol">5. POLÍTICA</a>
            <a href="#anexos">6. ANEXOS</a>
            <br>
            <a href="login.php">Inicio</a>
        </p>
    </div>
</div>

<div class="card-group">

<div class="card-deck mx-sm-3" id="obj">
    <div class="card-body">
        <h5 class="card-title alert-secondary">OBJETIVO</h5>
        <p class="card-text">Establecer los lineamientos para seleccionar al talento y ubicarlo en el puesto adecuado con base en competencias, conocimientos y experiencia, así como brindar oportunidades profesionales atractivas que permitan la permanencia de los empleados en la organización</p>
        <!--<a href="#" class="btn btn-primary">Go somewhere</a>-->
    </div>
</div>


<div class="card-text align-content-lg-center" id="alc">
    <div class="card-body">
        <h5 class="card-title alert-secondary">ALCANCE</h5>
        <p class="card-text">Esta política aplica a todo el personal que labora en Lactalis México.</p>
        <!--<a href="#" class="btn btn-primary">Go somewhere</a>-->
    </div>
</div>


<div class="card-deck mt-lg-5 align-self-auto mx-sm-3" id="def">
    <div class="card-body">
        <h5 class="card-title alert-secondary">DEFINICIONES</h5>
        <p class="card-text">
            <a>
                3.1 Requisición de personal: Es el formato que se ocupa para proceder con las altas de personal
                cuando exista una vacante o una posición de nueva creación.
            </a>
            <a>
                3.2 Descripción de Puesto: Es el formato que se ocupa para detallar las funciones y responsabilidades,
                así como las competencias y habilidades requeridas para desempeñar correctamente el puesto.

            </a>
        </p>
        <!--<a href="#" class="btn btn-primary">Go somewhere</a>-->
    </div>
</div>



<div class="card-deck mx-sm-3" id="resp">
    <div class="card-body">
        <h5 class="card-title alert-secondary">RESPONSABILIDADES</h5>
        <p class="card-text">
            <a>4.1 Jefe inmediato: Realiza y entrega la requisición de personal y descripción de puesto</a>
            <a>4.2 Director de Área: Aprueba requisición de personal</a>
            <a>4.3 Recursos Humanos: Revisa que exista la vacante y aprueba la requisición de personal</a>
            <a>4.4 Dirección General: Aprueba las requisiciones para las posiciones de nueva creación</a>
        </p>
        <!--<a href="#" class="btn btn-primary">Go somewhere</a>-->
    </div>
</div>


<div class="card-deck  mx-sm-3" id="pol">
    <div class="card-body">
        <h5 class="card-title alert-secondary">POLÍTICA GENERAL</h5>
        <p class="card-text">
            <a>5.1 El solicitante deberá informar al Gerente de Atracción de Talento sobre su necesidad de cubrir una
                posición </a>
            <a>5.2 Gerente de Atracción de Talento confirma que exista la vacante contra el HC autorizado
                (presupuesto).
            </a>
            <a>5.3 El jefe inmediato llena la requisición de personal (Anexo 1), así como la descripción del puesto
                (Anexo 2), indicando si la posición a cubrir se trata de una sustitución o de nueva creación. Nota: Es importante considerar que, si se genera una sustitución, se deberá incluir el nombre de la persona que se sustituye y deberá existir una solicitud de baja de personal. (Anexo 3).
            </a>
            <a>5.4 El solicitante recabará la firma del Director de área y entregará la requisición de personal y
                descripción de puesto al Gerente de Atracción de Talento.
            </a>
            <a>5.5 El Gerente de Atracción de Talento verifica que la requisición de personal, así como la descripción
                de puesto estén llenadas correctamente y que exista la vacante.
            </a>
        </p>
        <!--<a href="#" class="btn btn-primary">Go somewhere</a>-->
    </div>
</div>


<div class="card-deck  mx-sm-1" id="anexos">
    <div class="card-body">
        <h5 class="card-title alert-secondary">DRH-P001</h5>
        <p class="card-text">
            <a>5.6 El Gerente de Atracción de Talento recaba la firma del Director de Recursos Humanos para las sustituciones y la firma del Director de Recursos Humanos y Director General para los puestos de nueva creación.</a>
            <a>5.7 Una vez completada la documentación, el Gerente de Atracción de Talento publicará la vacante de
                forma interna y externa.
            </a>
            <a>5.8 El solicitante podrá recomendar posibles candidatos, los cuales serán incluidos en el proceso de
                reclutamiento y selección siempre y cuando cubran el perfil.
            </a>
            <a>5.9 Si hubiera una persona que este laborando dentro de la empresa y desea participar en el proceso,
                esta deberá cumplir con los requisitos establecidos en el lineamiento del plan de carrera. (Anexo 4).
            </a>
            <a>5.10 En caso de reingresos y familiares Recursos Humanos determinará si puede participar en el
                proceso.
            </a>
        </p>
        <!--<a href="#" class="btn btn-primary">Go somewhere</a>-->
    </div>
</div>
</div>