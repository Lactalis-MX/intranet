<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Templete</title>
    <title>Bienvenidos a Lactalis</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/js/bootstrap.bundle.min.js">
</head>
<body>
<?php
include 'partials/header.php';
include "partials/imagen_Index.php";
include "partials/registro.php";

$mvc    =   new MvcController();
$mvc    ->enlacesPaginasController();
?>
</body>
</html>