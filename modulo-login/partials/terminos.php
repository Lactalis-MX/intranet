<div class="container">

    <div class="card-body">
        <div class="card-header alert-info ">
            <h3><strong>AVISO DE PRIVACIDAD</strong></h3></div>
        <div class="card-body text-info">
            <h5 class="card-title ">ÍNDICE PÁGINA</h5>
            <p class="card-text col-md-12">
                <a href="#obj">1. RECOLECCION</a>
                <a href="#alc">2. CONSENTIMIENTO</a>
                <a href="#def">3. CONTACTENOS</a>
                <a href="#pol">4. VIGENCIA Y ACTAUALIZACIONES</a>
                <br>
                <a href="index.php">Principal</a>
            </p>
        </div>
        </div>



        <div class="justify-content-center card-body">
        <p class="px-sm-5 justify-content-lg-start font-weight-light text-center" >En cumplimiento a lo previsto en la Ley Federal de Protección de Datos Personales
            en Posesión de los Particulares, nos permitimos informarle lo siguiente:
            Distribuidora de Lácteos Alguil S.A. de C.V. (en lo Sucesivo “el Grupo”), señalando para efectos del presente Aviso de Privacidad el domicilio
            ubicado en Av. Lázaro Cárdenas No. 860, Col. La Nogalera, Guadalajara, Jalisco, México, C.P.44470,
            será el responsable de recabar sus datos, del uso que se les dé a los mismos y de su protección. </p>
    </div>

    <div class="justify-content-center" id="obj">
        <h4 class="text-xl-left">Recolección:</h4>
        <p class="px-sm-5 justify-content-lg-start font-weight-light text-center">
            La Recolección de los Datos Personales y/o Datos Sensibles será realizada únicamente por “el Grupo” y directamente con el titular y con su pleno consentimiento.
        </p>
        <p class="px-sm-5 justify-content-lg-start font-weight-light text-center">
            Es responsabilidad del Titular de los Datos Personales, el garantizar que los Datos, facilitados a “el Grupo” sean veraces y completos, y es responsable de comunicar a “el Grupo” cualquier modificación en los mismos a efecto de que se pueda cumplir con la obligación de mantener la información actualizada
        </p>
        <p class="px-sm-5 justify-content-lg-start font-weight-light text-center">
            Los medios para dicha recolección son: Entrevista, Papel, Teléfono y/o Medios electrónicos. Actualmente el Sitio del grupo (www.esmeralda.com.mx) no recaba ningún tipo de información de manera automatizada por medio de Cookies.
        </p>

        <p class="px-sm-5 justify-content-lg-start font-weight-light text-center">
            “Los Cookies son archivos de textos que un servidor Web almacena en el disco duro de la computadora del "USUARIO", que contienen información diversa para conocer las preferencias de acceso de información del "USUARIO" de suerte tal que "el Grupo" pueda enviar banners promocionales u ofrecer servicios personalizados. El "USUARIO" puede desactivar el uso de cookies desde su explorador y así navegar por el "SITIO" sin dificultad.”
        </p>
    </div>

<br>

    <div class="justify-content-center" id="alc">
        <h4 class="text-xl-left">Consentimiento:</h4>
        <p class="px-sm-5 justify-content-lg-start font-weight-light text-center">
            Cuando usted proporciona Información “el Grupo”, por cualquiera de los medios arriba mencionados, comprende y esta de acuerdo que “el Grupo” puede brindar esta información a algunas de sus afiliadas o socios comerciales.
        </p>
    </div>

    <br>

    <div class="justify-content-center" id="def">
        <h4 class="text-xl-left">Contáctenos:</h4>
        <p class="px-sm-5 justify-content-lg-start font-weight-light text-center">

            A partir de este Instante usted podrá hacer ejercicio de sus Derechos ARCO. (Acceso, rectificación, cancelación u oposición de sus datos Personales)
        </p>

        <p class="px-sm-5 justify-content-lg-start font-weight-light text-center">
            Para éste efecto la recepción, registro y atención de sus solicitudes, se llevará a cabo por escrito en el domicilio arriba mencionado.
        </p>

        <p class="px-sm-5 justify-content-lg-start font-weight-light text-center">
            Para ser aceptada por “el Grupo”, la solicitud ARCO que se le presente deberá contener y acompañar todo lo señalado en el Art. 29 de la Ley.
        </p>
    </div>

    <br>

    <div class="justify-content-center" id="pol">
        <h4 class="text-xl-left">Vigencia y Actualizaciones:</h4>
        <p class="px-sm-5 justify-content-lg-start font-weight-light text-center">
            Estas políticas de privacidad se encuentran vigentes desde la fecha de su publicación en el sitio “www.esmeralda.com.mx” y tienen una vigencia indefinida; "el Grupo" puede actualizarlas en cualquier momento que lo estime necesario o cuando las normatividades aplicables lo exijan, por lo que se recomienda al "USUARIO" su lectura frecuente para estar enterado y adherirse o rechazar su contenido.
        </p>
    </div>

        <br>

        <div class="card-footer">
            <div class="blockquote-footer">
                <div class="footer-copyright text-center py-3">© 2018 Copyright:
                    <a href="http://www.esmeralda.com.mx/"> Esmeralda.com.mx</a>
                </div>
            </div>
        </div>

    </div>


</div>