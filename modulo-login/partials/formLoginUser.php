<script src="assets/js/validarRegistro.js"></script>
<div class="caja">

    <!--<script type="text/javascript">

        function  validacion() {
            var user   =   document.forms["Login"]["numUser"].value;
            var pass   =    document.forms["Login"]["passwordAccess"].value;
            if (user==""){
              //  swal("Oops, Algo salió mal!!", "error");
                //alert("Debe ingresar su número de empleado");
                swal({
                    title:"error",
                    text:"Su número de usuario es obligatorio",
                    icon:"error",
                    timer:5000,
                });
                return false;
            } else if (pass ==''){
                swal({
                    title:"error",
                    text:"Su contraseña es obligatoria",
                    icon:"error",
                    timer:5000,
                });
                return false;
            }
        }
    </script>-->
    <!--<script src="assets/js/validarRegistro.js" type="text/javascript"></script>-->
    <script type="text/javascript">
        function mayus(e) {
            e.value = e.value.toUpperCase();
        }
    </script>

    <form name="Login" id="Login" method="POST" onsubmit="return validarRegistro()">
        <h2>Inicio de Sesión</h2>
        <label> Seleccione la empresa en la que labora</label>
        <br>
        <div class="">
            <select  name="Enterprises" id="Enterprises" class="dropdown">
                <option value="0" class="dropdown-item">Seleccione una empresa</option>
                <option value="Delesa" class="dropdown-item">DERIVADOS DE LECHE LA ESMERALDA</option>
                <option value="Dilasa" class="dropdown-item">DISTRIBUIDORA DE LACTEOS ALGIL</option>
                <option value="Dasa" class="dropdown-item">DISTRIBUIDORA ALGIL</option>
            </select>
        </div>
<br>
        <label for="NoEmp">Número de Empleado <span> </span></label>
        <input type="text" name="numUser" placeholder="Ingrese su número de empleado" id="NoEmp" class="form-control"
               maxlength="6">
        <label for="EmpPass">RFC de Usuario</label>
        <input type="text" name="passwordAccess" placeholder="Ingrese su rfc" id="EmpPass" class="form-control"
               maxlength="10" onkeyup="mayus(this);">
        <div id="wait"
             style="display:none;width:69px;height:89px;border:1px solid black;position:absolute;top:50%;left:50%;padding:2px;">
            <img src='img/spinner.gif' width="64" height="64"/><br>Loading..
        </div>


        <div class="md-form ml-xl-5 py-0">
            <p>
                <input type="checkbox" id="terminos">
                <a href="avisoPrivacidad.php">
                    Acepta términos y condiciones
                </a>
            </p>
        </div>

        <!--<input type="submit" value="Enviar" class="btn btn-outline-success" onclick="this.disabled=true; this.value='enviando.....';this.form.submit()">-->
        <button type="submit" value="Enviar" class="btn-lg btn-primary" id="btnLogin"
                onsubmit="this.disabled=false; this.value='Enviando..!!';this.form.submit(); return validarRegistro()">
            Enviar
        </button>
        <br>

        <?php
        //   require 'partials/conditions.php';
        require 'partials/forgetPass.php';
        ?>

    </form>

</div>

