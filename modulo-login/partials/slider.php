<h1 class="col-lg-11 text-center" id="title" style="font-family: 'Arial Narrow'"><b>Últimos Comunicados</b></h1>

<hr>
<div class="row" id="">
    <div class="card-body">

        <div id="comunicados" class="carousel slide  " data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#comunicados" data-slide-to="0" class="active"></li>
                <li data-target="#comunicados" data-slide-to="1" class="active"></li>
                <li data-target="#comunicados" data-slide-to="2" class="active"></li>
                <li data-target="#comunicados" data-slide-to="3" class="active"></li>
                <li data-target="#comunicados" data-slide-to="4" class="active"></li>
                <li data-target="#comunicados" data-slide-to="5" class="active"></li>
                <li data-target="#comunicados" data-slide-to="6" class="active"></li>
                <li data-target="#comunicados" data-slide-to="7" class="active"></li>
                <li data-target="#comunicados" data-slide-to="8" class="active"></li>
                <li data-target="#comunicados" data-slide-to="9" class="active"></li>
                <li data-target="#comunicados" data-slide-to="10" class="active"></li>
                <li data-target="#comunicados" data-slide-to="12" class="active"></li>
                <li data-target="#comunicados" data-slide-to="13" class="active"></li>
                <li data-target="#comunicados" data-slide-to="14" class="active"></li>
                <li data-target="#comunicados" data-slide-to="15" class="active"></li>
                <li data-target="#comunicados" data-slide-to="16" class="active"></li>
                <li data-target="#comunicados" data-slide-to="17" class="active"></li>
                <li data-target="#comunicados" data-slide-to="18" class="active"></li>
                <li data-target="#comunicados" data-slide-to="19" class="active"></li>
                <li data-target="#comunicados" data-slide-to="20" class="active"></li>

            </ol>
            <div class="carousel-inner" id="contentComunicate_1">
                <div class="carousel-item active" id="contentComunicate_1">
                    <img class="w3-center  w-25 img-responsive"
                         src="img/img_comunicados/COMUNICADO%20NUEVO_INGRES_ALBERTO_SUAREZ.png" alt=""
                         id="1_imgComunicate">
                </div>

                <div class="carousel-item" id="contentComunicate_1">
                    <img class="w3-center  w-25 img-responsive"
                         src="img/img_comunicados/COMUNICADO_ACTIVIDAD_FISICA.png" alt=""
                         id="1_imgComunicate">
                </div>


                <div class="carousel-item" id="contentComunicate_1">
                    <img class="w3-center  w-25 img-responsive"
                         src="img/img_comunicados/COMUNICADO_ASIGNACIÓN.png" alt=""
                         id="1_imgComunicate">
                </div>

                <div class="carousel-item" id="contentComunicate_1">
                    <embed src="img/img_comunicados/Prioridades_Q3_2019.pdf#toolbar=0&navpanes=0&scrollbar=0" type="application/pdf" width="90%" height="700px" />
                    <!--<iframe src="https://docs.google.com/viewer?url='../img/img_comunicados/Prioridades_Q3_2019.pdf'&embedded=true" width="600" height="780" style="border: none;"></iframe>-->
                </div>

                <div class="carousel-item " id="contentComunicate_1">
                    <img class="w3-center  w-50 img-responsive"
                         src="img/img_comunicados/semana_cita.png" alt=""
                         id="1_imgComunicate">
                </div>

                <div class="carousel-item " id="contentComunicate_1">
                    <img class="w3-center  w-25 img-responsive"
                         src="img/img_comunicados/JOVENES_CONSTRUYENDO_JUAN_ANTONIO.png" alt=""
                         id="1_imgComunicate">
                </div>

                <div class="carousel-item " id="contentComunicate_1">
                    <img class="w3-center  w-50 img-responsive"
                         src="img/img_comunicados/proveedores.png" alt="First slide"
                         id="1_imgComunicate">
                </div>

                <div class="carousel-item " id="contentComunicate_1">
                    <img class="w3-center  w-25 img-responsive"
                         src="img/img_comunicados/agenda_cita.png" alt="Secod slide"
                         id="1_imgComunicate">
                </div>

                <div class="carousel-item " id="contentComunicate_1">
                    <img class="w3-center  w-25 img-responsive"
                         src="img/img_comunicados/participacion_compras.jpg" alt=""
                         id="1_imgComunicate">
                </div>

                <div class="carousel-item " id="contentComunicate_1">
                    <img class="w3-center  w-25 img-responsive"
                         src="img/img_comunicados/feria_salud.jpg" alt=""
                         id="1_imgComunicate">
                </div>

                <div class="carousel-item " id="contentComunicate_1">
                    <img class="w3-center  w-25 img-responsive"
                         src="img/img_comunicados/becarios_3.png" alt=""
                         id="1_imgComunicate">
                </div>


                <div class="carousel-item " id="contentComunicate_1">
                    <img class="w3-center  w-25 img-responsive"
                         src="img/img_comunicados/Feria_salud.png" alt="Second slide"
                         id="1_imgComunicate">
                </div>

                <div class="carousel-item ">
                    <img class="w3-center w-25 img-responsive" src="img/img_comunicados/AVISO_URGENTE.jpg"
                         alt="" id="1_imgComunicate">
                </div>

                <div class="carousel-item" id="contentComunicate_1">
                    <img class="w3-center  w-25 img-responsive"
                         src="img/img_comunicados/ISKALI_INFORMES_FINANCIEROS.png" alt="Second slide"
                         id="1_imgComunicate">
                </div>

                <div class="carousel-item" id="contentComunicate_1">
                    <img class="w3-center  w-25 img-responsive"
                         src="img/img_comunicados/deciciones.png" alt="Second slide"
                         id="1_imgComunicate">
                </div>

                <div class="carousel-item" id="contentComunicate_1">
                    <img class="w3-center  w-25 img-responsive"
                         src="img/img_comunicados/nuevosIngresosEnRed.png" alt="Second slide"
                         id="1_imgComunicate">
                </div>

                <div class="carousel-item" id="contentComunicate_1">
                    <img class="w3-center  w-25 img-responsive"
                         src="img/img_comunicados/JOVENES%20CONSTRUYENDO%20-%20ARACELI.png" alt="Second slide"
                         id="1_imgComunicate">
                </div>

                <div class="carousel-item" id="contentComunicate_1">
                    <img class="w3-center w-50  img-responsive"
                         src="img/img_comunicados/VENTA_EMPLEADOS_JULIO.png" alt="Second slide"
                         id="1_imgComunicate">
                </div>

                <div class="carousel-item" id="contentComunicate_1">
                    <img class="w3-center w-50  img-responsive"
                         src="img/img_comunicados/BIENESTAR_Y_SALUD%20_MANEJO_DE_CARGAS.png" alt="Second slide"
                         id="1_imgComunicate">
                </div>

                <div class="carousel-item" id="contentComunicate_1">
                    <img class="w3-center w-25  img-responsive"
                         src="img/img_comunicados/BIENESTAR_Y_SALUD_DIARREAS_E_INFECCIONES.png"
                         alt="Third slide"
                         id="1_imgComunicate">
                </div>

                <div class="carousel-item" id="contentComunicate_1">
                    <img class="w3-center w-25  img-responsive"
                         src="img/img_comunicados/JOVENES_CONSTRUYEND_EL_FUTURO_KAREN.png"
                         alt="Third slide"
                         id="1_imgComunicate">
                </div>

                <div class="carousel-item">
                    <img class="w3-center w-50  img-responsive"
                         src="img/img_comunicados/CAMBIO_DE_TELEFONOS_ANALOGOS.png"
                         alt="Third slide"
                         id="1_imgComunicate">
                </div>

                <div class="carousel-item" id="contentComunicate_1">
                    <img class="w3-center w-25  img-responsive" src="img/img_comunicados/PROGRAMA_DE_RECONOCIMIENO.jpg"
                         alt="Third slide"
                         id="1_imgComunicate">
                </div>






            </div>
            <a class="carousel-control-prev bg" href="#comunicados" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon " aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next bg" href="#comunicados" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>

    </div>
</div>
<hr>