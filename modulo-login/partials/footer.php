<footer class="py-5 bg" style="background: #77c5e7">
    <div class="container" style="color: #FFFFFF">
        © 2018 Copyright:
        <a href="http://www.esmeralda.com.mx/" style="color: #FFFFFF"> Esmeralda.com.mx</a>
        <p>todos los derechos reservados</p>
        <div class="col-lg-12 py-lg-5">
            <div class="mb-3 flex-center">
                <a class="fb-ic"
                   href="https://www.facebook.com/pages/category/Industrial-Company/Vacantes-Lactalis-M%C3%A9xico-281980632337467/">
                    <img src="img/facebook.png" alt="">
                </a>
                <a class="tw-ic" href="https://twitter.com/groupe_lactalis?lang=en">
                    <img src="img/twitter.png">
                </a>

                <a class="ins-ic" href="https://www.instagram.com/galbanicheese/?hl=en">
                    <img src="img/instagram.png">
                </a>

            </div>
        </div>
    </div>
    <!-- /.container -->
</footer>
