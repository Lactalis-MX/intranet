<?php
require_once "../Controllers/Controller.php";
require_once "../Models/Crud.php";
/*
 * Clase AJAX que permite validar si algun usuario pertenece y esta dentro de alguna de las empresas seleccionadas.
 * el objetivo de ajax es devolver una respueta asincrona.
 */
class ajax
{
    public $validarUsuario;
    public $validarQueja;
    public $vairdarPass;
    public $validarEmail;
    public $vacationsDays;
    public $validateUserLogin;
/*
 * Metodo para validar si algun usuario existe en alguna BD, contiene una funcion para la conexion a BD llamada validarUser
 *
 */
    public function validarUsuariosAjax()
    {
        $datos = $this->validarUsuario;
        $respuesta = MvcController::validarUser($datos);
        echo $respuesta;

    }
/*
 * Metodos que aun no estan en uso en produccion
 */
    public function validarVacataions()
    {
        $dataRequest = $this->vacationsDays;
        $respuesta = MvcController::validarVacations($dataRequest);
        echo $respuesta;
    }

    public function sendEamil(){
        $request    =   $this->validarQueja;
        $dataReq    =   MvcController::validateQueja($request);
        echo $dataReq;
    }


    public function validateVacationsAjax()
    {
        $datos = $this->vacationsDays;
        $respuesta = MvcController::validateVacationsController($datos);
        echo $respuesta;
    }


}

/*
 *Sentencia if donde se valida si hay algun dato en el metodo $_POST['validateDataLogin']
 * La variable 'validateDataLogin', es enviada desde JS desde el archivo validarRegistro.js
 *
 */

if (isset($_POST['validateDataLogin'])) {
    $metodoValidateUser = new ajax();
    $obj = json_decode($_POST['validateDataLogin'], true);
    $metodoValidateUser->validarUsuario = $obj;
    $metodoValidateUser->validarUsuariosAjax();
}

if (isset($_POST['validarQueja'])){
    $metodoValidarQueja =   new ajax();
    $arreglo    =   json_decode($_POST['validarQueja'],true);
    $metodoValidarQueja->validarQueja   =   $arreglo;
    $metodoValidarQueja->sendEamil();
}

if (isset($_POST['valdiateDays'])) {
    $vacationsDays  =  new ajax();
    $objVacations   =   json_decode($_POST['valdiateDays'],true);
    $vacationsDays->vacationsDays   =   $objVacations;
    $vacationsDays->validarVacataions();
}


if (isset($_POST['validateVactions'])) {
    $validateDays = new Ajax();
    $validateDays->vacationsDays = $_POST['validateVactions'];
    $validateDays->validateVacationsAjax();
}
