<?php
require_once 'Controllers/Controller.php';
require_once 'Models/Crud.php';
require 'Controllers/reports.php';
session_start();
$infoUserVac = new MvcController();
$solicitud  =   new MvcController();
if (!isset($_SESSION['user'])) {
    header('Location: index.php');
}
$idUser = $_SESSION['user'];
$infoUserVac->infoUserVacations($idUser);
$arregloVac = array();
$arregloVac = $infoUserVac->infoUserVacations($idUser);
$solicitud  ->giveRequest($arregloVac);


?>
<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mis vacaciones</title>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="assets/jquery-ui-1.12.1.custom/jquery-ui.js"></script>
    <script src="assets/js/pruebaDataPicker.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="/scripts/bootstrap-datetimepicker.*js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="assets/js/fullcalendar.min.js"></script>
    <script src="assets/js/es.js"></script>
    <script src="assets/js/index.js"></script>

    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/jquery-ui-1.12.1.custom/jquery-ui.css">
    <link rel="stylesheet" href="assets/jquery-ui-1.12.1.custom/jquery-ui.structure.css">
    <link rel="stylesheet" href="assets/jquery-ui-1.12.1.custom/jquery-ui.theme.css">
    <link rel="stylesheet" href="assets/css/style_Register.css">
    <link rel="stylesheet" href="assets/css/solicitudStyle.css">
    <link rel="stylesheet" href="Content/bootstrap-datetimepicker.css"/>
    <!--<script src="assets/js/jquery.min.js"></script>-->
    <link rel="stylesheet" href="assets/css/fullcalendar.min.css">


    <!--<script src='lib/moment.min.js'></script>
    <link href='scheduler.css' rel='stylesheet' />
    <script src='jquery.js'></script>
    <script src='fullcalendar.js'></script>
    <script src='scheduler.js'></script>-->
    <script src="assets/js/dinamicSolicitud.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>

</head>

<body>

<?php
require 'partials/headerMisVacaciones.php';
#require 'Controllers/fechaActual.php';
//require 'partials/vacaciones.php';
//require 'partials/modaDemo.php';
#require 'partials/solicitud.php';
//require 'partials/vacaciones.php';
?>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong">Instrucciones</button>

<!-- Modal -->
<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Intrucciones de uso</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="card-body">
                    <!--<h5 class="card-title">Instrucciones de uso</h5>-->
                    <h6 class="card-subtitle">Siga las intrucciones para poder hacer unsa solicitud exitosa</h6>
                    <br>
                    <p class="card-text">1. Los campos de Solicitud se llenan en automatico</p>
                    <p class="card-text">2. Su número de empleado no debe contener caracteres especiales '/*.,$#\'</p>
                    <p class="card-text">3. Verifique si sus campos son correctos</p>
                    <p class="card-text">4. Debe hacer click en el botón "Enviar" que se encuentra en la parte
                        inferior</p>
                    <p class="card-text">5. Una vez hecho el paso anterior, usted debe indicar la fecha y los días que
                        requiere</p>
                    <p class="card-text">6. Si tiene días pendientes le permitirá poder hacer la solicitud de lo
                        contrario no podrá</p>
                    <!--<a href="misSolicitudes.php" class="btn btn-primary">Mis Solicitudes</a>-->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <!--<button type="reset" class="btn btn-danger" data->Borrar</button>
                    <button type="submit" class="btn btn-primary" value="Cancelar">Enviar</button>-->
                </div>
            </div>

        </div>
    </div>
</div>
</div>

<!-- Card de SOLICITUD -->
<div class="mt-lg-5 ">
    <div class="card ">
        <div class="card-body">
            <h1 class="card-title">SOLICITUD DE VACACIONES</h1>

            <div class="card-body mx-sm-5">
                <div class="card-columns justify-content-lg-center ">
                    <form method="POST" class="card-text col-sm-12" id="peticion" onsubmit="return validacionSol()">

                        <section class="card-header-pills">
                            <!--Primera seccion-->
                            <div class="input-group col-md-4">
                                <div class="form-group mx-sm-3">
                                    <label for="nameUser" class="col-form-label-lg">Nombre</label>
                                    <input type="text" name="nombreUsuario" id="nameUser" placeholder="Nombre"
                                           class="form-control" value="<?php echo $arregloVac[1] ?>" disabled>
                                </div>


                                <div class="form-group mx-sm-3">
                                    <label for="userNum" class="col-form-label-lg">No. de empleado</label>
                                    <input type="text" name="numeroEmpelado" id="userNum" placeholder="No Empleado"
                                           class="form-control-sm" maxlength="5" value="<?php echo $arregloVac[0] ?>">
                                </div>

                                <div class="form-group mx-sm-3">
                                    <label for="solicitados" class="col-form-label-lg mx-sm-3"> Solicitados</label>
                                    <input type="text" name="sol" id="solicitados" class="form-control"
                                           value=" <?php ?>">
                                </div>
                            </div>

                            <!--Segunda seccion-->
                            <div class="input-group col-md-4">
                                <div class="form-group mx-sm-3">
                                    <label for="paterno" class="col-form-label-lg mx-sm-3">Paterno</label>
                                    <input type="text" name="apellidoP" id="paterno" placeholder="Apellido Paterno"
                                           class="form-control" value="<?php echo $arregloVac[2] ?>" disabled>
                                </div>

                                <div class="form-group mx-sm-3">
                                    <label for="diasPen" class="col-form-label-lg mx-sm-3">Días Pendientes</label>
                                    <input type="text" name="diasPendientes" id="diasPen" placeholder="Departamento"
                                           class="form-control" value="<?php echo $arregloVac[15] ?>" disabled>
                                </div>


                                <div class="form-group mx-sm-3">
                                    <label for="hasta" class="col-form-label-lg mx-sm-3">Hasta</label>
                                    <input type="text" name="Hasta" id="hasta" class="form-control" value="<?php ?>">
                                </div>
                            </div>


                            <!--Tercera seccion-->
                            <div class="input-group col-md-4">
                                <div class="form-group mx-sm-3">
                                    <label for="materno" class="col-form-label-lg mx-sm-3">Materno</label>
                                    <input type="text" name="apellidoM" id="materno" placeholder="Apellido Materno"
                                           class="form-control" value="<?php echo $arregloVac['3'] ?>" disabled>
                                </div>

                                <div class="form-group mx-sm-3">
                                    <label for="desde" class="col-form-label-lg mx-sm-3">Desde</label>
                                    <input type="text" name="Desde" id="desde" class="form-control">
                                </div>

                                <div class="form-group mx-sm-3">
                                    <label for="desde" class="col-form-label-lg mx-sm-3">Presentarse</label>
                                    <input type="text" name="Desde" id="desde" class="form-control" value=" <?php ?>"
                                           disabled>
                                </div>
                            </div>
                        </section>
                        <button type="submit" class="btn-lg btn-primary btn-responsive" id="sendNotification">Enviar</button>
                        <button type="reset" class="btn-lg btn-danger btn-responsive">Limpiar</button>
                        <!--Seccion de botones-->

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="card-footer">

</section>


</body>
</html>