/*
$("#desde").datepicker({
    minDate:0,
    beforeShowDay: function () {
        $(this).datepicker('option','maxDate',$("#hasta").val());
    }
});
$('#hasta').datepicker({
    defaultDate:'+1w',
    beforeShowDay:function () {
        $(this).datepicker('option','minDate',$('#desde').val());
        if ($('#desde').val() === '') $(this).datepicker('option', 'minDate', 0);
    }
});

 */


$(document).ready(function () {
    $("#sendNotification").attr('disabled', true).css("color", "gray");


    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: 'Previo',
        nextText: 'Proximo',

        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
            'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
            'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        monthStatus: 'Ver otro mes', yearStatus: 'Ver otro año',
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        dateFormat: 'dd/mm/yy', firstDay: 0,
        initStatus: 'Selecciona la fecha ', isRTL: false
    };

    $.datepicker.setDefaults($.datepicker.regional['es']);

    //minDate: Fecha de comienzo D=días | M=mes | Y=año
    //masDate:  fecha tope D=días | M=mes | Y=año

    $("#desde").datepicker({
        minDate: "Today",
        maxDate: "+1M  +10D",
        beforeShowDay: function (day) {
            var day = day.getDay();
            if (day == 0 || day == 6) {
                return [false, "somecssclass"];

            } else {
                return [true, "someothercssclass"];

            }
        },

        onSelect: function (dateStr) {
            var min = $(this).datepicker('getDate');
            $("#hasta").datepicker('option', 'minDate', min || '0');
        }


    });

    $("#hasta").datepicker({
        minDate: "+1D", maxDate: "+2M",
        beforeShowDay: function (day) {
            var day = day.getDay();
            if (day == 0 || day == 6) {
                return [false, "somecssclass"];

            } else {
                return [true, "someothercssclass"];

            }
        },

        onSelect: function (dateStr) {
            var max = $(this).datepicker('getDate');
            $('#datepicker').datepicker('option', 'maxDate', max || '+1Y+6M');
            var start = $('#desde').datepicker('getDate');
            var end = $('#hasta').datepicker('getDate');
            var idUser = $("#userNum").val();
            var convertToDays = (1000 * 60 * 60 * 24);
            var days = (end - start) / convertToDays;
            switch (days) {
                case 1:
                    $('#solicitados').val(days);
                    console.log(days);
                    break;
                case 2:
                    $('#solicitados').val(days);
                    console.log(days);
                    break;
                case 3:
                    var lessWekend = 2;
                    var finalDays = days - lessWekend;
                    $('#solicitados').val(finalDays);
                    console.log(finalDays);
                    break;

                case 4:
                    var lessWekend = 2;
                    var finalDays = days - lessWekend;
                    $('#solicitados').val(finalDays);
                    console.log(finalDays);
                    break;

                case 5:
                    var lessWekend = 2;
                    var finalDays = days - lessWekend;
                    $('#solicitados').val(finalDays);
                    console.log(finalDays);
                    break;
                case 6:
                    var lessWekend = 2;
                    var finalDays = days - lessWekend;
                    $('#solicitados').val(finalDays);
                    console.log(days);
                    break;
                case 7:
                    var lessWekend = 2;
                    var finalDays = days - lessWekend;
                    $('#solicitados').val(finalDays);
                    console.log(days - 2);
                    break;
                case 8:
                    var lessWekend = 2;
                    var finalDays = days - lessWekend;
                    $('#solicitados').val(finalDays);
                    console.log(days - 2);
                    break;
                case 9:
                    var lessWekend = 2;
                    var finalDays = days - lessWekend;
                    $('#solicitados').val(finalDays);
                    console.log(days - 2);
                    break;
                case 10:
                    var lessWekend = 4;
                    var finalDays = days - lessWekend;
                    $('#solicitados').val(finalDays);
                    console.log(days - 4);
                    break;

                case 11:
                    var lessWekend = 4;
                    var finalDays = days - lessWekend;
                    $('#solicitados').val(finalDays);
                    console.log(days - 4);
                    break;
                case 12:
                    var lessWekend = 4;
                    var finalDays = days - lessWekend;
                    $('#solicitados').val(finalDays);
                    console.log(days - 4);
                    break;
                case 13:
                    var lessWekend = 4;
                    var finalDays = days - lessWekend;
                    $('#solicitados').val(finalDays);
                    console.log(days - 4);
                    break;

                case 14:
                    var lessWekend = 4;
                    var finalDays = days - lessWekend;
                    $('#solicitados').val(finalDays);
                    console.log(days - 4);
                    break;
                case 15:
                    var lessWekend = 4;
                    var finalDays = days - lessWekend;
                    $('#solicitados').val(finalDays);
                    console.log(days - 4);
                    break;
                case 16:
                    var lessWekend = 6;
                    var finalDays = days - lessWekend;
                    $('#solicitados').val(finalDays);
                    console.log(days - 6);
                    break;

                case 17:
                    var lessWekend = 6;
                    var finalDays = days - lessWekend;
                    $('#solicitados').val(finalDays);
                    console.log(days - 4);
                    break;

                case 18:
                    var lessWekend = 6;
                    var finalDays = days - lessWekend;
                    $('#solicitados').val(finalDays);
                    console.log(days - 6);
                    break;

                case 19:
                    var lessWekend = 6;
                    var finalDays = days - lessWekend;
                    $('#solicitados').val(finalDays);
                    console.log(days - 6);
                    break;
                case 20:
                    var lessWekend = 6;
                    var finalDays = days - lessWekend;
                    $('#solicitados').val(finalDays);
                    console.log(days - 6);
                    break;
                case 21:
                    var lessWekend = 6;
                    var finalDays = days - lessWekend;
                    $('#solicitados').val(finalDays);
                    console.log(days - 6);
                    break;
                case 22:
                    var lessWekend = 6;
                    var finalDays = days - lessWekend;
                    $('#solicitados').val(finalDays);
                    console.log(days - 6);
                    break;

                case 23:
                    var lessWekend = 6;
                    var finalDays = days - lessWekend;
                    $('#solicitados').val(finalDays);
                    console.log(days - 6);
                    break;
                case 17:
                    $('#solicitados').val(days - 4);
                    break;
                case 26:
                    $('#solicitados').val(days - 8);
                    console.log(days - 8);
                    break;
                case 27:
                    $('#solicitados').val(days - 8);
                    console.log(days - 8);
                    break;
                case 28:
                    $('#solicitados').val(days - 8);
                    console.log(days - 8);
                    break;
                case 29:
                    $('#solicitados').val(days - 8);
                    console.log(days - 8);
                    break;
                case 30:
                    $('#solicitados').val(days - 8);
                    console.log(days - 8);
                    break;

                case 33:
                    $('#solicitados').val(days - 10);
                    console.log(days - 10);
                    break;


                /*
                $.ajax({
                    url: "modulos/ajax.php",
                    method: "POST",
                    data: datos,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (repuesta) {
                        console.log("Respuesta de los días" + repuesta);
                    }

                });

                 */

            }

            var datosArray = {From: start, To: end, User: idUser, Dias: finalDays};
            var dataString = JSON.stringify(datosArray);
            var dataSend = new FormData();
            dataSend.append("valdiateDays", dataString);
            console.log("Días calculados: " + dataString);

            $.ajax({
                url: "modulos/ajax.php",
                method: "POST",
                data: dataSend,
                cache: false,

                contentType: false,
                processData: false,
                success: function (respuesta) {


                   if (respuesta ==  0){
                       console.log("La credenciales coinciden");
                       $("#solicitados").attr('disabled',true);
                       $("#solicitados").show();
                       $("#sendNotification").removeAttr('disabled',true).css('color','white');
                   } else if (respuesta ==  1){
                       $("#solicitados").hide();
                       $("#sendNotification").attr('disabled', true).css("color", "gray");
                       console.log("Las credenciales no coinciden");
                   }


                }
            });

        }


    });


});