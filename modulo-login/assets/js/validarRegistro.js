//Validar registro con AJAX
$(document).ready(function () {
    $("input").attr("disabled", true);
    $("#btnLogin").attr('disabled', true).css("color", "gray");



    $("#Enterprises").change(function () {
        if ($("#Enterprises").val() === "0") {
            $("input").attr("disabled", true);
            $("#btnLogin").attr('disabled', true).css("color", "gray");
        } else {
            $("input").prop("disabled", false);
            $("#btnLogin").removeAttr('disabled', false).css("color", "white");
        }
    });


    $("input").change(function () {
        var selection = $("#Enterprises").val();
        console.log("Lo seleccionado es: " + selection);

        var idUser = $("#NoEmp").val();
        var passUser = $("#EmpPass").val();


        dataPassUser = passUser.toUpperCase();
        console.log("Prueba de letras" + passUser);

        var dataArray = {User: idUser, pass: dataPassUser, Select: selection};
        var dataString = JSON.stringify(dataArray);
        var dataSend = new FormData();
        dataSend.append("validateDataLogin", dataString);

        console.log("Estos son los datos obtenidos: " + dataString);


        $.ajax({
            url: "modulos/ajax.php",
            method: "POST",
            data: dataSend,
            cache: false,

            contentType: false,
            processData: false,
            always: function () {
                $(document).ajaxComplete(function () {
                    $("#btnLogin").submit(function () {
                        //$("#spinner").show();

                    });
                });
            },

            success: function (respuesta) {
                console.log("repuesta de php: " + respuesta);
                if (respuesta == 0) {
                    setTimeout(function () {
                        $("label[for='NoEmp'] span").html('<p style="color: green;font-size: 15px;font-family: "Helvetica Neue", Helvetica, Arial, sans-serif">' +
                            ' Sus Datos Son Correctos</p>').delay(500).fadeIn("slow");
                        /* swal({
                             title: "Correcto",
                             text: "El usuario es correcto",
                             icon: "success",
                             timer: 1000,
                             type: "success",

                         });

                         */
                        //  $("label[for='NoEmp'] span").html('<p style="color: green">Tus datos son correctos</p>').fadeOut(1000);
                        $("#btnLogin").removeAttr('disabled', true).css("color", "white");

                    });

                } else if (respuesta == 1) {
                    setTimeout(function () {
                        $("label[for='NoEmp'] span").html('<p style="color: red;font-size: 15px;font-family: "Helvetica Neue", Helvetica, Arial, sans-serif">' +
                            'Error al verificar sus datos</p>').delay(500).fadeIn("slow");
                        $("#btnLogin").attr('disabled', true).css("color", "gray");
                    });

                    // $("#btnLogin").attr('disabled', true).css("color", "gray");
                } else if (respuesta == 2) {
                    setTimeout(function () {
                        $("label[for='NoEmp'] span").html('<p style="color: red;font-size: 15px;font-family: "Helvetica Neue", Helvetica, Arial, sans-serif">' +
                            'Usted no pertenece a la empresa seleccionada</p>').delay(500).fadeIn("slow");
                        $("#btnLogin").attr('disabled', true).css("color", "gray");
                    });

                } else if (respuesta == 3) {
                    setTimeout(function () {
                        $("label[for='NoEmp'] span").html('<p style="color: red;font-size: 15px;font-family: "Helvetica Neue", Helvetica, Arial, sans-serif">' +
                            'Usted no pertenece a la empresa seleccionada</p>').delay(500).fadeIn("slow");
                        $("#btnLogin").attr('disabled', true).css("color", "gray");
                    });

                }
            },
            error: function (req) {
                console.log("Hubo un error: " + req["User"]);
            }
        });


    });
});


//Validación de Login y/o  Registro con JS

function validarRegistro() {
    //var user    =   document.querySelector("#NoEmp").value;
    //var pass    =   document.querySelector("#EmpPass").value;
    var user = document.querySelector("#NoEmp").value;
    var pass = document.querySelector("#EmpPass").value;
   var terms = document.querySelector("#terminos").checked;
    if ($("#Enterprises") != "0") {
        $("input").attr("disabled", false);
    } else if ($(this).val() === "0") {
        $("input").prop("disabled", true);
    }
    if (user != "") {
        var caracters = user.length;
        //   var caracPass = pass.length;
        var exprUser = /^[0-9]*$/;
        // var exprPass =  /^[*$]*$/;
        if (caracters > 7) {
            swal({
                text: "El número de usuario no debe ser mayor de 7 caracteres",
                icon: "warning",

            });
            return false;
        }
        if (!exprUser.test(user)) {
            swal({
                text: "El número de usuario no debe contener letras o caracteres especiales",
                icon: "warning",
            });
            return false;
        }

    } else {
        swal({
            text: "El número de usuario es necesario",
            icon: "warning",
        });
        return false;
    }

    if (pass != "") {
        var caracPass = pass.length;
        var exprPass = /^[A-Z0-9]*$/;
        if (caracPass > 10) {
            swal({
                text: "La contraseña no de tener más de 10 caracteres",
            });
            return false;
        }
        if (!exprPass.test(pass)) {
            swal({
                text: "La contraseña no debe tener minúsculas y/o caracteres espaciales",
                icon: "warning",
            });
            return false;
        }

    } else {
        swal({
            text: "La contraseña es requerida",
            icon: "warning",
        });
        return false;
    }

    if (!terms) {
        document.querySelector("#NoEmp").value = user;
        document.querySelector("#EmpPass").value = pass;
        swal({
            text: "Debe aceptar los términos para seguir navegando",
            icon: "warning",
        });
        return false;
    }
    return true;
}



function validacionSol() {
    var numUser = document.querySelector("#userNum").value;
    var dateIn = document.querySelector("#desde").value;
    var dateOut = document.querySelector("#hasta").value;
    var rquested = document.querySelector("#solicitados").value;
    var dayPend = document.querySelector("#diasPen").value;
    //var fomrulario  =   document.getElementById("#peticion").style.visibility  =   "hidden";
    //  var dateIng     =   document.querySelector("#fechaIngreso").value;
    //  var dateToday   =   document.querySelector("#fechaActual").value;
    // var user   =   document.forms["Login"]["numUser"].value;
    // var pass   =    document.forms["Login"]["passwordAccess"].value;


    if (numUser != "") {
        //  swal("Oops, Algo salió mal!!", "error");
        //alert("Debe ingresar su número de empleado");
        var caracter = numUser.length;
        var exprUsr = /^[0-9]*$/;
        if (caracter > 5) {
            swal({
                text: "El número de usuario no debe ser mayor de 5 caracteres",
                icon: 'warning'
            });
            return false;
        }
        if (!exprUsr.test(numUser)) {
            swal({
                text: "El número de usuario no debe contener letras o caracteres especiales",
                icon: "warning",
            });
            return false;
        }
        if (dateIn == "") {
            swal({
                text: 'La Fecha de Inicio no debe estar vacia, verifique por favor',
                icon: 'warning'
            });
            return false;
        }
    } else {
        swal({
            text: 'El número de usuario es indispensable',
            icon: 'warning'
        });
        return false;
    }


    if (!exprUsr.test(rquested)) {
        swal({
            text: 'Este campo no acepta números con decimales',
            icon: 'warning'
        });
        return false;

    } else if (rquested == "") {
        swal({
            text: 'Debe ingresar la cantidad de días por distrutar',
            icon: 'warning'
        });
        return false;
    }


    return true;
}


function validateRecoverPass() {
    var userEmail = document.querySelector("#emailRecoverUser").value;
    var numUser = document.querySelector("#numberRecoverUser").value;

    if (numUser != "") {
        var caracters = numUser.length;
        var exprUser = /^[0-9]*$/;

        if (caracters > 6) {
            swal({
                text: 'El número de usuario no debe ser mayor de 5 caracteres',
                icon: 'warning'
            });
            return false;
        }
        if (!exprUser.test(numUser)) {
            swal({
                text: "El número de usuario no debe contener letras o caracteres especiales",
                icon: "warning",
            });
            return false;
        } else if (userEmail == "") {
            swal({
                text: 'El correo del usuario es requerido',
                icon: 'warning'
            });
            return false;
        }


    } else {

        swal({
            text: 'El número de usuario es indispensable',
            icon: 'warning'
        });
        return false;
    }

    return true;
}


function llenar() {
    var campos = document.getElementById('').value;
    window.location.href = 'http://localhost:81/modulos/modulo-login/misVacaciones.php?campos=' + campos;
}


function validarTerm() {
    var terms = document.querySelector("#terminos").checked;

}

function ocultarPassword() {
    $(document).ready(function () {
        $('#hiddenPass').click(function () {
            if ($('#hiddenPass').is(':checked')) {
                $('#EmpPass').attr('type', 'text');
            } else {
                $('#EmpPass').attr('type', 'password');
            }

        });
    });
}

