$(document).ready(function () {
    $("#title").hover(function () {
        var title = $("#title");
        title.animate({left: '100px'}, "slow");
        title.animate({fontSize: '3em'}, "slow");
        title.animate({opacity: '0.8'});
        title.animate({height: '150px'});
    });

    $("#title_comunicados").hover(function () {
        var title = $("#title_comunicados");
        title.animate({left: '100px'}, "slow");
        title.animate({fontSize: '3em'}, "slow");
        title.animate({opacity: '0.8'});
        title.animate({height: '150px'});
    });

    $("#title_historia").hover(function () {
        var title = $("#title_historia");
        title.animate({left: '100px'}, "slow");
        title.animate({fontSize: '3em'}, "slow");
        title.animate({opacity: '0.8'});
        title.animate({height: '150px'});
    });

    /*
        $("#img_modal_1").hover(function () {
            var imagenes = $("#img_modal_1");
            imagenes.animate({height: '130px'});
        });

        $("#img_cumple").hover(function () {
            var imagenes = $("#img_cumple");
            imagenes.animate({height: '130px'});
        });
        $("#img_modal_3").hover(function () {
            var imagenes = $("#img_modal_3");
            imagenes.animate({height: '130px'});
        });
        $("#img_modal_4").hover(function () {
            var imagenes = $("#img_modal_4");
            imagenes.animate({height: '130px'});
        });

     */

    //Esta funcion muestra y oculta la seccion ""Acerca de nosotros"
    $("#title_acerca").click(function () {
        var tituloVal = $("#title_acerca");
        var contentMision = $("#cont_mision");
        var contentVision = $("#cont_vision");
        contentMision.hide(1500, "swing");
        contentMision.hide("slow");

    });


    $("#title_acerca").hover(function () {
        var tituloval = $("#title_acerca");
        var content = $("#cont_mision");
        content.show(1500, "swing");
        content.show("slow");
    });


    //Esta funcion oculta la forma ciruclar de los valores "Ambicion"
    $("#title_ambicion").click(function () {
        var title_ambicion = $("#title_ambicion");
        var cont = $("#forma_valores");
        cont.hide(2000, "swing");
        cont.hide("slow");
    });
    $("#title_ambicion").hover(function () {
        var title_ambicion = $("#title_ambicion");
        var cont = $("#forma_valores_compromiso");
        cont.show(1500, "swing");
        cont.show("slow");
    });


    $("#title_ambicion").click(function () {
        var title_ambicion = $("#title_ambicion");
        var cont = $("#forma_valores_compromiso");
        cont.hide(2000, "swing");
        cont.hide("slow");
    });
    $("#title_ambicion").hover(function () {
        var title_ambicion = $("#title_ambicion");
        var cont = $("#forma_valores");
        cont.show(1500, "swing");
        cont.show("slow");
    });

    $("#title_ambicion").click(function () {
        var title_ambicion = $("#title_ambicion");
        var cont = $("#forma_valores_sencillez");
        cont.hide(2000, "swing");
        cont.hide("slow");
    });
    $("#title_ambicion").hover(function () {
        var title_ambicion = $("#title_ambicion");
        var cont = $("#forma_valores_sencillez");
        cont.show(1500, "swing");
        cont.show("slow");
    });


    $("#img_croquetas").click(function () {
        var contenido = $("#modal_contenido");
        $("#modalCroquetas").modal();
        contenido.css("background", "fondo_modal.jpg");
    });

    $("#img_ensalada").click(function () {
        $("#modalEnsalada").modal();
    });

    $("#img_pay").click(function () {
        $("#modalPay").modal();
    });

    $("#img_tamal").click(function () {
        $("#modalTamal").modal();
    });





});