﻿<?php
session_start();
require 'Controllers/Controller.php';
require 'Models/Crud.php';
$solicitud = new MvcController();
$datos = new MvcController();
$solicitud->ingresoUserController();
$idUser = array('UserDil' => $_SESSION['user'], 'UserDel' => $_SESSION['user_del'], 'UserDasa' => $_SESSION['user_dasa']);
$empresas = $_SESSION['empresas'];
if (!isset($_SESSION['user'])  && !isset($_SESSION['user_dasa']) && !isset($_SESSION['user_del'])) {
    header('Location: index.php');
}
$datos->validacionDashBoard($idUser['UserDil'], $idUser['UserDel'], $idUser['UserDasa'], $empresas);
?>
<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bienvenidos a Lactamex</title>
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    <script src="assets/js/jquery-ui-1.12.1.custom/jquery-ui.js"></script>
    <script src="assets/js/zoomy.js"></script>
    <script src="assets/js/sliderDash.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="assets/js/envioEmail.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/dynamicDashboard.js"></script>
    <script src="assets/js/zoomImg.js"></script>
    <script src="assets/js/contadorDias.js"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-140977958-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-140977958-1');
    </script>
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/imagehover.min.css">
    <link rel="stylesheet" href="assets/css/imgStyle.css">
    <link rel="stylesheet" href="assets/css/imgEfectsStyles.css">
    <link rel="stylesheet" href="http://csshake.surge.sh/csshake.css">
    <link rel="stylesheet" href="assets/css/styleDashBoard.css">
    <link rel="stylesheet" href="assets/css/sliderStyle.css">
    <link rel="stylesheet" href="assets/css/menuStyle.css">
    <link rel="stylesheet" href="assets/css/bannerStile.css">
    <link rel="stylesheet" href="assets/css/footerStyle.css">
    <link rel="icon" href="img/grupo_lactalis.png" type="image/gif" sizes="16x16">
</head>
<body>
<nav class="navbar fixed-top navbar-expand-lg navbar- bg fixed-top" style="background: #77c5e7">
    <div class="container">
        <a class="navbar-brand" href="principal.php" id="imgLogoLactalis"><img src="img/lactalis_mexico.jpg" alt=""
                                                                               class="float-lg-left w-50"
                                                                               id="img_logo_home"></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="acerca.php" style="color: #FFFFFF;font-size: 18px">Acerca de nosotros</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="contactUs.php" style="color: #FFFFFF;font-size: 18px"">Contáctanos</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="historia.php" style="color: #FFFFFF;font-size: 18px"">Historia</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="#title" style="color: #FFFFFF;font-size: 18px"">Comunicados</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="lactajob.php" style="color: #FFFFFF;font-size: 18px"">LactaJob</a>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false" style="color: #FFFFFF;font-size: 18px"">
                    Formatos
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                        <a class="dropdown-item" href="intranet/Formatos/Formato%20de%20Servicios%20IT.pdf">Formato de
                            servicios IT</a>
                        <a class="dropdown-item" href="intranet/Formatos/FORMATO%20PRESTAMOS.pdf">Formato de
                            préstamos</a>
                        <a class="dropdown-item" href="intranet/Formatos/FORMATO%20VACACIONES.pdf">Formato de
                            vacaciones</a>
                        <a class="dropdown-item" href="intranet/Formatos/Anexo%201%20-Requisición%20de%20Personal.pdf">Requisición
                            de Personal</a>
                        <a class="dropdown-item"
                           href="intranet/Formatos/Formato Entrevista de Salida Nuevo V04062019.pdf">Formato Entevista
                            de Salida</a>
                        <a class="dropdown-item" href="intranet/Formatos/FORMATO%20DE%20MOVIM.%20DE%20PERSONAL.PDF">Formato
                            Movimiento de Personal</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false" style="color: #FFFFFF;font-size: 18px"">
                    Politicas
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                        <a class="dropdown-item" href="intranet/POLITICAS/Anexo%201%20-Requisición%20de%20Personal.pdf">Política
                            Requisición de Personal</a>
                        <a class="dropdown-item"
                           href="intranet/POLITICAS/DRH-P012-POLITICA%20UBER%20FOR%20BUSINESS.pdf">Política Uber para
                            Negocios</a>
                        <a class="dropdown-item"
                           href="intranet/POLITICAS/Política%20de%20viajes%20nacionales%20e%20internacionales.pdf">Política
                            de Viajes</a>
                        <a class="dropdown-item" href="intranet/POLITICAS/CODIGO%20DE%20CONDUCTA.PDF">Código de
                            Conducta</a>
                        <a class="dropdown-item"
                           href="intranet/POLITICAS/Comunicado Interno - Movimientos de Personal.pdf">Movimiento de
                            Personal</a>
                    </div>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false" style="color: #FFFFFF;font-size: 18px"">
                    Configuración
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                        <a class="dropdown-item" href="profile.php">Perfil</a>
                        <a class="dropdown-item" href="Controllers/cerrarSesion.php">Cerrar sesión</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
<br><br><br>
<header>
    <div id="demo" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ul class="carousel-indicators">
            <li data-target="#demo" data-slide-to="0" class="active"></li>
            <li data-target="#demo" data-slide-to="1"></li>
            <li data-target="#demo" data-slide-to="2"></li>
        </ul>
        <!-- The slideshow -->
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="img/banners/header_quesos_editado.png" alt="">

            </div>
            <div class="carousel-item">
                <img src="img/marcas_esmeralda.jpg" alt="">
            </div>
            <div class="carousel-item">
                <div class="encima" style="">
                    <p>
                        Más de 80 años apasionados por los lácteos.
                    </p>
                </div>
                <img src="img/header_lactalis_galbani_editado.png" alt="">
            </div>
        </div>

        <!-- Left and right controls -->
        <a class="carousel-control-prev" href="#demo" data-slide="prev">
            <span class="carousel-control-prev-icon"></span>
        </a>
        <a class="carousel-control-next" href="#demo" data-slide="next">
            <span class="carousel-control-next-icon"></span>
        </a>

    </div>
</header>
<?php
try {
    setlocale(LC_ALL, "es_MX", "esp_esp");
    $fechaActual = strftime("%A %d de %B del %Y");
} catch (Exception   $exception) {
    $exception->getMessage();
}
$arregloPrueba = array();
$arregloPrueba = $datos->validacionDashBoard($idUser['UserDil'], $idUser['UserDel'], $idUser['UserDasa'], $empresas);
echo '<br>';
#
if ($arregloPrueba[0] === $idUser['UserDil']) {
    echo '<p class="text-md-center align-content-sm-center " style="font-size: 17px"><strong>' . '  ' . $arregloPrueba[1] . ' ' . $arregloPrueba[2] . ' ' . $arregloPrueba[3] . '</strong></p>';
    echo '<p class="text-md-left col-md-5" style="font-size: 18px">' . $fechaActual . '</p>';
    echo '<p class="text-md-right col-md-12 mb-4" style="font-size: 18px;color: #0b2e13"><a href="intranet/uploads/Directorio%20Telefónico.pdf" style="background: #FFFFFF">Directorio</a></p>';
} else if($arregloPrueba[0] === $idUser['UserDel']) {
    echo '<p class="text-md-center align-content-sm-center " style="font-size: 17px"><strong>' . '  ' . $arregloPrueba[1] . ' ' . $arregloPrueba[2] . ' ' . $arregloPrueba[3] . '</strong></p>';
    echo '<p class="text-md-left col-md-5" style="font-size: 18px">' . $fechaActual . '</p>';
    echo '<p class="text-md-right col-md-12 mb-4" style="font-size: 18px;color: #0b2e13"><a href="intranet/uploads/Directorio%20Telefónico.pdf" style="background: #FFFFFF">Directorio</a></p>';
}else if($arregloPrueba[0] === $idUser['UserDasa']){
    echo '<p class="text-md-center align-content-sm-center " style="font-size: 17px"><strong>' . '  ' . $arregloPrueba[1] . ' ' . $arregloPrueba[2] . ' ' . $arregloPrueba[3] . '</strong></p>';
    echo '<p class="text-md-left col-md-5" style="font-size: 18px">' . $fechaActual . '</p>';
    echo '<p class="text-md-right col-md-12 mb-4" style="font-size: 18px;color: #0b2e13"><a href="intranet/uploads/Directorio%20Telefónico.pdf" style="background: #FFFFFF">Directorio</a></p>';
}

require_once "partials/dashBoard.php";
echo "<br>";
require_once "partials/slider.php";
#echo "<br>";
#echo "<br>";
require_once "partials/footer.php";
?>
</body>
</html>

