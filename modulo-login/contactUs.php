﻿<?php
session_start();
require_once 'Controllers/Controller.php';
require_once 'Models/Crud.php';
if (!isset($_SESSION['user'])  && !isset($_SESSION['user_dasa']) && !isset($_SESSION['user_del'])) {
    header('Location: index.php');
}
$coment =   new MvcController();
$coment ->comentarios();
?>
<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Contáctanos</title>
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    <script src="assets/js/jquery-ui-1.12.1.custom/jquery-ui.js"></script>
    <script src="assets/js/sliderDash.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!--<script src="assets/js/envioEmail.js"></script>-->
    <script src="assets/js/bootstrap.min.js"></script>
    <!--<script src="assets/js/validarRegistro.js"></script>-->


    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="http://csshake.surge.sh/csshake.css">
    <link rel="stylesheet" href="assets/css/styleDashBoard.css">
    <link rel="stylesheet" href="assets/css/sliderStyle.css">
    <link rel="stylesheet" href="assets/css/menuStyle.css">
    <link rel="stylesheet" href="assets/css/bannerStile.css">
    <link rel="stylesheet" href="assets/css/footerStyle.css">
    <link rel="icon" href="img/grupo_lactalis.png" type="image/gif" sizes="16x16">
</head>
<body>
<!-- Navigation -->
<nav class="navbar fixed-top navbar-expand-lg navbar- bg fixed-top" style="background: #77c5e7">
    <div class="container">
        <a class="navbar-brand" href="principal.php" id="imgLogoLactalis"><img src="img/lactalis_mexico.jpg" alt=""
                                                                               class="float-lg-left w-50"
                                                                               id="img_logo_home"></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="acerca.php" style="color: #FFFFFF;font-size: 18px">Acerca de nosotros</a>
                </li>
                <!--<li class="nav-item">
                    <a class="nav-link" href="misVacaciones.php" style="color: #FFFFFF;font-size: 18px">Vacaciones</a>
                </li>-->
                <li class="nav-item">
                    <a class="nav-link" href="contactUs.php" style="color: #FFFFFF;font-size: 18px"">Contáctanos</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="historia.php" style="color: #FFFFFF;font-size: 18px"">Historia</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="principal.php#title" style="color: #FFFFFF;font-size: 18px"">Comunicados</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="lactajob.php" style="color: #FFFFFF;font-size: 18px"">LactaJob</a>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false" style="color: #FFFFFF;font-size: 18px"">
                    Formatos
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                        <a class="dropdown-item" href="intranet/Formatos/Formato%20de%20Servicios%20IT.pdf">Formato de servicios IT</a>
                        <a class="dropdown-item" href="intranet/Formatos/FORMATO%20PRESTAMOS.pdf">Formato de préstamos</a>
                        <a class="dropdown-item" href="intranet/Formatos/FORMATO%20VACACIONES.pdf">Formato de vacaciones</a>
                        <a class="dropdown-item" href="intranet/Formatos/Anexo%201%20-Requisición%20de%20Personal.pdf">Requisición de Personal</a>
                        <a class="dropdown-item" href="intranet/Formatos/Formato Entrevista de Salida Nuevo V04062019.pdf">Formato  Entevista de Salida</a>
                        <a class="dropdown-item" href="intranet/Formatos/FORMATO%20DE%20MOVIM.%20DE%20PERSONAL.PDF">Formato Movimiento de Personal</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false" style="color: #FFFFFF;font-size: 18px"">
                    Politicas
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                        <a class="dropdown-item" href="intranet/POLITICAS/Anexo%201%20-Requisición%20de%20Personal.pdf">Política Requisición de Personal</a>
                        <a class="dropdown-item" href="intranet/POLITICAS/DRH-P012-POLITICA%20UBER%20FOR%20BUSINESS.pdf">Política Uber para Negocios</a>
                        <a class="dropdown-item" href="intranet/POLITICAS/Política%20de%20viajes%20nacionales%20e%20internacionales.pdf">Política de Viajes</a>
                        <a class="dropdown-item" href="intranet/POLITICAS/CODIGO%20DE%20CONDUCTA.PDF">Código de Conducta</a>
                        <a class="dropdown-item" href="intranet/POLITICAS/Comunicado Interno - Movimientos de Personal.pdf">Movimiento de Personal</a>
                    </div>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false" style="color: #FFFFFF;font-size: 18px"">
                    Configuración
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                        <a class="dropdown-item" href="profile.php">Perfil</a>
                        <a class="dropdown-item" href="Controllers/cerrarSesion.php">Cerrar sesión</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>


<br><br><br>
<!-- Page Content -->
<div class="container">

    <!-- Page Heading/Breadcrumbs -->
    <h1 class="mt-4 mb-3">
        <b class="" style="">Contáctanos</b>


    </h1>

    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="principal.php">Inicio</a>
        </li>
        <li class="breadcrumb-item active">Contacto</li>
    </ol>

    <!-- Content Row -->
    <div class="row">
        <!-- Map Column -->
        <div class="col-lg-8 mb-4">
            <!-- Embedded Google Map -->
            <iframe width="100%" height="400px" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
                    src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1330.128185005725!2d-99.16401263691837!3d19.448208456307093!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1f8bf06aaaaab%3A0xe7b1e0425411fbbf!2sDistribuidora+de+L%C3%A1cteos+Algil+S.A.+de+C.V.!5e0!3m2!1ses-419!2smx!4v1555945130953!5m2!1ses-419!2smx"></iframe>
        </div>
        <!-- Contact Details Column -->
        <div class="col-lg-4 mb-4">
            <p>
                Cto. Interior Instituto Técnico Industrial 172, Santo Tomás,
                <br> 11340 Ciudad de México, CDMX

                <br>
            </p>
            <p>
                <abbr title="Phone">P</abbr>: 01 55 5089 2800
            </p>

            <p>
                <abbr title="Hours">H</abbr>: Lunes - Viernes: 8:00 AM a 18:00 PM
            </p>
            <h3>Quejas y Sugerencias</h3>
            <p>
                Tú opinión es importante para nosotros y queremos saberla.
            </p>
            <p>
                <abbr title="Email">E</abbr>:
                <a href="mailto:name@example.com">buzon.lactamex@lactalis.com.mx</a>
            </p>
        </div>
    </div>
    <!-- /.row -->

    <!-- Contact Form -->
    <!-- In order to set the email address and subject line for the contact form go to the bin/contact_me.php file. -->
    <div class="row">
        <div class="col-lg-7  mb-4">
            <h3></h3>
            <form name="sentMessage"  method="post" id="contactForm" >
                <div class="control-group form-group">
                    <div class="controls">
                        <label>Nombre Completo:</label>
                        <input type="text" class="form-control" id="name" required
                               data-validation-required-message="Please enter your name." name="nameUser">
                        <p class="help-block"></p>
                    </div>
                </div>
                <div class="control-group form-group">
                    <div class="controls">
                        <label>Número Telefonico:</label>
                        <input type="tel" class="form-control" id="phone" required
                               data-validation-required-message="Please enter your phone number." name="telUser">
                    </div>
                </div>
                <div class="control-group form-group">
                    <div class="controls">
                        <label>Correo Electronico:</label>
                        <input type="email" class="form-control" id="email" required
                               data-validation-required-message="Please enter your email address." name="emailUser">
                    </div>
                </div>
                <div class="control-group form-group">
                    <div class="controls">
                        <label>Mensaje:</label>
                        <textarea rows="10" cols="100" class="form-control" id="mensaje" required
                                  data-validation-required-message="Please enter your message" maxlength="999"
                                  style="resize:none" name="comentarios"></textarea>
                    </div>
                </div>
                <div id="success"></div>
                <!-- For success/fail messages -->
                <button type="submit" class="btn btn-primary" id="sendMessageButton">Enviar Mensaje</button>
            </form>
        </div>
        <div class="col-lg-4 mb-lg-5">
            <img src="img/contactos.png" alt="" class="">
        </div>
    </div>
    <!-- /.row -->

</div>
<!-- /.container -->

<!-- Footer -->
<footer class="py-5 bg" style="background: #77c5e7">
    <div class="container" style="color: #FFFFFF">
        © 2018 Copyright:
        <a href="http://www.esmeralda.com.mx/" style="color: #FFFFFF"> Esmeralda.com.mx</a>
        <p>todos los derechos reservados</p>
        <div class="col-lg-12 py-lg-5">
            <div class="mb-3 flex-center">
                <a class="fb-ic"
                   href="https://www.facebook.com/pages/category/Industrial-Company/Vacantes-Lactalis-M%C3%A9xico-281980632337467/">
                    <img src="img/facebook.png" alt="">
                </a>
                <a class="tw-ic" href="https://twitter.com/groupe_lactalis?lang=en">
                    <img src="img/twitter.png">
                </a>

                <a class="ins-ic" href="https://www.instagram.com/galbanicheese/?hl=en">
                    <img src="img/instagram.png">
                </a>

            </div>
        </div>
    </div>
    <!-- /.container -->
</footer>
</body>
</html>
