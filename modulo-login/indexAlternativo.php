<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" />	<title>
        Lactamex | Inicio
    </title>
    <link rel="sitemap" type="application/xml" title="Sitemap" href="http://empresaspolar.com/sitemap.xml" />
    <meta name="description" content="Lactamex | Inicio"/>

    <meta content="Sitio Web Corporativo de Empresas Polar" property="og:title">
    <meta content="Sitio Web Corporativo de Empresas Polar." property="og:description">
    <meta content="website" property="og:type">
    <meta content="http://empresaspolar.com/img/app/facebook_metadata.jpg" property="og:image">
    <meta content="http://empresaspolar.com/" property="og:url">
    <meta content="Empresas Polar" property="og:site_name">
    <meta content="http://empresaspolar.com/" property="og:see_also">

    <script src="http://empresaspolar.com/js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="http://empresaspolar.com/js/jquery-ui-1.10.4.min.js" type="text/javascript"></script>
    <script src="http://empresaspolar.com/js/jquery.bvalidator.js" type="text/javascript"></script>
    <script src="http://empresaspolar.com/js/jquery.mousewheel.min.js" type="text/javascript"></script>
    <script src="http://empresaspolar.com/js/perfect-scrollbar.js" type="text/javascript"></script>
    <script src="http://empresaspolar.com/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="http://empresaspolar.com/js/prefixfree.min.js" type="text/javascript"></script>

    <link rel="stylesheet" type="text/css" href="http://empresaspolar.com/css/ui-lightness/jquery-ui-1.10.4.min.css" media="screen, projection">
    <link rel="stylesheet" type="text/css" href="http://empresaspolar.com/css/bootstrap.css" media="screen, projection">
    <link rel="stylesheet" type="text/css" href="http://empresaspolar.com/css/jquery.bvalidator.css" media="screen, projection">
    <link rel="stylesheet" type="text/css" href="http://empresaspolar.com/css/perfect-scrollbar.css" media="screen, projection">
    <link rel="stylesheet" type="text/css" href="http://empresaspolar.com/css/css_elements.css" media="screen, projection">
    <link rel="stylesheet" type="text/css" href="http://empresaspolar.com/css/css_app.css" media="screen, projection">
</head>
<body>
<script>
    $('document').ready(function()
    {
        $('.navegacion-bloque').each(function()
        {
            cant = $(this).children('.navegacion-aqui').children('.navegacion-contenido').length;
            alto = (cant + 1) * parseInt($(this).children('.navegacion-tapa').height());
            desp = alto * -1;
            $(this).css('transform', 'translate(0, ' + desp + 'px)');
            $(this).show();
        });

        $('.navegacion').hover(
            function()
            {
                $(this).siblings('.navegacion-bloque').css('transform', 'translate(0, 0)');
            },
            function()
            {
                cant = $(this).siblings('.navegacion-bloque').children('.navegacion-aqui').children('.navegacion-contenido').length;
                alto = (cant + 1) * parseInt($(this).siblings('.navegacion-bloque').children('.navegacion-tapa').height());
                desp = alto * -1;
                $(this).siblings('.navegacion-bloque').css('transform', 'translate(0, ' + desp + 'px)');
            }
        );

        $('.navegacion-bloque').hover(
            function()
            {
                $(this).css('transform', 'translate(0, 0)');
            },
            function()
            {
                cant = $(this).children('.navegacion-aqui').children('.navegacion-contenido').length;
                alto = (cant + 1) * parseInt($(this).children('.navegacion-tapa').height());
                desp = alto * -1;
                $(this).css('transform', 'translate(0, ' + desp + 'px)');
            }
        );

        function animacion_menu(bloque, cuantos, but, altu, despla)
        {
            if(cuantos > 0)
            {
                contador = 0;
                countdown = cuantos;
                bloque.siblings('.navegacion-bloque').children('.navegacion-aqui').children('.navegacion-contenido').each(function()
                {
                    if(contador >= but)
                    {
                        desp = (despla - (countdown * altu)) * -1;

                        console.log(desp + ' ' + despla + ' ' + countdown + ' ' + altu);

                        $(this).css('transform', 'translate(0, ' + desp + 'px)');

                        countdown--;
                    }
                    contador++;

                });
                but++;
                cuantos--;
                despla = despla - altu;
                setTimeout(function()
                {
                    animacion_menu(bloque, cuantos, but, altu, despla);
                }, 100);
            }
        }
    });
</script>
<nav class="hidden-xs navbar navbar-default menu" role="navigation">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle menu-boton" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Desplegar navegación</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand menu-alto menu-home" href=""><img src=""/></a> <!-- PRODUCTION -->
                </div>

                <div class="collapse navbar-collapse navbar-ex1-collapse menu-colapse">
                    <ul class="nav navbar-nav menu-derecha">

                        <li style="z-index:11">
                            <div class="navegacion menu-estilo">
                                <a href="http://empresaspolar.com/gente-polar"><span id="menu_1" style="z-index:10000000;"class="llamar ">Gente Polar</span></a>
                            </div>

                            <div class="navegacion-parche"></div>
                            <div class="navegacion-bloque">
                                <div class="navegacion-tapa"></div>
                                <div class="navegacion-aqui">

                                    <div class="navegacion-contenido">
                                        <div class="navegacion-todo">
                                            <div class="navegacion-lista">
                                                <a href="http://empresaspolar.com/gente-polar/quehacer">Quehacer</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="navegacion-contenido">
                                        <div class="navegacion-todo">
                                            <div class="navegacion-lista">
                                                <a href="http://empresaspolar.com/gente-polar/razon-de-ser">Razón de Ser</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="navegacion-contenido">
                                        <div class="navegacion-todo">
                                            <div class="navegacion-lista">
                                                <a href="http://empresaspolar.com/gente-polar/como-somos">Cómo Somos</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="navegacion-contenido">
                                        <div class="navegacion-todo">
                                            <div class="navegacion-lista">
                                                <a href="http://empresaspolar.com/gente-polar/filosofia-polar">Filosofía Polar</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="navegacion-contenido">
                                        <div class="navegacion-todo">
                                            <div class="navegacion-lista">
                                                <a href="http://empresaspolar.com/gente-polar/que-nos-distingue">Qué nos Distingue</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="navegacion-contenido">
                                        <div class="navegacion-todo">
                                            <div class="navegacion-lista">
                                                <a href="http://empresaspolar.com/gente-polar/beneficios">Beneficios</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="navegacion-contenido">
                                        <div class="navegacion-todo">
                                            <div class="navegacion-lista">
                                                <a href="http://empresaspolar.com/gente-polar/voluntariado">Voluntariado</a>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </li>

                        <li style="z-index:10">
                            <div class="navegacion menu-estilo">
                                <a href="http://empresaspolar.com/nuestra-historia"><span id="menu_1" style="z-index:10000000;"class="llamar ">Nuestra Historia</span></a>
                            </div>

                            <div class="navegacion-parche"></div>
                            <div class="navegacion-bloque">
                                <div class="navegacion-tapa"></div>
                                <div class="navegacion-aqui">

                                    <div class="navegacion-contenido">
                                        <div class="navegacion-todo">
                                            <div class="navegacion-lista">
                                                <a href="http://empresaspolar.com/nuestra-historia/historia">Historia</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="navegacion-contenido">
                                        <div class="navegacion-todo">
                                            <div class="navegacion-lista">
                                                <a href="http://empresaspolar.com/nuestra-historia/cronologia">Cronología</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="navegacion-contenido">
                                        <div class="navegacion-todo">
                                            <div class="navegacion-lista">
                                                <a href="http://empresaspolar.com/nuestra-historia/pioneros">Pioneros</a>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </li>

                        <li style="z-index:9">
                            <div class="navegacion menu-estilo">
                                <a href="http://empresaspolar.com/negocios-y-marcas"><span id="menu_1" style="z-index:10000000;"class="llamar ">Negocios y Marcas</span></a>
                            </div>

                            <div class="navegacion-parche"></div>
                            <div class="navegacion-bloque">
                                <div class="navegacion-tapa"></div>
                                <div class="navegacion-aqui">

                                    <div class="navegacion-contenido">
                                        <div class="navegacion-todo">
                                            <div class="navegacion-lista">
                                                <a href="http://empresaspolar.com/negocios-y-marcas/cerveceria-polar">Cervecería Polar</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="navegacion-contenido">
                                        <div class="navegacion-todo">
                                            <div class="navegacion-lista">
                                                <a href="http://empresaspolar.com/negocios-y-marcas/alimentos-polar">Alimentos Polar</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="navegacion-contenido">
                                        <div class="navegacion-todo">
                                            <div class="navegacion-lista">
                                                <a href="http://empresaspolar.com/negocios-y-marcas/pepsi-cola-venezuela">Pepsi-Cola Venezuela</a>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </li>

                        <li style="z-index:8">
                            <div class="navegacion menu-estilo">
                                <a href="http://empresaspolar.com/compromiso-social"><span id="menu_1" style="z-index:10000000;"class="llamar ">Compromiso Social</span></a>
                            </div>

                            <div class="navegacion-parche"></div>
                            <div class="navegacion-bloque">
                                <div class="navegacion-tapa"></div>
                                <div class="navegacion-aqui">

                                    <div class="navegacion-contenido">
                                        <div class="navegacion-todo">
                                            <div class="navegacion-lista">
                                                <a href="http://empresaspolar.com/compromiso-social/modelo-de-gestion">Modelo de Gestión</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="navegacion-contenido">
                                        <div class="navegacion-todo">
                                            <div class="navegacion-lista">
                                                <a href="http://empresaspolar.com/compromiso-social/foco-modelo-conceptual">Foco Modelo Conceptual</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="navegacion-contenido">
                                        <div class="navegacion-todo">
                                            <div class="navegacion-lista">
                                                <a href="http://empresaspolar.com/compromiso-social/centro-de-desarrollo-deportivo">Centro de Desarrollo Deportivo</a>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </li>

                        <li style="z-index:7">
                            <div class="navegacion menu-estilo">
                                <a href="http://empresaspolar.com/consumo-sustentable"><span id="menu_1" style="z-index:10000000;"class="llamar ">Consumo Sustentable</span></a>
                            </div>

                            <div class="navegacion-parche"></div>
                            <div class="navegacion-bloque">
                                <div class="navegacion-tapa"></div>
                                <div class="navegacion-aqui">

                                    <div class="navegacion-contenido">
                                        <div class="navegacion-todo">
                                            <div class="navegacion-lista">
                                                <a href="http://empresaspolar.com/consumo-sustentable/comprometidos-contigo">Comprometidos Contigo</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="navegacion-contenido">
                                        <div class="navegacion-todo">
                                            <div class="navegacion-lista">
                                                <a href="http://empresaspolar.com/consumo-sustentable/bienestar-en-el-consumo">Bienestar en el Consumo</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="navegacion-contenido">
                                        <div class="navegacion-todo">
                                            <div class="navegacion-lista">
                                                <a href="http://empresaspolar.com/consumo-sustentable/conciencia-ambiental">Compromiso Ambiental</a>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </li>

                        <li style="z-index:6">
                            <div class="navegacion menu-estilo">
                                <a href="http://empresaspolar.com/sala-de-prensa"><span id="menu_1" style="z-index:10000000;"class="llamar ">Sala de Prensa</span></a>
                            </div>

                            <div class="navegacion-parche"></div>
                            <div class="navegacion-bloque">
                                <div class="navegacion-tapa"></div>
                                <div class="navegacion-aqui">

                                    <div class="navegacion-contenido">
                                        <div class="navegacion-todo">
                                            <div class="navegacion-lista">
                                                <a href="http://empresaspolar.com/sala-de-prensa/notas-de-prensa">Notas de Prensa</a>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </li>

                        <li style="z-index:5">
                            <div class="navegacion menu-estilo">
                                <a href="http://empresaspolar.com/contacto"><span id="menu_1" style="z-index:10000000;"class="llamar ">Contacto</span></a>
                            </div>

                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
</nav>


<nav class="hidden-sm hidden-md hidden-lg navbar navbar-default menu2" role="navigation">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle menu-boton" data-toggle="collapse" data-target=".navbar-ex6-collapse">
                        <span class="sr-only">Desplegar navegación</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand menu-alto" href="http://empresaspolar.com/"><img class="img-responsive" src="http://empresaspolar.com/img/app/header-logo-empresas-polar.png"/></a>
                </div>

                <div class="collapse navbar-collapse navbar-ex6-collapse">
                    <ul class="nav navbar-nav menu-derecha2">

                        <li>
                            <a href="http://empresaspolar.com/gente-polar" class="menu-estilo2"><span >Gente Polar</span></a>
                        </li>

                        <li>
                            <a href="http://empresaspolar.com/nuestra-historia" class="menu-estilo2"><span >Nuestra Historia</span></a>
                        </li>

                        <li>
                            <a href="http://empresaspolar.com/negocios-y-marcas" class="menu-estilo2"><span >Negocios y Marcas</span></a>
                        </li>

                        <li>
                            <a href="http://empresaspolar.com/compromiso-social" class="menu-estilo2"><span >Compromiso Social</span></a>
                        </li>

                        <li>
                            <a href="http://empresaspolar.com/consumo-sustentable" class="menu-estilo2"><span >Consumo Sustentable</span></a>
                        </li>

                        <li>
                            <a href="http://empresaspolar.com/sala-de-prensa" class="menu-estilo2"><span >Sala de Prensa</span></a>
                        </li>

                        <li>
                            <a href="http://empresaspolar.com/contacto" class="menu-estilo2"><span >Contacto</span></a>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
</nav>

<script>
    arreglo = [
        "logo_banner3.jpg","logo_banner16.jpg","logo_banner19.jpg","logo_banner14.jpg","logo_banner33.jpg","logo_banner24.jpg","logo_banner30.jpg","logo_banner27.jpg","logo_banner12.jpg","logo_banner5.jpg","logo_banner26.png","logo_banner10.jpg","logo_banner11.png","logo_banner25.jpg","logo_banner2.jpg","logo_banner31.jpg","logo_banner29.jpg","logo_banner13.jpg","logo_banner32.jpg","logo_banner28.jpg"];
    arreglo3 = [
        "logo_banner16.jpg","logo_banner10.jpg","logo_banner12.jpg","logo_banner11.png","logo_banner13.jpg","logo_banner14.jpg"];
    arreglo2 = [
        "imagen-banner-25.png","imagen-banner-24.jpg","imagen-banner-26.jpg","imagen-banner-3.png","imagen-banner-5.png","imagen-banner-7.png"];
    $('document').ready(function()
    {
        iniciar();

        $(window).resize(function()
        {
            iniciar();
        });

        $(window).load(function()
        {
            iniciar();
        });

        $('.banner-cargando').show();

        banner_productos(1);

        banner_productos_cerveceria(1);

        banner_billboard(1);

        $('.banner-carousel').carousel(
            {
                interval: 14000,
                pause: false
            });

        $('.banner-carousel').carousel('pause');

        /*$('.banner-carousel').on('slide.bs.carousel', function(e)
        {
            $('.contenido-productos').carousel('next');
        });*/

        $('.contenido-productos').carousel(
            {
                interval: 7000,
                pause: false
            });

        $('.contenido-productos').carousel('pause');

        $('.contenido-productos-cerveceria').carousel(
            {
                interval: 7000,
                pause: false
            });

        $('.contenido-productos-cerveceria').carousel('pause');

        /*$('.contenido-productos').on('slide.bs.carousel', function(e)
        {
            $('.banner-carousel').carousel('next');
        });*/


        $('.contenido-salaprensa').carousel('pause');

        $('.contenido-twitter').carousel('pause');

        $('.barrido').hover(
            function()
            {
                $(this).children('.contenido-derecha').children('.contenido-texto2').addClass('activo');
            },
            function()
            {
                $(this).children('.contenido-derecha').children('.contenido-texto2').removeClass('activo');
            }
        );

        $('#capas').hover(
            function()
            {
                $('#capa-superior').hide();
                $('#capa-inferior').show();
            },
            function()
            {
                $('#capa-inferior').hide();
                $('#capa-superior').show();
            }
        );

    });

    function iniciar()
    {
        $('#ticker-slider').children('.carousel-inner').children('.item').addClass('active');

        $('.ticker-tamano').height('auto');
        mayor_ticker = 0;
        $('.ticker-tamano').each(function()
        {
            if(parseInt($(this).height()) > mayor_ticker)
            {
                mayor_ticker = parseInt($(this).height());
            }
        });
        $('.ticker-tamano').height(mayor_ticker);

        $('#ticker-slider').children('.carousel-inner').children('.item').removeClass('active');
        $('#ticker-slider').children('.carousel-inner').children('.item').first().addClass('active');
    }

    function banner_productos(cual)
    {
        $('.producto-falsa' + cual).attr('src', 'http://empresaspolar.com/files/producto/banner/' + arreglo[cual - 1]).load(function()
        {
            $(this).remove();
            $('.banner-producto' + cual).css('background-image', 'url(http://empresaspolar.com/files/producto/banner/' + arreglo[cual - 1] + ')');

            if(cual < 20)
            {
                banner_productos(cual + 1);
            }
            else
            {
                $('.contenido-productos').carousel('cycle');

                $('.contenido-productos').children('.carousel-control').show();
            }
        });
    }

    function banner_productos_cerveceria(cual)
    {
        $('.producto-falsa-cerveceria' + cual).attr('src', 'http://empresaspolar.com/files/producto/banner/' + arreglo3[cual - 1]).load(function()
        {
            $(this).remove();
            $('.banner-producto-cerveceria' + cual).css('background-image', 'url(http://empresaspolar.com/files/producto/banner/' + arreglo3[cual - 1] + ')');

            if(cual < 6)
            {
                banner_productos_cerveceria(cual + 1);
            }
            else
            {
                $('.contenido-productos-cerveceria').carousel('cycle');

                $('.contenido-productos-cerveceria').children('.carousel-control').show();
            }
        });
    }

    function banner_billboard(cual)
    {
        $('.billboard-falsa' + cual).attr('src', 'http://empresaspolar.com/files/banner/imagen/' + arreglo2[cual - 1]).load(function()
        {
            $(this).remove();
            $('.fondo-banner' + cual).css('background-image', 'url(http://empresaspolar.com/files/banner/imagen/' + arreglo2[cual - 1] + ')');

            if(cual == 1)
            {
                $('.banner-cargando').hide();
            }

            //if(cual < 6)
            if(cual < 6)
            {
                banner_billboard(cual + 1);
            }
            else
            {
                $('.banner-carousel').carousel('cycle');

                $('.banner-control').show();
            }
        });
    }

</script>

<img class="producto-falsa producto-falsa1"/>

<img class="producto-falsa producto-falsa2"/>

<img class="producto-falsa producto-falsa3"/>

<img class="producto-falsa producto-falsa4"/>

<img class="producto-falsa producto-falsa5"/>

<img class="producto-falsa producto-falsa6"/>

<img class="producto-falsa producto-falsa7"/>

<img class="producto-falsa producto-falsa8"/>

<img class="producto-falsa producto-falsa9"/>

<img class="producto-falsa producto-falsa10"/>

<img class="producto-falsa producto-falsa11"/>

<img class="producto-falsa producto-falsa12"/>

<img class="producto-falsa producto-falsa13"/>

<img class="producto-falsa producto-falsa14"/>

<img class="producto-falsa producto-falsa15"/>

<img class="producto-falsa producto-falsa16"/>

<img class="producto-falsa producto-falsa17"/>

<img class="producto-falsa producto-falsa18"/>

<img class="producto-falsa producto-falsa19"/>

<img class="producto-falsa producto-falsa20"/>


<img class="producto-falsa-cerveceria producto-falsa-cerveceria1"/>

<img class="producto-falsa-cerveceria producto-falsa-cerveceria2"/>

<img class="producto-falsa-cerveceria producto-falsa-cerveceria3"/>

<img class="producto-falsa-cerveceria producto-falsa-cerveceria4"/>

<img class="producto-falsa-cerveceria producto-falsa-cerveceria5"/>

<img class="producto-falsa-cerveceria producto-falsa-cerveceria6"/>


<img class="billboard-falsa billboard-falsa1"/>

<img class="billboard-falsa billboard-falsa2"/>

<img class="billboard-falsa billboard-falsa3"/>

<img class="billboard-falsa billboard-falsa4"/>

<img class="billboard-falsa billboard-falsa5"/>

<img class="billboard-falsa billboard-falsa6"/>
<h1 style="position:absolute;opacity:0;">Inicio</h1>

<div class="banner">
    <div id="banner-carousel" class="banner-carousel slide">
        <div class="carousel-inner">


            <div class="item active">
                <a class="banner-enlace" href="http://empresaspolar.com/" target="_self">


                    <div class="banner-fondo fondo-banner1" style="background-position:center center;background-size:auto 100%;background-color:#;">

                    </div>
                </a>

            </div>

            <div class="item ">
                <a class="banner-enlace" href="http://bibliofep.fundacionempresaspolar.org/" target="_self">


                    <div class="banner-fondo fondo-banner2" style="background-position:center center;background-size:cover;background-color:#;">

                    </div>
                </a>

            </div>

            <div class="item ">
                <a class="banner-enlace" href="http://www.fundacionempresaspolar.org/files/informes/FEP_Una_Mirada_2018.pdf" target="_self">


                    <div class="banner-fondo fondo-banner3" style="background-position:center center;background-size:cover;background-color:#d5cdcf;">

                    </div>
                </a>

            </div>

            <div class="item ">
                <a class="banner-enlace" href="http://empresaspolar.com/negocios-y-marcas/cerveceria-polar" target="_self">


                    <div class="banner-fondo fondo-banner4" style="background-position:center center;background-size:cover;background-color:#;">

                    </div>
                </a>

            </div>

            <div class="item ">
                <a class="banner-enlace" href="http://empresaspolar.com/negocios-y-marcas/pepsi-cola-venezuela" target="_self">


                    <div class="banner-fondo fondo-banner5" style="background-position:center center;background-size:cover;background-color:#;">

                    </div>
                </a>

            </div>

            <div class="item ">
                <a class="banner-enlace" href="http://empresaspolar.com/negocios-y-marcas/alimentos-polar" target="_self">


                    <div class="banner-fondo fondo-banner6" style="background-position:center center;background-size:cover;background-color:#;">

                    </div>
                </a>

            </div>

        </div>
        <a class="left carousel-control banner-control">
            <div class="icon-prev" href="#banner-carousel" data-slide="prev"></div>
        </a>
        <a class="right carousel-control banner-control">
            <div class="icon-next" href="#banner-carousel" data-slide="next"></div>
        </a>
    </div>
    <div class="banner-cargando"></div>
</div>

<div class="contenido">
    <div class="contenido-sombra"></div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="contenido-interno">
                    <div class="contenido-columna">
                        <div id="capas">
                            <div id="capa-superior">
                                <div class="contenido-completo">
                                    <div class="contenido-full" style="background-image:url(http://empresaspolar.com/img/app/index/azuloscuro.png);">
                                        <h2 class="contenido-texto texto-rt">REPORTES DE TRANSPARENCIA</h2>
                                        <div class="contenido-imagen">
                                            <img class="contenido-centro img-responsive" src="http://empresaspolar.com/img/app/index/logo-alimentos-polar.png"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="capa-inferior" style="display: none !important;">
                                <div class="contenido-doble2">
                                    <a href="http://reporte.empresaspolar.com/reportes/" target="_blank">
                                        <div class="contenido-mitad full padding-r11" style="background-image:url(http://empresaspolar.com/img/app/index/rojo.png);">
                                            <h2 class="contenido-texto ct11">REPORTE DE ALIMENTOS POLAR</h2>
                                            <div class="contenido-imagen">
                                                <img class="contenido-centro img-responsive" src="http://empresaspolar.com/img/app/index/logo-alimentos-polar.png"/>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="contenido-doble2">
                                    <a href="http://empresaspolar.com/reportemateriaprima/" target="_blank">
                                        <div class="contenido-mitad padding-r12" style="background-image:url(http://empresaspolar.com/img/app/index/mostaza.png);">
                                            <h2 class="contenido-texto" style="">REPORTE DIARIO DE RECEPCIÓN DE MAÍZ</h2>
                                            <div class="contenido-imagen">
                                                <img class="contenido-centro img-responsive" src="http://empresaspolar.com/img/app/index/logo-alimentos-polar.png"/>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="http://www.empresaspolar.com/reportesinventarios/" target="_blank">
                                        <div class="contenido-mitad padding-r13" style="background-image:url(http://empresaspolar.com/img/app/index/azulclaro.png);">
                                            <h2 class="contenido-texto">REPORTE DE INVENTARIO E INSPECCIONES DE ALIMENTOS POLAR</h2>
                                            <div class="contenido-imagen">
                                                <img class="contenido-centro img-responsive" src="http://empresaspolar.com/img/app/index/logo-alimentos-polar.png"/>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="contenido-celda" style="background:#ffffff;">
                            <div id="contenido-productos-cerveceria" class="contenido-productos-cerveceria slide">
                                <div class="carousel-inner">

                                    <div class="item active">
                                        <a href="http://empresaspolar.com/negocios-y-marcas/cerveceria-polar/polar-light"><div class="contenido-fill banner-producto-cerveceria1"></div></a>
                                    </div>

                                    <div class="item ">
                                        <a href="http://empresaspolar.com/negocios-y-marcas/cerveceria-polar/solera"><div class="contenido-fill banner-producto-cerveceria2"></div></a>
                                    </div>

                                    <div class="item ">
                                        <a href="http://empresaspolar.com/negocios-y-marcas/cerveceria-polar/bodegas-pomar"><div class="contenido-fill banner-producto-cerveceria3"></div></a>
                                    </div>

                                    <div class="item ">
                                        <a href="http://empresaspolar.com/negocios-y-marcas/cerveceria-polar/polar-pilsen"><div class="contenido-fill banner-producto-cerveceria4"></div></a>
                                    </div>

                                    <div class="item ">
                                        <a href="http://empresaspolar.com/negocios-y-marcas/cerveceria-polar/polar-ice"><div class="contenido-fill banner-producto-cerveceria5"></div></a>
                                    </div>

                                    <div class="item ">
                                        <a href="http://empresaspolar.com/negocios-y-marcas/cerveceria-polar/polar-zero"><div class="contenido-fill banner-producto-cerveceria6"></div></a>
                                    </div>
                                </div>
                                <a class="left carousel-control" href="#contenido-productos-cerveceria" data-slide="prev">
                                    <div class="icon-prev"></div>
                                </a>
                                <a class="right carousel-control" href="#contenido-productos-cerveceria" data-slide="next">
                                    <div class="icon-next"></div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="contenido-columna">
                        <a href="http://empresaspolar.com/recetas">
                            <div class="contenido-celda barrido">
                                <div class="contenido-izquierda" style="background-image:url(http://empresaspolar.com/img/app/index/fondo_recetas.jpg);"></div>
                                <div class="contenido-derecha">
                                    <div class="contenido-texto2" style="background-image:url(http://empresaspolar.com/img/app/index/mostaza.png);"><div><div>RECETAS</div></div></div>
                                </div>
                            </div>
                        </a>
                        <div class="contenido-celda" style="background-image:url(http://empresaspolar.com/img/app/index/naranja.png);">
                            <h2 class="contenido-texto4">SALA DE PRENSA</h2>
                            <div id="contenido-salaprensa" class="contenido-salaprensa slide">

                                <div class="carousel-inner">

                                    <div class="item active">
                                        <div class="contenido-texto3">
                                            <a href="http://empresaspolar.com/sala-de-prensa/anmarie-camacho-tomo-las-redes-sociales-de-polar-pilsen">
                                                <div>Anmarie Camacho tomó las redes sociales de Polar Pilsen</div>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="item ">
                                        <div class="contenido-texto3">
                                            <a href="http://empresaspolar.com/sala-de-prensa/fundacion-empresas-polar-presenta-una-nueva-edicion-del-libro-lo-escribo-yo-mi-comunidad">
                                                <div>Fundación Empresas Polar presenta una nueva edición del libro Lo escribo yo, mi comunidad</div>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="item ">
                                        <div class="contenido-texto3">
                                            <a href="http://empresaspolar.com/sala-de-prensa/saqueo-en-agencia-de-pepsi-cola-venezuela-en-cabimas-incrementa-las-perdidas-para-la-compania">
                                                <div>Saqueo en agencia de Pepsi-Cola Venezuela en Cabimas incrementa las pérdidas para la compañía</div>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="item ">
                                        <div class="contenido-texto3">
                                            <a href="http://empresaspolar.com/sala-de-prensa/polar-light-se-activa-con-su-nueva-campana-publicitaria">
                                                <div>Polar Light se activa con su nueva campaña publicitaria</div>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="item ">
                                        <div class="contenido-texto3">
                                            <a href="http://empresaspolar.com/sala-de-prensa/empresas-polar-alerta-sobre-consecuencias-de-saqueos-en-sus-instalaciones-en-el-zulia">
                                                <div>Empresas Polar alerta sobre consecuencias de saqueos en sus instalaciones en el Zulia</div>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="item ">
                                        <div class="contenido-texto3">
                                            <a href="http://empresaspolar.com/sala-de-prensa/polar-light-acompana-a-wizzard-en-el-estreno-de-su-nuevo-tema-mad-love">
                                                <div>Polar Light acompaña a Wizzard en el estreno de su nuevo tema Mad Love</div>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="item ">
                                        <div class="contenido-texto3">
                                            <a href="http://empresaspolar.com/sala-de-prensa/ninos-de-monagas-fueron-premiados-por-clasificar-en-olimpiadas-de-la-historia">
                                                <div>Niños de Monagas fueron premiados por clasificar en Olimpiadas de la Historia </div>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="item ">
                                        <div class="contenido-texto3">
                                            <a href="http://empresaspolar.com/sala-de-prensa/marie-claire-harp-disfruto-unapolarcitajuntos-en-vivo-en-polarpilsen">
                                                <div>Marie Claire Harp disfrutó #UnaPolarcitaJuntos  en vivo en @polarpilsen</div>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="item ">
                                        <div class="contenido-texto3">
                                            <a href="http://empresaspolar.com/sala-de-prensa/yonathan-monsalve-rompio-record-y-gano-la-v-edicion-del-gatorade-bicirock">
                                                <div>Yonathan Monsalve rompió récord y ganó la V edición del Gatorade BiciRock</div>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="item ">
                                        <div class="contenido-texto3">
                                            <a href="http://empresaspolar.com/sala-de-prensa/lipton-te-verde-ahora-estara-disponible-en-presentacion-de-1-5-litros">
                                                <div>Lipton Té Verde ahora estará disponible en presentación de 1,5 litros</div>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                <a class="left carousel-control" href="#contenido-salaprensa" data-slide="prev">
                                    <div class="icon-prev"></div>
                                </a>
                                <a class="right carousel-control" href="#contenido-salaprensa" data-slide="next">
                                    <div class="icon-next"></div>
                                </a>

                            </div>
                        </div>
                        <a class="banner-enlace" href="http://empresaspolar.com/empleo">
                            <div class="contenido-celda barrido">
                                <div class="contenido-izquierda" style="background-image:url(http://empresaspolar.com/img/app/index/fondo_empleo.jpg);background-position: right center;"></div>
                                <div class="contenido-derecha">
                                    <div class="contenido-texto2" style="background-image:url(http://empresaspolar.com/img/app/index/azuloscuro.png);"><div><div>ÚNETE A<br/>NUESTRO EQUIPO</div></div></div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="contenido-columna">
                        <div class="contenido-celda" style="background:#ffffff;">
                            <div id="contenido-productos" class="contenido-productos slide">

                                <div class="carousel-inner">

                                    <div class="item active">
                                        <a href="http://empresaspolar.com/negocios-y-marcas/alimentos-polar/cachapas-p-a-n"><div class="contenido-fill banner-producto1"></div></a>
                                    </div>

                                    <div class="item ">
                                        <a href="http://empresaspolar.com/negocios-y-marcas/cerveceria-polar/polar-light"><div class="contenido-fill banner-producto2"></div></a>
                                    </div>

                                    <div class="item ">
                                        <a href="http://empresaspolar.com/negocios-y-marcas/pepsi-cola-venezuela/pepsi"><div class="contenido-fill banner-producto3"></div></a>
                                    </div>

                                    <div class="item ">
                                        <a href="http://empresaspolar.com/negocios-y-marcas/cerveceria-polar/polar-zero"><div class="contenido-fill banner-producto4"></div></a>
                                    </div>

                                    <div class="item ">
                                        <a href="http://empresaspolar.com/negocios-y-marcas/alimentos-polar/las-llaves"><div class="contenido-fill banner-producto5"></div></a>
                                    </div>

                                    <div class="item ">
                                        <a href="http://empresaspolar.com/negocios-y-marcas/pepsi-cola-venezuela/7up"><div class="contenido-fill banner-producto6"></div></a>
                                    </div>

                                    <div class="item ">
                                        <a href="http://empresaspolar.com/negocios-y-marcas/alimentos-polar/chiffon"><div class="contenido-fill banner-producto7"></div></a>
                                    </div>

                                    <div class="item ">
                                        <a href="http://empresaspolar.com/negocios-y-marcas/pepsi-cola-venezuela/golden"><div class="contenido-fill banner-producto8"></div></a>
                                    </div>

                                    <div class="item ">
                                        <a href="http://empresaspolar.com/negocios-y-marcas/cerveceria-polar/bodegas-pomar"><div class="contenido-fill banner-producto9"></div></a>
                                    </div>

                                    <div class="item ">
                                        <a href="http://empresaspolar.com/negocios-y-marcas/alimentos-polar/margarita"><div class="contenido-fill banner-producto10"></div></a>
                                    </div>

                                    <div class="item ">
                                        <a href="http://empresaspolar.com/negocios-y-marcas/pepsi-cola-venezuela/gatorade"><div class="contenido-fill banner-producto11"></div></a>
                                    </div>

                                    <div class="item ">
                                        <a href="http://empresaspolar.com/negocios-y-marcas/cerveceria-polar/solera"><div class="contenido-fill banner-producto12"></div></a>
                                    </div>

                                    <div class="item ">
                                        <a href="http://empresaspolar.com/negocios-y-marcas/cerveceria-polar/polar-pilsen"><div class="contenido-fill banner-producto13"></div></a>
                                    </div>

                                    <div class="item ">
                                        <a href="http://empresaspolar.com/negocios-y-marcas/alimentos-polar/lipton"><div class="contenido-fill banner-producto14"></div></a>
                                    </div>

                                    <div class="item ">
                                        <a href="http://empresaspolar.com/negocios-y-marcas/alimentos-polar/gelatina-golden"><div class="contenido-fill banner-producto15"></div></a>
                                    </div>

                                    <div class="item ">
                                        <a href="http://empresaspolar.com/negocios-y-marcas/alimentos-polar/dogourmet"><div class="contenido-fill banner-producto16"></div></a>
                                    </div>

                                    <div class="item ">
                                        <a href="http://empresaspolar.com/negocios-y-marcas/alimentos-polar/pampero"><div class="contenido-fill banner-producto17"></div></a>
                                    </div>

                                    <div class="item ">
                                        <a href="http://empresaspolar.com/negocios-y-marcas/cerveceria-polar/polar-ice"><div class="contenido-fill banner-producto18"></div></a>
                                    </div>

                                    <div class="item ">
                                        <a href="http://empresaspolar.com/negocios-y-marcas/alimentos-polar/primor"><div class="contenido-fill banner-producto19"></div></a>
                                    </div>

                                    <div class="item ">
                                        <a href="http://empresaspolar.com/negocios-y-marcas/alimentos-polar/mavesa"><div class="contenido-fill banner-producto20"></div></a>
                                    </div>
                                </div>

                                <a class="left carousel-control" href="#contenido-productos" data-slide="prev">
                                    <div class="icon-prev"></div>
                                </a>
                                <a class="right carousel-control" href="#contenido-productos" data-slide="next">
                                    <div class="icon-next"></div>
                                </a>

                            </div>
                        </div>
                        <div class="contenido-celda2" style="background-image:url(http://empresaspolar.com/img/app/index/azulceleste.png);">
                            <h2 class="contenido-texto-noticias">@EMPRESASPOLAR</h2>
                            <div id="contenido-twitter" class="contenido-twitter slide">
                                <div class="carousel-inner">
                                    <div class="item active">
                                        <div class="contenido-contenedor">
                                            <a href="https://twitter.com/EmpresasPolar" target="_blank">Nuestros valores son parte de la #FilosofíaPolar y sirven de referencia para los comportamientos y conductas de los… https://t.co/yEGGPwHTml</a><br/><br/>
                                            <a href="https://twitter.com/EmpresasPolar" target="_blank">En 1996 se consolidó la alianza estratégica acordada con el socio internacional PepsiCo, con lo que nace lo que hoy… https://t.co/0oBRSz8Fou</a><br/><br/>
                                            <a href="https://twitter.com/EmpresasPolar" target="_blank">Nuestro #VoluntariadoEP promueve incansablemente la identidad nacional a través de actividades didácticas relaciona… https://t.co/DXIWGOvsTO</a><br/><br/>
                                        </div>
                                    </div>
                                    <div class="item ">
                                        <div class="contenido-contenedor">
                                            <a href="https://twitter.com/EmpresasPolar" target="_blank">Cine foros, conciertos, talleres y conferencias se realizarán en La Casa de Estudio de la Historia de Venezuela en… https://t.co/AcrURfRCcy</a><br/><br/>
                                            <a href="https://twitter.com/EmpresasPolar" target="_blank">El Desarrollo de competencias de los docentes es un foco fundamental para #FundaciónEP y gracias  a  sus distintas… https://t.co/cfZZ5pkSNH</a><br/><br/>
                                            <a href="https://twitter.com/EmpresasPolar" target="_blank">Para nuestra marca @mimaltinpolar representa un orgullo auspiciar la Liga Colegial de Fútbol de Venezuela que contr… https://t.co/1PM5RP05Tf</a><br/><br/>
                                        </div>
                                    </div>
                                    <div class="item ">
                                        <div class="contenido-contenedor">
                                            <a href="https://twitter.com/EmpresasPolar" target="_blank">El esfuerzo y talento de nuestros trabajadores son fundamentales en el programa de #VoluntariadoEP https://t.co/NHAbqKA8Bt</a><br/><br/>
                                            <a href="https://twitter.com/EmpresasPolar" target="_blank">En #EmpresasPolar creemos firmemente que el progreso se logra a través del trabajo bien hecho tanto en las tareas c… https://t.co/0NNoGoieIV</a><br/><br/>
                                            <a href="https://twitter.com/EmpresasPolar" target="_blank">Gracias a un pequeño emprendimiento en 1941, #EmpresasPolar es actualmente una de las organizaciones más importante… https://t.co/oQAGQwueKJ</a><br/><br/>
                                        </div>
                                    </div>
                                    <div class="item ">
                                        <div class="contenido-contenedor">
                                            <a href="https://twitter.com/EmpresasPolar" target="_blank">El #VoluntariadoEP es una manifestación de lo que nos distingue como #GentePolar, somos personas comprometidas con… https://t.co/8txPpnqvvF</a><br/><br/>
                                            <a href="https://twitter.com/EmpresasPolar" target="_blank">@pabloguillen25 Hola, escríbenos por mensaje directo para brindarte mayor información. ¡Saludos!</a><br/><br/>
                                            <a href="https://twitter.com/EmpresasPolar" target="_blank">#EmpresasPolar se ha caracterizado por la dedicación  al desarrollo del ser humano, de sus capacidades y talentos,… https://t.co/mMOpDsvfxu</a><br/><br/>
                                        </div>
                                    </div>
                                    <div class="item ">
                                        <div class="contenido-contenedor">
                                            <a href="https://twitter.com/EmpresasPolar" target="_blank">#BiblioFEP está disponible a través del portal https://t.co/aopuu78f5r donde a tan solo un clic se abre un universo… https://t.co/yEizEyS6DD</a><br/><br/>
                                            <a href="https://twitter.com/EmpresasPolar" target="_blank">En @CANIAoficial involucramos a las familias en el tema nutricional con el fin de conocer los aspectos de su desarr… https://t.co/pDHHfwYDDe</a><br/><br/>
                                            <a href="https://twitter.com/EmpresasPolar" target="_blank">@saludactiva3 Buenos días, por favor escríbanos por mensaje directo con su solicitud para brindarle la información adecuada. ¡Saludos!</a><br/><br/>
                                        </div>
                                    </div>
                                </div>

                                <a class="left carousel-control" href="#contenido-twitter" data-slide="prev">
                                    <div class="icon-prev"></div>
                                </a>
                                <a class="right carousel-control" href="#contenido-twitter" data-slide="next">
                                    <div class="icon-next"></div>
                                </a>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('document').ready(function()
    {

        $('#idioma_espanol').addClass('activo');

        $('.footer-descargable').hover(
            function()
            {
                $('.footer-tooltip').show();
            },
            function()
            {
                $('.footer-tooltip').hide();
            }
        );

        $('.footer-tooltip').hover(
            function()
            {
                $(this).show();
            },
            function()
            {
                $(this).hide();
            }
        );

        $('.footer-visits').hover(
            function()
            {
                $('.footer-tooltip2').show();
            },
            function()
            {
                $('.footer-tooltip2').hide();
            }
        );

        $('.footer-tooltip2').hover(
            function()
            {
                $(this).show();
            },
            function()
            {
                $(this).hide();
            }
        );

        $('.footer-proveedor').hover(
            function()
            {
                $('.footer-tooltip3').show();
            },
            function()
            {
                $('.footer-tooltip3').hide();
            }
        );

        $('.footer-tooltip3').hover(
            function()
            {
                $(this).show();
            },
            function()
            {
                $(this).hide();
            }
        );

        $('.enlace-idioma').click(function()
        {
            aux = $(this).attr('id').split('_');
            idioma = aux[1];

            $.ajax(
                {
                    type: 'POST',
                    cache: false,
                    url: 'http://empresaspolar.com/pages/idioma/index/' + idioma,
                    dataType: 'text',
                    success: function(result)
                    {
                        document.location = result;
                        return false;
                    },
                    error: function(xhr, ajaxOptions, thrownError)
                    {
                        //alert(xhr.status);
                        //alert(thrownError);
                    }
                });
        });

        $('.footer-input').keypress(function(event)
        {
            if(event.which == 13)
            {
                cad_buscar = encodeURI($(this).val());
                url_buscar = 'http://empresaspolar.com/buscar/' + cad_buscar;
                document.location = url_buscar;
                return false;
            }
        });

        $('.enlace-buscar').click(function()
        {
            cad_buscar = encodeURI($('.footer-input').val());
            url_buscar = 'http://empresaspolar.com/buscar/' + cad_buscar;
            document.location = url_buscar;
            return false;
        });
    });
</script>
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="footer-inner">
                    <div class="footer-central">
                        <div class="footer-container">
                            <div class="footer-texto">© Empresas Polar 2015. Todos los derechos reservados. Rif: J-00006372-9 | <a href="http://empresaspolar.com/terminos-y-condiciones">Términos y Condiciones</a> | <a href="http://empresaspolar.com/aspectos-legales">Aspectos Legales</a></div>
                        </div>
                        <!--<div class="footer-container hidden-xs hidden-sm">
                            <div class="footer-contenedor footer-visits">
                                <img src="http://empresaspolar.com/img/app/footer-imagen-visitas-institucionales.png"/>
                                <span class="footer-enlace footer-visitas">Programa de Visitas</span>
                            </div>
                        </div>-->

                        <div class="footer-container hidden-xs hidden-sm">
                            <div class="footer-contenedor footer-proveedor">
                                <span class="footer-enlace footer-proveedores">Proveedor</span>

                            </div>
                        </div>

                        <!--<div class="footer-container hidden-xs hidden-sm">
                            <div class="footer-contenedor footer-descargable">
                                <span class="footer-enlace footer-descargas hidden-md">Presentación Descargable</span>
                                <img src="http://empresaspolar.com/img/app/footer-imagen-presentacion-descargable.png"/>
                            </div>
                        </div>-->
                        <div class="footer-container hidden-xs hidden-sm">
                            <div class="footer-busqueda footer-fondo">
                                <img src="http://empresaspolar.com/img/app/footer-imagen-buscar-texto.png" class="footer-comun footer-buscar enlace-buscar"/>
                                <input type="text" class="footer-input"/>
                            </div>
                        </div>
                        <!--<div class="footer-container hidden-xs">
                            <div class="footer-contenedor">
                                <span class="footer-enlace"><span class="enlace-idioma" id="idioma_ingles">English</span>&nbsp;/&nbsp;<span class="enlace-idioma" id="idioma_espanol">Español</span></span>
                            </div>
                        </div>-->
                    </div>
                </div>
            </div>
        </div>
        <!--<div class="footer-tooltip">
            <div class="footer-download">Descargar PDF</div>
            <div class="footer-pdf"><a href="http://empresaspolar.com/pdf/Polar_2WEB.pdf" target="_blank">Presentación Empresas Polar</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="http://empresaspolar.com/pdf/Polar_2WEB_en.pdf" target="_blank">Empresas Polar (In English)</a></div>
        </div>-->
        <!--<div class="footer-tooltip2">
            <div class="footer-download">Programa de Visitas</div>
            <div class="footer-pdf"><a href="http://empresaspolar.com/programa-de-visitas" target="_blank">Obtén información para agendar una visita a nuestras plantas</a></div>
        </div>-->
        <div class="footer-tooltip3">
            <div class="footer-pdf">
                <ul>
                    <li><a target="_blank" href="https://pp.dm-ep.com:44400/slc_selfreg">Nuevo proveedor</a></li>
                    <li><a target="_blank" href="https://pp.dm-ep.com:444/irj/portal">Proveedor registrado</a></li>
                    <li><a target="_blank" href="http://empresaspolar.com/pdf/Terminos-y-Condiciones-de-Uso-Portal-de-Proveedores-Empresas-Polar.pdf">Términos y Condiciones</a></li>
                    <li><a target="_blank" href="http://empresaspolar.com/pdf/Documento-Ayuda-para-registro.pdf">Ayuda para registro</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>	<script>
    $('document').ready(function()
    {

    });

</script>
<div class="redes">
    <div class="redes-padding">
        <a href="https://www.facebook.com/empresaspolar?ref=ts&fref=ts" target="_blank">
            <div class="redes-red redes-facebook"></div>
        </a>
    </div>
    <div class="redes-padding">
        <a href="https://twitter.com/EmpresasPolar" target="_blank">
            <div class="redes-red redes-twitter"></div>
        </a>
    </div>
    <div class="redes-padding">
        <a href="http://ve.linkedin.com/company/empresas-polar" target="_blank">
            <div class="redes-red redes-linkedin"></div>
        </a>
    </div>
    <!--<div class="redes-padding">
        <a href="http://instagram.com/empresaspolar" target="_blank">
            <div class="redes-red redes-instagram"></div>
        </a>
    </div>-->
    <div class="redes-padding">
        <a href="http://www.youtube.com/user/EmpresasPolarOficial" target="_blank">
            <div class="redes-red redes-youtube"></div>
        </a>
    </div>
</div>	<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-42577042-1', 'empresaspolar.com');
    ga('send', 'pageview');

</script>
</body>
</html>
