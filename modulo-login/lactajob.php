<?php
session_start();
require_once 'Controllers/Controller.php';
require_once 'Models/Crud.php';
$solicitud = new MvcController();
$solicitud->ingresoUserController();

if (!isset($_SESSION['user'])  && !isset($_SESSION['user_dasa']) && !isset($_SESSION['user_del'])) {
    header('Location: index.php');
}
$idUser = $_SESSION['user'];
$datos = new MvcController();
#$datos->validacionDashBoard($idUser);

?>
<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <title>LactaJob</title>
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    <script src="assets/js/jquery-ui-1.12.1.custom/jquery-ui.js"></script>
    <script src="assets/js/sliderDash.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="assets/js/envioEmail.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/dynamicDashboard.js"></script>
    <script src="assets/js/lateralMenu.js"></script>

    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/imgEfectsStyles.css">
    <link rel="stylesheet" href="assets/css/imgStyle.css">
    <link rel="stylesheet" href="assets/css/imagehover.min.css">

    <!--<link rel="stylesheet" href="http://csshake.surge.sh/csshake.css">-->
    <link rel="stylesheet" href="assets/css/styleDashBoard.css">
    <link rel="stylesheet" href="assets/css/imgEfectsStyles.css">
    <link rel="stylesheet" href="assets/css/sliderStyle.css">
    <link rel="stylesheet" href="assets/css/menuStyle.css">
    <link rel="stylesheet" href="assets/css/bannerStile.css">
    <link rel="stylesheet" href="assets/css/footerStyle.css">
    <link rel="icon" href="img/grupo_lactalis.png" type="image/gif" sizes="16x16">
    <style type="text/css">

        body {
            font-family: "Segoe UI", sans-serif;
            font-size: 100%;
        }

        .sidebar {
            position: relative;
            height: 50%;
            width: 50%;
            top: 50px;
            left: 0;
            z-index: 1;
            background-color: #77c5e7;
            overflow-x: hidden;
            transition: 0.4s;
            padding: 1rem 0;
            box-sizing: border-box;
        }

        .sidebar .boton-cerrar {
            position: absolute;
            top: 0.5rem;
            right: 1rem;
            font-size: 2rem;
            display: block;
            padding: 0;
            line-height: 1.5rem;
            margin: 0;
            height: 32px;
            width: 32px;
            text-align: center;
            vertical-align: top;
        }

        .sidebar ul, .sidebar li {
            margin: 0;
            padding: 0;
            list-style: none inside;
        }

        .sidebar ul {
            margin: 4rem auto;
            display: block;
            width: 80%;
            min-width: 200px;
        }

        .sidebar a {
            display: block;
            font-size: 120%;
            color: #eee;
            text-decoration: none;

        }

        .sidebar a:hover {
            color: #fff;
            background-color: #f90;

        }

        h1 {
            color: #f90;
            font-size: 180%;
            font-weight: normal;
        }

        #contenidoJob {
            align-content: center;
            transition: margin-left .7s;
            padding: 1rem;
        }

        .abrir-cerrar {
            color: #2E88C7;
            font-size: 1rem;
            background-image: url("img/Icons/menuToggle.png");
        }

        #abrir {

        }

        #cerrar {
            display: none;
        }

        .toggle {
            display: block;
            width: 40px;
            height: 40px;
            background: #f2f2f2 url(img/Icons/menuToggle.png) 50% 50% no-repeat
        }


    </style>
</head>
<body>
<nav class="navbar fixed-top navbar-expand-lg navbar- bg fixed-top" style="background: #77c5e7">
    <div class="container">
        <a class="navbar-brand" href="principal.php" id="imgLogoLactalis"><img src="img/lactalis_mexico.jpg" alt=""
                                                                               class="float-lg-left w-50"
                                                                               id="img_logo_home"></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="acerca.php" style="color: #FFFFFF;font-size: 18px">Acerca de nosotros</a>
                </li>
                <!--<li class="nav-item">
                    <a class="nav-link" href="misVacaciones.php" style="color: #FFFFFF;font-size: 18px">Vacaciones</a>
                </li>-->
                <li class="nav-item">
                    <a class="nav-link" href="contactUs.php" style="color: #FFFFFF;font-size: 18px"">Contáctanos</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="historia.php" style="color: #FFFFFF;font-size: 18px"">Historia</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="principal.php#title"
                       style="color: #FFFFFF;font-size: 18px"">Comunicados</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="lactajob.php" style="color: #FFFFFF;font-size: 18px"">LactaJob</a>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false" style="color: #FFFFFF;font-size: 18px"">
                    Formatos
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                        <a class="dropdown-item" href="intranet/Formatos/Formato%20de%20Servicios%20IT.pdf">Formato de
                            servicios IT</a>
                        <a class="dropdown-item" href="intranet/Formatos/FORMATO%20PRESTAMOS.pdf">Formato de
                            préstamos</a>
                        <a class="dropdown-item" href="intranet/Formatos/FORMATO%20VACACIONES.pdf">Formato de
                            vacaciones</a>
                        <a class="dropdown-item" href="intranet/Formatos/Anexo%201%20-Requisición%20de%20Personal.pdf">Requisición
                            de Personal</a>
                        <a class="dropdown-item"
                           href="intranet/Formatos/Formato Entrevista de Salida Nuevo V04062019.pdf">Formato Entevista
                            de Salida</a>
                        <a class="dropdown-item" href="intranet/Formatos/FORMATO%20DE%20MOVIM.%20DE%20PERSONAL.PDF">Formato
                            Movimiento de Personal</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false" style="color: #FFFFFF;font-size: 18px"">
                    Politicas
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                        <a class="dropdown-item" href="intranet/POLITICAS/Anexo%201%20-Requisición%20de%20Personal.pdf">Política
                            Requisición de Personal</a>
                        <a class="dropdown-item"
                           href="intranet/POLITICAS/DRH-P012-POLITICA%20UBER%20FOR%20BUSINESS.pdf">Política Uber para
                            Negocios</a>
                        <a class="dropdown-item"
                           href="intranet/POLITICAS/Política%20de%20viajes%20nacionales%20e%20internacionales.pdf">Política
                            de Viajes</a>
                        <a class="dropdown-item" href="intranet/POLITICAS/CODIGO%20DE%20CONDUCTA.PDF">Código de
                            Conducta</a>
                        <a class="dropdown-item"
                           href="intranet/POLITICAS/Comunicado Interno - Movimientos de Personal.pdf">Movimiento de
                            Personal</a>
                    </div>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false" style="color: #FFFFFF;font-size: 18px"">
                    Configuración
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                        <a class="dropdown-item" href="profile.php">Perfil</a>
                        <a class="dropdown-item" href="Controllers/cerrarSesion.php">Cerrar sesión</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>


<br><br><br>

<br><br><br>


<div class="container">


    <h1 class="mt-4 mb-3">
        <b class="" style="color: #0b2e13;font-size: 45px">LactaJob</b>

    </h1>

    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="principal.php">Inicio</a>
        </li>
        <li class="breadcrumb-item active">LactaJob</li>
    </ol>
    <h3> Conoce nuestras vacantes dando click en la imagen</h3>


    <div class="row">


        <div class="col-xs-4 col-lg-4 col-lg-2">
            <div class="col-lg-12  mb-4 py-5">
                <div id="estado">
                    <script src="assets/js/contadorDias.js"></script>
                </div>
                <figure class="imghvr-push-left">
                    <img src="img/img_postulaciones/kam.png" alt="example-image" id="img_sabias">
                    <figcaption>
                        <h3 class="ih-fade-down ih-delay-sm ">LactaJob</h3>
                        <strong><p class="ih-zoom-in ih-delay-md">Vacante: KAM</p>
                        </strong>
                    </figcaption>
                    <a href="" class="text-lg" style="font-size: 40px"
                       data-toggle="modal" data-target="#job_1"></a>
                </figure>
            </div>

            <div class="modal" id="job_1">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">


                        <div class="modal-header">
                            <h1 class="modal-title text-center" style="color:#77c5e7 ">LactaJob</h1>
                            <button type="button" class="close" data-dismiss="modal">×</button>
                        </div>


                        <div class="modal-body" style="font-family: 'Apple Color Emoji'">
                            <h4 style="font-family: 'Arial Narrow'">Prueba de lactajob</h4>

                            <img src="img/img_postulaciones/kam.png" class="img-responsive"
                                 style="max-width: 750px;max-height: 950px">
                            <br>

                        </div>


                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                        </div>

                    </div>
                </div>
            </div>
        </div>


        <div class="col-xs-4 col-lg-4 col-lg-2">
            <div class="col-lg-12  mb-4 py-5">

                <div id="estado1">
                    <script src="assets/js/contadorDias.js"></script>
                </div>

                <figure class="imghvr-push-left">
                    <img src="img/img_postulaciones/gerenteCalidad.jpg" alt="example-image" id="img_pizza">
                    <figcaption>
                        <h3 class="ih-fade-down ih-delay-sm ">LactaJob</h3>
                        <strong><p class="ih-zoom-in ih-delay-md">Vacante: Gerentes de Cuentas Clave</p>
                        </strong>


                    </figcaption>
                    <a href="" class="text-lg" style="font-size: 40px"
                       data-toggle="modal" data-target="#job_2"></a>
                </figure>
            </div>

            <div class="modal" id="job_2">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">


                        <div class="modal-header">
                            <h1 class="modal-title text-center" style="color:#77c5e7 ">LactaJob</h1>
                            <button type="button" class="close" data-dismiss="modal">×</button>
                        </div>


                        <div class="modal-body" style="font-family: 'Apple Color Emoji'">
                            <h4 style="font-family: 'Arial Narrow'">Prueba de lactajob</h4>

                            <img src="img/img_postulaciones/gerenteCuentasClave.jpg" class="img-responsive"
                                 style="max-width: 750px;max-height: 950px">
                            <br>

                        </div>


                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                        </div>

                    </div>
                </div>
            </div>

        </div>


        <div class="col-xs-4 col-lg-4 col-lg-2">

            <div class="col-lg-12  mb-4 py-5">
                <div id="estado2">
                    <script src="assets/js/contadorDias.js"></script>
                </div>

                <figure class="imghvr-push-left">
                    <img src="img/img_postulaciones/cross_border.jpg" alt="example-image" id="img_sabias">
                    <figcaption>
                        <h3 class="ih-fade-down ih-delay-sm ">LactaJob</h3>
                        <strong><p class="ih-zoom-in ih-delay-md">Vacante: Analista de Croos Border</p>
                        </strong>
                    </figcaption>
                    <a href="" class="text-lg" style="font-size: 40px"
                       data-toggle="modal" data-target="#job_3"></a>
                </figure>
            </div>
            <div class="modal" id="job_3">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">


                        <div class="modal-header">
                            <h1 class="modal-title text-center" style="color:#77c5e7 ">LactaJob</h1>
                            <button type="button" class="close" data-dismiss="modal">×</button>
                        </div>


                        <div class="modal-body" style="font-family: 'Apple Color Emoji'">
                            <h4 style="font-family: 'Arial Narrow'">Prueba de lactajob</h4>

                            <img src="img/img_postulaciones/cross_border.jpg" class="img-responsive"
                                 style="max-width: 750px;max-height: 950px">
                            <br>

                        </div>


                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                        </div>

                    </div>
                </div>
            </div>

        </div>


        <div class="col-xs-4 col-lg-4 col-lg-2">

            <div class="col-lg-12  mb-4 py-5">
                <div id="estado3">
                    <script src="assets/js/contadorDias.js"></script>
                </div>

                <figure class="imghvr-push-left">
                    <img src="img/img_postulaciones/cpfr.jpg" alt="example-image" id="img_sabias">
                    <figcaption>
                        <h3 class="ih-fade-down ih-delay-sm ">LactaJob</h3>
                        <strong><p class="ih-zoom-in ih-delay-md">Vacante: CPFR</p>
                        </strong>

                    </figcaption>
                    <a href="" class="text-lg" style="font-size: 40px"
                       data-toggle="modal" data-target="#job_4"></a>
                </figure>
            </div>
            <div class="modal" id="job_4">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">


                        <div class="modal-header">
                            <h1 class="modal-title text-center" style="color:#77c5e7 ">LactaJob</h1>
                            <button type="button" class="close" data-dismiss="modal">×</button>
                        </div>


                        <div class="modal-body" style="font-family: 'Apple Color Emoji'">
                            <h4 style="font-family: 'Arial Narrow'">Prueba de lactajob</h4>

                            <img src="img/img_postulaciones/cpfr.jpg" class="img-responsive"
                                 style="max-width: 750px;max-height: 950px">
                            <br>

                        </div>


                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                        </div>

                    </div>
                </div>
            </div>

        </div>




        <div id="resultado"></div>


        <!--<script type="application/javascript">
            var today   = new Date('7/10/2019');
            var sumDay  =   10;
            var future  =  today.getDate()+ sumDay ;
            document.getElementById('resultado').style.color='blue';
           // document.getElementById('resultado').innerHTML = today.getDate()+ '/' + (today.getMonth()+1)+ '/'+ today.getFullYear();

                if (today   != future){
                    document.getElementById('resultado').innerHTML = future;
                }else if (today ==  future){
                    document.getElementById('resultado').innerHTML = future;
                }





        </script>-->


    </div>


</div>
<p>
    <strong>
        Nota:
        Nuestras vacantes tienen 10 días de vigencia dentro de la nuestro sitio a partir del día en el que sean
        publicadas
    </strong>
</p>
<br><br><br>


<footer class="py-5 bg" style="background: #77c5e7">
    <div class="container" style="color: #FFFFFF">
        © 2018 Copyright:
        <a href="http://www.esmeralda.com.mx/" style="color: #FFFFFF"> Esmeralda.com.mx</a>
        <p>todos los derechos reservados</p>
        <div class="col-lg-11 py-lg-5">
            <div class="mb-3 flex-center">
                <a class="fb-ic"
                   href="https://www.facebook.com/pages/category/Industrial-Company/Vacantes-Lactalis-M%C3%A9xico-281980632337467/">
                    <img src="img/facebook.png" alt="">
                </a>
                <a class="tw-ic" href="https://twitter.com/groupe_lactalis?lang=en">
                    <img src="img/twitter.png">
                </a>

                <a class="ins-ic" href="https://www.instagram.com/galbanicheese/?hl=en">
                    <img src="img/instagram.png">
                </a>

            </div>
        </div>
    </div>
    <!-- /.container -->
</footer>


</body>

</html>